{{--email layout for profile update--}}
<html lang="en">
<head>
</head>
<body>
    <body>
        <p>Hi {{ $name }},</p>
        <p>Your profile has been successfully updated on {{ $time }}.</p>
        <p><i>If you didn't make any changes, please contact IT Department ASAP.</i></p>
        <p>..<br/>Regards<br/>Team Entropy</p>
    </body>
</body>
<footer>
</footer>
</html>

