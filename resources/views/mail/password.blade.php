{{--email layout for password update--}}
<html lang="en">
<head>
</head>
<body>
    <p>Hi {{ $name }},</p>
    <p>Your password has been successfully changed on {{ $time }}.</p>
    <p><i>If you didn't make any changes, please contact IT Department ASAP.</i></p>
    <p>..<br/>Regards<br/>Team Entropy</p>
</body>
<footer>
</footer>
</html>
