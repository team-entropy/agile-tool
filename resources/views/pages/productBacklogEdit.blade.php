@extends('app')

@section('links')
<link type="text/css" rel="stylesheet" href="/css/projects/product-backlog.css">
@stop

@section('content')
<div class="row card">
    <h4>Product Backlog - Edit User Story</h4>
</div> <!-- main-header-->

<div class="row">

        <div class="row alert alert-danger" id="user-story-edit-alert-error" style="display: none">

        </div>
        <div class="row alert alert-success" id="user-story-edit-alert-success" style="display: none">

        </div>
    <div class="panel panel-default">
        <div class="panel-heading" id="user-story-edit-info">
            User Story ID: {{ $userStory->id }}&nbsp&nbsp&nbsp|&nbsp&nbsp&nbspProject ID: {{ $userStory->project_id }}
        </div>
        <div class="panel-body" >
            <form style="padding: 10px;" role="form" class="form-horizontal form" method="POST" action="/product-backlog/store" id="user-story-edit-form">
                <div class="form-group">
                    <label for="title">Title</label>
                    <input value="{{ $userStory->title }}" type="text" class="form-control" name="title">
                </div>
                <div class="form-group">
                    <label for="description">Description</label>
                    <textarea class="form-control" name="description" rows="5">{{ $userStory->description }}
                    </textarea>
                </div>
                <div class="form-group col-md-3">
                    <label for="effort">Effort</label>
                    <select class="form-control" name="effort">
                        @for ($i = 0; $i <= 12; $i++)
                            @if ($i == $userStory->effort)
                                <option value="{{ $i }}" selected> {{ $i }} </option>
                            @else
                                <option value="{{ $i }}"> {{ $i }} </option>
                            @endif
                        @endfor

                    </select>
                </div>
                <div class="form-group col-md-3 " style="margin-left: 20px; margin-right: 20px;">
                    <label for="progress">Progress</label>

                    <select class="form-control" name="progress">
                        @if (strcasecmp($userStory->progress, "completed") == 0)
                            <option value="Completed" selected> Completed </option>
                            <option value="In Progress"> In Progress </option>
                            <option value="Testing"> Testing </option>
                        @elseif (strcasecmp($userStory->progress, "in progress") == 0)
                            <option value="Completed"> Completed </option>
                            <option value="In Progress" selected> In Progress </option>
                            <option value="Testing"> Testing </option>
                        @elseif (strcasecmp($userStory->progress, "testing") == 0)
                        <option value="Completed"> Completed </option>
                        <option value="In Progress"> In Progress </option>
                        <option value="Testing" selected> Testing </option>
                        @endif
                    </select>
                </div>
                <div class="form-group col-md-3">
                    <label for="priority">Priority</label>
                    <select class="form-control" name="priority">
                        @for ($i = 0; $i <= 5; $i++)
                            @if ($i == $userStory->priority)
                                <option value="{{ $i }}" selected> {{ $i }} </option>
                            @else
                                <option value="{{ $i }}"> {{ $i }} </option>
                            @endif
                        @endfor
                    </select>
                </div>

                <hr/>

                <div class="form-group">
                    <div class="col-sm-4">
                        <button type="submit" class="btn btn-sm btn-primary" id="user-story-edit-submit">Save</button>
                    </div>
                    <div class="pull-right">
                        <button type="submit" class="btn btn-sm btn-danger" id="user-story-delete-submit">Delete</button>
                    </div>
                </div>



                <input type="hidden" name="user_story_id" value="{{ $userStory->id }}"/>
            </form>
        </div>
    </div>
</div>
@stop

@section('scripts')
    <script type="application/javascript" src="/js/projects/product-backlog-edit.js"></script>
@stop