@extends('app')

@section('links')
    <link type="text/css" rel="stylesheet" href="/css/jquery.dataTables.min.css">
    <link type="text/css" rel="stylesheet" href="/css/projects/product-backlog.css">
@stop

@section('content')
    <div class="row">
        <div class="card">
            <h4>Product Backlog</h4>
        </div>
        <div class="alert alert-danger" id="product-backlog-errors" style="display: none">

        </div>
        <div class="panel panel-default" id="main-header">
            <div class="panel-heading">
                User stories included in this project's product backlog
            </div>
            <div class="panel-body">
                <table class="table table-hover" id="product-backlog-table">
                </table>
            </div>
        </div>
    </div> <!-- main-header-->
@stop

@section('scripts')
    <script type="application/javascript" src="/js/jquery.dataTables.min.js"></script>
    <script type="application/javascript" src="/js/projects/product-backlog.js"></script>
@stop