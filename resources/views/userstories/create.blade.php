@extends('app')

@section('scripts')
    <script src="/js/jquery.validate.js"></script>
    <script src="/js/userstories/createUserStory.js"></script>
    <script src="/js/dev/userstory-validation.js"></script>
    <script src="/js/projects/table.js"></script>
@stop

{{--css files--}}
@section('links')
    <link href="/css/projects/project-custom.css" rel="stylesheet">
@stop

{{--breadcrumb--}}
@section('breadcrumbs')
    {!! Breadcrumbs::render('create-user-story', Auth::user()->type) !!}
@stop

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <h4 class="card" style="padding: 0.5em;">User Story</h4>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Create User Story</h3>
                </div>
                <div class="panel-body">
                    @if(Auth::user()->type == 'dev')
                        <form method="POST" action="/dev/user-story/create" id="userStoryForm" enctype="multipart/form-data">
                    @elseif(Auth::user()->type  == 'pm')
                        <form method="POST" action="/pm/user-story/create" id="userStoryForm" enctype="multipart/form-data">
                    @endif
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="form-group">
                            <label for="title">User Story</label>
                            <textarea class="form-control" name="title" placeholder="User Story" rows="2"></textarea>
                        </div>
                        <br>
                        <div class="form-group">
                            <label for="description">Description</label>
                            <textarea class="form-control" rows="5" name="description" placeholder="Description"></textarea>
                        </div>
                        <br>
                        <div class="row">
                            <div class="form-group">
                                <div class="col-md-2">
                                    <label for="effort">Effort (hrs)</label>
                                </div>
                                <div class="col-md-4">
                                    <input class="form-control" name="effort" type="number" placeholder="hrs">
                                </div>
                                <div class="col-md-1"></div>
                                <div class="col-md-2">
                                    <label for="progress">Progress</label>
                                </div>
                                <div class="col-md-3">
                                    <div class="btn-group">
                                        <button id="us_dropdown" type="button" class="btn" style="background-color: #ffffff; border-color: darkgrey; width:100px;" >in progress</button>
                                        <button class="btn dropdown-toggle" type="button" data-toggle="dropdown" style="background-color: #ffffff; border-color: darkgrey;">
                                        <span class="caret"></span>
                                        </button>
                                        <ul class="dropdown-menu" id="us_progress" role="menu" aria-labelledby="dropdownMenu" >
                                               <li><a tabindex="-1">in progress</a></li>
                                               <li><a tabindex="-1">testing</a></li>
                                               <li><a tabindex="-1">completed</a></li>
                                        </ul>
                                        <input type="hidden" name="progress" value="in progress">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br><br>
                        <div class="row">
                            <div class="form-group">
                                <div class="col-md-4">
                                    <button id="assignClick" type="button" class="btn btn-default">
                                        Assign Member
                                    </button>
                                    <input type="hidden" name="userId" id="userId">
                                </div>
                                <div class="col-md-5">
                                    <input type="file" class="btn btn-default" name="upload[]" multiple style="width: 15em;">
                                </div>
                                <div class="col-md-3">
                                    <button type="submit" class="btn btn-primary">Save User Story</button>
                                </div>
                            </div>
                        </div>
                        <br><br>
                        <div class="row">
                            <div class="col-md-7" id="assignContainer" style="display: none;">
                                <h5>Select Member for User Story</h5>
                                  <table class="table table-hover" id="dev-table">
                                    <div class="form-group">
                                        <input type="text" class="form-control" id="dev-table-filter" data-action="filter" data-filters="#dev-table" placeholder="Filter Members" />
                                    </div>
                                    <thead>
                                      <tr>
                                        <th>Name</th>
                                        <th></th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($members as $member)
                                            @if(Auth::user()->type == 'dev' && $member->name == Auth::user()->name)
                                                <tr>
                                                    <td> {{ $member->name  }} </td>
                                                    <td> <input type="radio" id="radioUserId" name="user_id" value="{{ $member->id }}"> </td>
                                                </tr>
                                            @elseif(Auth::user()->type == 'pm')
                                                <tr>
                                                    <td> {{ $member->name  }} </td>
                                                    <td> <input type="radio" id="radioUserId" name="user_id" value="{{ $member->id }}"> </td>
                                                </tr>
                                            @endif
                                        @endforeach
                                    </tbody>
                                  </table>
                                  <button type="button" id="memberAssign" class="btn btn-default" style="float: right;" disabled>Assign</button>
                            </div>
                        </div>
                    @if(Auth::user()->type == 'dev')
                        </form>
                    @elseif(Auth::user()->type  == 'pm')
                        </form>
                    @endif
                </div>
                <p id="scroll" style="color: #ffffff">scroll</p>
            </div>

        </div>

    </div>

</div>
@stop