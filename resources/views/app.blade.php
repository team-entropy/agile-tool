@include('includes.header')
<div id="wrapper">
        @include('includes.sidebar')
        @include('includes.logout')
        <div id="page-content-wrapper">
            <div class="container-fluid" >
                <div class="row">
                    <div class="col-md-10">
                        <!-- The breadcrumbs section will go here. -->
                        <div class="container-fluid">
                            <div class="row" id="breadcrumbs">
                                <div class="col-md-12">
                                    @yield('breadcrumbs')
                                </div>
                        <!-- /breadcrumbs -->
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    @yield('content')
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-2">
                        <div class="row">
                            @yield('notifications')
                        </div>
                    </div>
            </div>
        </div>
    </div>
@include('includes.footer')
</div>