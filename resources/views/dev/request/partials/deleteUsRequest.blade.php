@if(!$deleteUsReq)
    <div class="well well-sm" style="font-style: italic"><b>No User Story Delete Requests to Display</b></div>
@else
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Delete Userstory Requests</h3>
            <div class="pull-right">
                <span class="clickable filter" data-toggle="tooltip" title="Toggle Search Bar" data-container="body">
                    <i class="glyphicon glyphicon-search"></i>
                </span>
            </div>
        </div>
        <div class="panel-body">
            <input type="text" class="form-control" id="dev-table-filter" data-action="filter" data-filters="#dev-table" placeholder="Filter Delete Requests" />
        </div>
        <div class="row">
            <div class="col-md-12">
                {{--table to display logged in developers' delte user story request details--}}
                 <table class="table table-hover" id="dev-table">
                    <thead>
                        <tr>
                            <th style="width: 20%">Project Title</th>
                            <th style="width: 4%">#</th>
                            <th style="width: 20%">Requested On</th>
                            <th style="width: 45%">Reason</th>
                            <th style="width: 10%"></th>
                        </tr>
                    </thead>
                    <tbody>
                        {{--dynamically loads table data--}}
                        @foreach($deleteUsReq as $request)
                            @if($request->type == "pending")
                                <tr style="background-color: #7BAFD4; font-weight: bold" id={{ $request->id }}>
                            @elseif($request->type == "approved")
                                <tr style="background-color: #90EE90; font-weight: bold" id={{ $request->id }}>
                            @elseif($request->type == "declined")
                                <tr style="background-color: #FF9999; font-weight: bold" id={{ $request->id }}>
                            @endif
                                <td>{{ $request->title }}</td>
                                <td>{{ $request->us_id }}</td>
                                <td>{{ $request->requested_on }}</td>
                                <td>{{ $request->delete_reason }}</td>
                                @if($request->type == "pending")
                                    <td><button class="btn btn-xs btn-danger" id={{ $request->id }} name="delete_us_request" onclick="deletePendingRequest(this)">Delete Request</button></td>
                                @else
                                    <td></td>
                                @endif
                            </tr>
                        @endforeach
                    </tbody>
                 </table>
            </div>
        </div>
    </div>
@endif