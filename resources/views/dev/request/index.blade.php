@extends('app')

{{--scripts--}}
@section('scripts')
    <script src="/js/bootbox.js"></script>
    <script src="/js/projects/table.js"></script>
    <script src="/js/dev/projects.js"></script>
@stop

{{--css files--}}
@section('links')
    <link href="/css/projects/table.css" rel="stylesheet">
@stop

{{--breadcrumb--}}
@section('breadcrumbs')
    {!! Breadcrumbs::render('dev-requests') !!}
@stop

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <h4 class="card" style="padding: 0.5em;">Requests</h4>
                <br>
                <div class="container-fluid">
                {{--displaying color code details--}}
                <div class="row">
                    <div class="col-md-12">
                        <div class="col-md-4">
                            <center>
                            <svg width="20" height="20">
                              <rect rx="5" ry="5" width="20" height="20" style="fill:rgb(123, 175, 212);" />
                            </svg>
                            <span style="font-weight: bold"> Pending</span>
                            </center>
                        </div>
                        <div class="col-md-4">
                            <center>
                            <svg width="20" height="20">
                              <rect rx="5" ry="5" width="20" height="20" style="fill:rgb(144, 238, 144);" />
                            </svg>
                            <span style="font-weight: bold"> Approved</span>
                            </center>
                        </div>
                        <div class="col-md-4">
                            <center>
                            <svg width="20" height="20">
                              <rect rx="5" ry="5" width="20" height="20" style="fill:rgb(255,153,153);" />
                            </svg>
                            <span style="font-weight: bold"> Declined</span>
                            </center>
                        </div>
                    </div>
                </div>
                </div>
                <br>
                {{--creating tab pages--}}
                <ul class="nav nav-tabs">
                    <li class="active">
                        <a data-toggle="tab" href="#project"><b>Project Request</b></a>
                    </li>
                    @if($projectId[0]->project_id)
                        <li>
                            <a data-toggle="tab" href="#userstoryEdit"><b>User Story Edit Requests</b></a>
                        </li>
                        <li>
                            <a data-toggle="tab" href="#userstoryDelete"><b>User Story Delete Requests</b></a>
                        </li>
                    @endif
                </ul>
                {{--contents to display under each tab page--}}
                <div class="tab-content">
                  <div id="project" class="tab-pane fade in active">
                    <br>
                    @include('dev.request.partials.projectRequest')
                  </div>
                  @if($projectId[0]->project_id)
                      <div id="userstoryEdit" class="tab-pane fade">
                        <br>
                        @include('dev.request.partials.editUsRequest')
                      </div>
                      <div id="userstoryDelete" class="tab-pane fade">
                        <br>
                        @include('dev.request.partials.deleteUsRequest')
                      </div>
                  @endif
                </div>

            </div>
        </div>
    </div>
@stop
