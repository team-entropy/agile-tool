@extends('app')

{{--scripts--}}
@section('scripts')
    <script src="/js/dev/projects.js"></script>
    <script src="/js/projects/table.js"></script>
@stop

{{--css files--}}
@section('links')
    <link href="/css/projects/table.css" rel="stylesheet">
@stop

{{--breadcrumb--}}
@section('breadcrumbs')
    {!! Breadcrumbs::render('dev-product-backlog') !!}
@stop

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <h4 class="card" style="padding: 0.5em;">Product Backlog</h4>
            @if(!$userStories)
                <div class="well well-sm" style="font-style: italic"><b>No User Stories to Display in the Product Backlog</b></div>
            @else
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Product Backlog - {{ $projectName[0]->title }}</h3>
                        <div class="pull-right">
                            <span class="clickable filter" data-toggle="tooltip" title="Toggle Search Bar" data-container="body">
                                <i class="glyphicon glyphicon-search"></i>
                            </span>
                        </div>
                    </div>
                    <div class="panel-body">
                        <input type="text" class="form-control" id="dev-table-filter" data-action="filter" data-filters="#dev-table" placeholder="Filter User Stories" />
                    </div>
                    {{--table to display all user stories of the project logged in developer working on--}}
                    <table class="table table-hover" id="dev-table" style="table-layout: fixed">
                        <thead>
                            <tr>
                                <th style="width: 5%">#</th>
                                <th style="width: 45%">User Story</th>
                                <th style="width: 20%">Assigned To</th>
                                <th style="width: 10%">Effort</th>
                                <th>Progress</th>
                                <th style="width: 7%"></th>
                            </tr>
                        </thead>
                        <tbody>
                            {{--dynamically loads table data--}}
                            @foreach($userStories as $userStory)
                                @if($userStory->email == $user)
                                    <tr style="background: 	#AECAD5; font-weight: bold;">
                                @else
                                    <tr>
                                @endif
                                        <td>{{ $userStory->id }}</td>
                                        <td>{{ $userStory->title }}</td>
                                        <td>{{ $userStory->name }}</td>
                                        <td>{{ $userStory->effort }}</td>
                                        <td>{{ $userStory->progress }}</td>
                                        <td>
                                            <a href="/dev/product-backlog/{{ $userStory->id }}" style="text-decoration: none;color: black">
                                                <button class="btn btn-xs btn-primary" id="{{$userStory->id}}">View</button>
                                            </a>
                                        </td>
                                    </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            @endif
        </div>
    </div>
</div>
@stop
