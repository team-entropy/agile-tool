@extends('app')

{{--scripts--}}
@section('scripts')
    <script src="/js/jquery.validate.js"></script>
    <script src="/js/bootbox.js"></script>
    <script type="text/javascript" src="/js/dev/projects.js"></script>
    <script src="/js/dev/userstory-validation.js"></script>
    <script src="/js/userstories/createUserStory.js"></script>
@stop

{{--css files--}}
@section('links')
    <link href="/css/projects/project-custom.css" rel="stylesheet">
@stop

{{--breadcrumb--}}
@section('breadcrumbs')
    {!! Breadcrumbs::render('dev-product-backlog-user-story') !!}
@stop

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Project - {{ $projectName[0]->title }}</h3>
                        <input type="hidden" id="userId" value="{{ $userStory[0]->user_id }}">
                        <input type="hidden" id="pmId" value="{{ $projectName[0]->project_owner }}">
                        <input type="hidden" id="projectId" value="{{ $projectName[0]->id }}">
                    </div>
                    {{--display developer selected user story with editing &/ deleting privilege--}}
                    <div class="panel-body">
                        <form id="userStoryForm">
                            {{--title--}}
                            <label>User Story ID :</label>
                                <input type="text" class="form-control" id="usId" value="{{ $userStory[0]->id }}" readonly>
                            <br>
                            <label>User Story :</label>
                            <textarea id="usTitle" name="title" class="form-control" rows="2" readonly>{{ $userStory[0]->title }}</textarea>
                            <a id="editUsTitle" href="#editTitle" style="text-decoration: none;color: black; float: right;" onclick="showEdit('editTitle')">
                                </br>
                                <button type="button" class="btn btn-xs btn-info editBtn" name="editBtn" style="float:right;">Edit User Story</button>
                            </a>
                            <br>
                            <div id="editTitle" style="display: none;">
                                <br><br>
                                <textarea id="title" name="title" class="form-control" rows="2" ></textarea>
                                <br>
                                <button type="button" class="btn btn-xs btn-primary" onclick="updateField('title', 'usTitle', 'editTitle', 'editUsTitle')" style="float:right">Request for Edit</button>
                            </div>
                            <br>
                            {{--description--}}
                            <label>Description :</label>
                            <textarea id="usDesc" name="desc" class="form-control" rows="3" readonly>{{ $userStory[0]->description }}</textarea>
                            <a id="editUsDesc" href="#editDesc" style="text-decoration: none;color: black; float: right;" onclick="showEdit('editDesc')">
                                <br>
                                <button type="button" class="btn btn-xs btn-info editBtn" style="float:right;">Edit Description</button>
                            </a>
                            <br>
                            <div id="editDesc" style="display: none;">
                                <br><br>
                                <textarea id="desc" name="description" class="form-control" rows="2" ></textarea>
                                <br>
                                <button type="button" class="btn btn-xs btn-primary" onclick="updateField('desc', 'usDesc', 'editDesc', 'editUsDesc')" style="float:right">Request for Edit</button>
                            </div>
                            <br>
                            {{--effort--}}
                            <label>Effort(hrs) :</label>
                            <input id="usEffort" name="effort" type="text" class="form-control" value="{{ $userStory[0]->effort }}" readonly>
                            <a id="editUsEffort" href="#editEffort" style="text-decoration: none;color: black; float: right;" onclick="showEdit('editEffort')">
                                <br>
                                <button type="button" class="btn btn-xs btn-info editBtn" style="float:right;">Edit Effort</button>
                            </a>
                            <br>
                            <div id="editEffort" style="display: none;">
                                <br><br>
                                <input id="effort" name="effort" type="number" min="1" max="12" class="form-control" placeholder="hrs">
                                <br>
                                <button type="button" class="btn btn-xs btn-primary" onclick="updateField('effort', 'usEffort', 'editEffort', 'editUsEffort')" style="float:right">Request for Edit</button>
                            </div>
                            <br>
                            {{--progress--}}
                            <label>Progress :</label>
                            <input id="usProgress" type="text" class="form-control" value="{{ $userStory[0]->progress }}" readonly>
                            <a id="editUsProgress" href="#editProgress" style="text-decoration: none;color: black; float: right;" onclick="showEdit('editProgress')">
                                <br>
                                <button type="button" class="btn btn-xs btn-info editBtn" style="float:right;">Edit Progress</button>
                            </a>
                            <br>
                            <div id="editProgress" style="display: none;">
                                <br><br>
                                <div class="btn-group" style="float: right;">
                                    <button id="us_dropdown" type="button" class="btn btn-info" style="width:100px;" >in progress</button>
                                    <button class="btn btn-info dropdown-toggle" type="button" data-toggle="dropdown">
                                    <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu" id="us_progress" role="menu" aria-labelledby="dropdownMenu" >
                                           <li><a tabindex="-1">in progress</a></li>
                                           <li><a tabindex="-1">testing</a></li>
                                           <li><a tabindex="-1">completed</a></li>
                                    </ul>
                                    <input type="hidden" id="progress" name="progress" value="in progress">
                                </div>
                                <br><br>
                                <button type="button" class="btn btn-xs btn-primary" onclick="updateField('progress', 'usProgress', 'editProgress', 'editUsProgress')" style="float:right">Request for Edit</button>
                                <br>
                            </div>
                            <br>
                            {{--files--}}
                            @if($userStory[0]->filename)
                            <label>Available Files :</label>
                                @foreach($fileNames as $file)
                                    <a href="/public/uploads/{{ $file }}" download>{{ $file }}</a>
                                    <br>
                                @endforeach
                            @endif
                            <br><br>
                            {{--delete button--}}
                            <a style="text-decoration: none;" href="#delConfirm">
                                <button class="btn btn-danger" type="button" id="deleteUs" style="float: right;">Delete User Story</button>
                            </a>
                            <div id="delUsReason" style="display: none">
                                <textarea id="deleteReasonText" name="delete" class="form-control" rows="2" ></textarea>
                                <br>
                                <button class="btn btn-danger" type="button" id="delConfirm" style="float: right">Request for Delete</button>
                                <button class="btn btn-info" type="button" id="delCancel">Cancel</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop