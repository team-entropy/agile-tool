@extends('app')

{{--scripts--}}
@section('scripts')
    <script src="/js/bootbox.js"></script>
    <script src="/js/dev/projects.js"></script>
@stop

{{--css files--}}
@section('links')
@stop

{{--breadcrumb--}}
@section('breadcrumbs')
    {!! Breadcrumbs::render('dev-project-view') !!}
@stop

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Project - {{ $project[0]['title'] }}</h3>
                    </div>
                    {{--display developer selected individual project details--}}
                    <div class="panel-body">
                        <label>Project ID :</label>
                        <input type="text" class="form-control" id="projectId" value="{{ $project[0]['id'] }}" readonly>
                        <br>
                        <label>Project Manager :</label>
                        <input type="text" class="form-control" id="projectManager" name="{{ $pm[0]['id'] }}" value="{{ $pm[0]['name'] }}" readonly>
                        <br>
                        <label>Project Title :</label>
                        <input type="text" class="form-control" value="{{ $project[0]['title'] }}" readonly>
                        <br>
                        <label>Project Description :</label>
                        <textarea class="form-control" rows="5" readonly>{{ $project[0]['description'] }}</textarea>
                        <br>
                        <label>Tags :</label>
                        <input type="text" class="form-control" value="{{ $project[0]['technology'] }}" readonly>
                        <br>
                        @if($projectId[0]['project_id'] != null)
                            <button class="btn btn-info" style="float: right;" disabled>Request</button>
                            <br><br>
                            <label class="label label-warning" style="float: right;">You're already working on a Project</label>
                        @else
                            <button class="btn btn-info" id="projectRequest" style="float: right;">Request</button>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop