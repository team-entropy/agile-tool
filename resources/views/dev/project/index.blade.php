@extends('app')

{{--scripts--}}
@section('scripts')
    <script src="/js/dev/projects.js"></script>
    <script src="/js/projects/table.js"></script>
@stop

{{--css files--}}
@section('links')
    <link href="/css/projects/table.css" rel="stylesheet">
@stop

{{--breadcrumb--}}
@section('breadcrumbs')
    {!! Breadcrumbs::render('dev-project') !!}
@stop

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <h4 class="card" style="padding: 0.5em;">Projects</h4>
                @if($projects->isEmpty())
                    <div class="well well-sm" style="font-style: italic"><b>No Projects to Display</b></div>
                @else
                    <div class="panel panel-default">
                        <div class="panel-heading">
                        <h3 class="panel-title">Projects</h3>
                            <div class="pull-right">
                                <span class="clickable filter" data-toggle="tooltip" title="Toggle Search Bar" data-container="body">
                                    <i class="glyphicon glyphicon-search"></i>
                                </span>
                            </div>
                        </div>
                        <div class="panel-body">
                            <input type="text" class="form-control" id="dev-table-filter" data-action="filter" data-filters="#dev-table" placeholder="Filter Projects" />
                        </div>
                        {{--table to display all company projects--}}
                        <table class="table table-hover" id="dev-table">
                            <thead>
                                <tr>
                                    <th>Project ID</th>
                                    <th>Project Title</th>
                                    <th>Tags</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                {{--dynamically loads table data--}}
                                @foreach($projects as $project)
                                    @if($project->id == $projectId[0]['project_id'])
                                        <tr style="background: 	#AECAD5; font-weight: bold;">
                                    @else
                                        <tr>
                                    @endif
                                            <td>{{ $project->id }}</td>
                                            <td>{{ $project->title }}</td>
                                            <td>{{ $project->technology }}</td>
                                            <td>
                                                <a href="/dev/project/{{ $project->id }}" style="text-decoration: none;color: black">
                                                    <button class="btn btn-xs btn-primary" id="{{$project->id}}">View</button>
                                                </a>
                                            </td>
                                        </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                @endif
            </div>
        </div>
    </div>
@stop
