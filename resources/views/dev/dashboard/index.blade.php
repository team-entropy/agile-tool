@extends('app')

{{--scripts--}}
@section('scripts')
    {{--<script src="/js/dev/projects.js"></script>--}}
    {{--<script src="/js/projects/table.js"></script>--}}
@stop

{{--css files--}}
@section('links')
    <link href="/css/projects/circle.css" rel="stylesheet">
    {{--<link href="/css/projects/table.css" rel="stylesheet">--}}
@stop

{{--breadcrumb--}}
@section('breadcrumbs')
    {!! Breadcrumbs::render('dev-dashboard') !!}
@stop

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <h4 class="card" style="padding: 0.5em;">Dashboard</h4>
            <div class="well">
                {{--table to display project summary dynamically--}}
                <table class="table">
                    <thead>
                        <th style="font-size: large">Summary</th>
                        <th></th>
                    </thead>
                    <tbody>
                        <tr>
                            <td>Name</td>
                            <td style="font-weight: bold">{{ Auth::user()->name }}</td>
                        </tr>
                        <tr>
                            <td>Profession</td>
                            <td style="font-weight: bold">Software Engineer</td>
                        </tr>
                        <tr>
                            <td>Type</td>
                            <td style="font-weight: bold">{{ Auth::user()->type }}</td>
                        </tr>
                        @if(!$project)
                            <tr>
                                <td>
                                    <label class="label label-warning">You're not assigned with a Project</label>
                                </td>
                                <td></td>
                            </tr>
                        @else
                            <tr>
                                <td>Project</td>
                                <td style="font-weight: bold">{{ $project[0]->title }}</td>
                            </tr>
                            <tr>
                                <td>Total number of user stories assigned with</td>
                                <td style="font-weight: bold">{{ $userStoryCount[0] }}</td>
                            </tr>
                            <tr>
                                <td>Number of user stories in progress</td>
                                <td style="font-weight: bold">{{ $userStoryCount[1] }}</td>
                            </tr>
                            <tr>
                                <td>Number of user stories testing</td>
                                <td style="font-weight: bold">{{ $userStoryCount[2] }}</td>
                            </tr>
                            <tr>
                                <td>Number of user stories completed</td>
                                <td style="font-weight: bold">{{ $userStoryCount[3] }}</td>
                            </tr>
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <h4 class="card" style="padding: 0.5em;">Progress</h4>
            <div class="well">
                {{--table to display project progress dynamically--}}
                <table class="table">
                    <thead>
                        <th style="font-size: large">Progress Summary</th>
                        <th></th>
                    </thead>
                    <tbody>
                        @if(!$project)
                            <tr>
                                <td>
                                    <label class="label label-warning">You're not assigned with a Project</label>
                                </td>
                                <td></td>
                            </tr>
                        @else
                            <tr>
                                <td>
                                    <p><b>Project Progress</b></p>
                                    <div class="c100 p{{ $percentage[0] }} green">
                                      <span>{{ $percentage[0] }}%</span>
                                      <div class="slice">
                                        <div class="bar"></div>
                                        <div class="fill"></div>
                                      </div>
                                    </div>
                                </td>
                                <td>
                                    <p><b>Project Responsibility</b></p>
                                    <div class="c100 p{{ $percentage[1] }} green">
                                      <span>{{ $percentage[1] }}%</span>
                                      <div class="slice">
                                        <div class="bar"></div>
                                        <div class="fill"></div>
                                      </div>
                                    </div>
                                </td>
                            </tr>
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@stop
