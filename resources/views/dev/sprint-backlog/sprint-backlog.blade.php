@extends('app')

@section('scripts')

    <script type="application/javascript" src="/js/jquery.dataTables.min.js"></script>
    <script src="/js/sprint-backlog/dev-sprintBacklog.js"></script>

@stop
@section('links')
    <link type="text/css" rel="stylesheet" href="/css/jquery.dataTables.min.css">
    <link type="text/css" rel="stylesheet" href="/css/projects/product-backlog.css">
@stop
{{--breadcrumb--}}
@section('breadcrumbs')
    {!! Breadcrumbs::render('dev-sprintBacklog') !!}
@stop

@section('content')
<div class="content-wrapper" >
    <input type="hidden" id="sprintValue" onclick="dataTable()" value="">
    <div id="dropdownElement" class="dropdown" style="margin-top: 1.5em; margin-bottom: 1.5em;">
        <button class="btn btn-default dropdown-toggle" type="button" id="sprintNo"  data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
            Select Sprint
            <span class="caret"></span>
        </button>
        <ul class="dropdown-menu" id="sprintDropDown" aria-labelledby="dropdownMenu1">
            @for($i = 1 ; $i <= $projectsDetails["sprintNo"] ; $i++)
                <li class="sprintCount" id="{{$i}}">Sprint #{{$i}}</li>
            @endfor
        </ul>
    </div>

    <div id="singleSprintView">

        {{--add loop to include sprints--}}

        <div class="row">
            <div class="card">
                <h4>Sprint Backlog</h4>
            </div>
            <div class="alert alert-danger" id="product-backlog-errors" style="display: none">

            </div>
            <div class="panel panel-default" id="main-header">
                <div class="panel-heading">
                    User stories included in Sprint
                </div>
                <div class="panel-body">
                    <table class="table table-hover" id="product-backlog-table">
                    </table>
                </div>
            </div>
        </div> <!-- main-header-->

    </div>


</div>
@stop
