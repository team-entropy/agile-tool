@extends('app')

{{--use the scripts sections to add any script to the index page of projects--}}
@section('scripts')
    <script src="/js/jquery.validate.js"></script>
    <script src="/js/projects/project-custom.js"></script>
@stop

{{--use the links sections to add stylesheets to the index page of projects--}}
@section('links')
    <link href="/css/projects/project-custom.css" rel="stylesheet">
@stop

{{--use the sections to add breadcrumbs--}}
@section('breadcrumbs')
    {!! Breadcrumbs::render('pm-projects-create')!!}
@stop

@section('content')
    <div class="col-md-12 create panel panel-default">
        <h3>Create New Project</h3>
        <form role="form" method="POST" action="/pm" id="createProjectForm">
            <input name="_token" type="hidden" value="{{ csrf_token() }}">
            <div class="form-group">
                <label for="title">Project Title:</label>
                <input type="text" name="title" class="form-control" id="title">
            </div>

            <div class="form-group">
                <label for="description">Description:</label>
                <textarea class="form-control" name="description" rows="5" id="description"></textarea>
            </div>

            <div class="form-group">
                <label for="repo-slug">Repo Slug:</label>
                <input type="text" class="form-control" name="repo_slug" rows="5" id="repo-slug"/>
            </div>
            <div class="form-group">
                <label for="repo-owner">Repo Owner:</label>
                <input type="text" class="form-control" name="repo_owner" rows="5" id="repo-owner"/>
            </div>

            <label for="input-group">Tags:</label>
            <div class="input-group" style="width:30%">
                <input class="form-control"  name="tags" id="tag">
                <span class="input-group-btn">
                    <button type="button" class="btn btn-default" id="addButton" disabled>Add</button>
                </span>
            </div>
            <span class="errorText"></span>

            <div class="form-group" id="hashTags" style="height: 55px;overflow-y: auto"></div>
                <input type="hidden" id="tagData" name="technology">

            <button type="submit" class="btn btn-default" style="margin-bottom: 20px">Submit</button>
        </form>
    </div>
@stop
