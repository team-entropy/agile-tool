@extends('app')

@section('links')
<link type="text/css" rel="stylesheet" href="/css/jquery.dataTables.min.css">
<link href="/css/projects/bitbucket-show.css" rel="stylesheet">
@stop

@section('content')
<div class="row breadcrumb">

</div>

<div class="row card" id="view-users-header">
    <h4>Bitbucket Repo Information</h4>
</div>
<div class="row" id="view-users-container">
    <div class="alert alert-danger" id="view-commit-data-errors" style="display: none">

    </div>
    <div class="panel panel-default">
        <div class="panel-heading">
            Project repo information
        </div>
        <div class="panel-body">
            <p>You can find information about the project's repo below</p>
            <table class="table table-hover" id="view-commit-data-table">

            </table>
        </div>
    </div>
</div>
@stop

@section('scripts')
<script type="application/javascript" src="/js/jquery.dataTables.min.js"></script>
<script src="/js/projects/bitbucket-show.js"></script>
@stop