@extends('app')

{{--use the links sections to add stylesheets to the index page of projects--}}
@section('links')
    <link href="/css/projects/circle.css" rel="stylesheet">
@stop

{{--use the scripts sections to add any script to the index page of projects--}}
@section('scripts')
    <script src="/js/projects/project-requests.js"></script>
@stop

{{--use the sections to add breadcrumbs--}}
@section('breadcrumbs')
    {!! Breadcrumbs::render('pm-projects', $project->id)!!}
@stop

@section('content')
    <div>
        <h4 class="card" style="padding: 10px;">
            <b>Project Details</b>
        </h4>
    </div>

    <div class="well" style="padding: 10px;margin-top: 10px">
        {{--Project progress circle--}}
        <div class="c100 p{{$projectPercentage}} green" style="margin-top: 30px">
            <span>{{$projectPercentage}}%</span>
            <div class="slice">
                <div class="bar"></div>
                <div class="fill"></div>
            </div>
        </div>
        {{--Project details table--}}
        <div>
            <table class="table" style="width: 80%;">
                <thead>
                <tr>
                    <th colspan="2">Project Summary
                        <a href="{{$project->id}}/edit" style="text-decoration: none;color: black;float: right">
                            <span title="Edit Details" class="glyphicon glyphicon-pencil clickable" id="{{$project->id}}" aria-hidden="true"></span>
                        </a>
                    </th>
                </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>Title</td>
                        <td>{{$project->title}}</td>
                    </tr>
                    <tr>
                        <td>Description</td>
                        <td>{{$project->description}}</td>
                    </tr>
                    <tr>
                        <td>Started On</td>
                        <td>{{$project->created_on}}</td>
                    </tr>
                    <tr>
                        <td>Technology</td>
                        <td>{{$project->technology}}</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    {{--Requests to join for the project--}}
    <div>
        <h4 class="card" style="padding: 10px;">
            <b>Project Team Requests</b>
            <a href="/pm/projects/delete-team" style="text-decoration: none;color: black;float: right">
                <span class="glyphicon glyphicon-user clickable" style="float: right" title="Manage Team"></span>
            </a>
        </h4>
    </div>
    <div id="deleteRequestsArea">
        @if(count($developerRequests) == 0)
            <div class="well" style="padding: 10px;margin-top: 10px">
                <h5><b><i>You don't have any requests for now, please check again later</i></b></h5>
            </div>
        @else
            <div class="panel panel-default" style="margin-top: 10px">
                    <table id="deleteRequests" class="table">
                        <thead>
                        <tr>
                            <th>
                                Developer
                            </th>
                            <th>
                                Preferred Technologies
                            </th>
                            <th>
                                Requested On
                            </th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($developerRequests as $developerRequest)
                            <tr id="{{$developerRequest->user_id}}" class="deleteRequest">
                                <td>
                                    {{$developerRequest->name}}
                                </td>
                                <td>
                                    {{$developerRequest->technology_tags}}
                                </td>
                                <td>
                                    {{$developerRequest->requested_on}}
                                </td>
                                <td style="float: right;">
                                    <input type="button" id="{{$developerRequest->user_id}}" name="approved" class="btn btn-xs btn-success sendRequest saveData" value="Confirm" style="margin-right: 5px">
                                    <input type="button" id="{{$developerRequest->user_id}}" name="declined" class="btn btn-xs btn-danger saveData" value="Deny" data-toggle="modal" data-target="#confirm">
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
            </div>
        @endif
    </div>
    {{--Modal for denying--}}
    <div id="confirm" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Confirm Denying</h4>
                </div>
                <div class="modal-body">
                    <p>Reason for denying the request.</p>
                    <textarea id="reason" class="form-control" rows="5"></textarea>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-danger sendRequest" data-dismiss="modal">Deny</button>
                </div>
            </div>
        </div>
    </div>
@stop