@extends('app')

{{--use the scripts sections to add any script to the index page of projects--}}
@section('scripts')
    <script src="/js/projects/table.js"></script>
@stop

{{--use the links sections to add stylesheets to the index page of projects--}}
@section('links')
    <link href="/css/projects/table.css" rel="stylesheet">
@stop

{{--use the sections to add breadcrumbs--}}
@section('breadcrumbs')
    {!! Breadcrumbs::render('pm-dashboard')!!}
@stop

@section('content')
    <div class="row">
        <div class="col-md-12">
            <h4 class="card" style="padding: 10px;">
                <b>Projects</b>
                <a href="/pm/projects/create"><span title="Create a Project" class="glyphicon glyphicon-plus" style="color: #2ca02c;float: right;"></span>
                </a>
            </h4>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Projects</h3>
                    <div class="pull-right">
                            <span class="clickable filter" data-toggle="tooltip" title="Search Projects" data-container="body">
                                <i class="glyphicon glyphicon-search"></i>
                            </span>
                    </div>
                </div>
                <div class="panel-body">
                    <input type="text" class="form-control" id="dev-table-filter" data-action="filter" data-filters="#dev-table" placeholder="Filter Projects" />
                </div>
                <table class="table table-hover" id="dev-table">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Title</th>
                        <th>Description</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    {{--dynamically loads table data--}}
                    @foreach($projects as $project)
                        <tr id="{{$project->id}}row">
                            <td>{{$project->id}}</td>
                            <td id="{{$project->id}}">
                                <a href="/pm/projects/{{$project->id}}" class="clickable" style="text-decoration: none;color: black;display:block;width:100%;">
                                    {{$project->title}}
                                </a>
                            </td>
                            <td id="description{{$project->id}}">
                                <a href="/pm/projects/{{$project->id}}" class="clickable" style="text-decoration: none;color: black;display:block;width:100%;">
                                    {{$project->description}}
                                </a>
                            </td>
                            <td style="width: 20%">
                                <div class="popover-markup">
                                    <a href="#" style="text-decoration: none;color: black;float: right;margin-right: 5px">
                                        <span title="Delete Project" class="glyphicon glyphicon-trash clickable" id="{{$project->id}}" aria-hidden="true" data-toggle="modal" data-target="#confirm-delete"></span>
                                    </a>
                                    <a href="/pm/projects/{{$project->id}}/edit" style="text-decoration: none;color: black;float: right;margin-right: 10px">
                                        <span title="Edit Details" class="glyphicon glyphicon-pencil clickable" id="{{$project->id}}" aria-hidden="true"></span>
                                    </a>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            {{--confirmation modal--}}
            <div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">

                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title" id="myModalLabel">Confirm Delete</h4>
                        </div>

                        <div class="modal-body">
                            <p>You are about to delete this project, this procedure is irreversible.</p>
                            <p>Do you want to proceed?</p>
                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                            <a class="btn btn-danger btn-ok">Delete</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop