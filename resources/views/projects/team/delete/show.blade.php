@extends('app')

{{--use the scripts sections to add any script to the index page of projects--}}
@section('scripts')
    <script src="/js/projects/table.js"></script>
    <script src="/js/projects/manage-members.js"></script>
@stop

{{--use the links sections to add stylesheets to the index page of projects--}}
@section('links')
    <link href="/css/projects/table.css" rel="stylesheet">
@stop

{{--use the sections to add breadcrumbs--}}
@section('breadcrumbs')
    {!! Breadcrumbs::render('pm-projects-delete-developers')!!}
@stop

@section('content')
    <div class="row">
        <div class="col-md-12">
            <h4 class="card" style="padding: 10px;">
                <b>Team Members</b>
            </h4>
        </div>
    </div>
    <div id="removeRequestsArea">
        @if(count($teamMembers) == 0)
            <div class="col-lg-12 well" style="padding: 10px;margin-top: 10px">
                <h5><b><i>You don't have any members on your project, please add members</i></b></h5>
            </div>
        @else
            <div class="col-lg-12 panel panel-default" style="margin-top: 10px;">
                <table class="table" id="removeRequests">
                    <thead>
                    <tr>
                        <th>
                            Developer
                        </th>
                        <th>
                            Contact
                        </th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody id="removeRequestsBody">
                    {{--dynamically loads table data--}}
                    @foreach($teamMembers as $teamMember)
                        <tr id="{{$teamMember->id}}" class="removeRequest">
                            <td>
                                {{$teamMember->name}}
                            </td>
                            <td>
                                {{$teamMember->email}}
                            </td>
                            <td style="width: 20%;">
                                <input type="button" id="{{$teamMember->id}}" name="remove" data-technology="{{$teamMember->technology_tags}}" class="btn btn-xs btn-danger saveData" value="Remove" data-toggle="modal" data-target="#confirm" style="float: right;">
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        @endif
    </div>
    {{--confirmation modal--}}
    <div class="modal fade" id="confirm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel">Confirm Removal</h4>
                </div>

                <div class="modal-body">
                    <p>You are about remove a member from the project</p>
                    <p>Do you want to proceed?</p>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <a class="btn btn-danger sendRequest" data-dismiss="modal">Remove</a>
                </div>
            </div>
        </div>
    </div>
@stop