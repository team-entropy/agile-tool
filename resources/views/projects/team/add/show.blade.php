@extends('app')

{{--use the scripts sections to add any script to the index page of projects--}}
@section('scripts')
    <script src="/js/projects/table.js"></script>
    <script src="/js/projects/manage-members.js"></script>
@stop

{{--use the links sections to add stylesheets to the index page of projects--}}
@section('links')
    <link href="/css/projects/table.css" rel="stylesheet">
@stop

{{--use the sections to add breadcrumbs--}}
@section('breadcrumbs')
    {!! Breadcrumbs::render('pm-projects-add-developers')!!}
@stop

@section('content')
    <div class="row">
        <div class="col-md-12">
            <h4 class="card" style="padding: 10px;">
                <b>Add Team Members</b>
            </h4>
            <div id="addRequestsArea">
                @if(count($availableMembers) == 0)
                    <div class="well" style="padding: 10px;margin-top: 10px">
                        <h5><b><i>No available members for now, please check again later</i></b></h5>
                    </div>
                @else
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Available Developers</h3>
                            <div class="pull-right">
                                <span class="clickable filter" data-toggle="tooltip" title="Search Developers" data-container="body">
                                    <i class="glyphicon glyphicon-search"></i>
                                </span>
                            </div>
                        </div>
                        <div class="panel-body">
                            <input type="text" class="form-control" id="dev-table-filter" data-action="filter" data-filters="#dev-table" placeholder="Search"/>
                        </div>
                        <table class="table table-hover" id="dev-table">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Contact</th>
                                <th>Technologies</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody id="addRequestsBody">
                            {{--dynamically loads table data--}}
                            @foreach($availableMembers as $availableMember)
                                <tr id="{{$availableMember->id}}" class="addRequest">
                                    <td>
                                        {{$availableMember->id}}
                                    </td>
                                    <td>
                                        {{$availableMember->name}}
                                    </td>
                                    <td>
                                        {{$availableMember->email}}
                                    </td>
                                    <td>
                                        {{$availableMember->technology_tags}}
                                    </td>
                                    <td style="width: 20%;">
                                        <input type="button" id="{{$availableMember->id}}" name="add" class="btn btn-xs btn-success saveData sendRequest" value="Add" style="float: right;">
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                @endif
            </div>
        </div>
    </div>
@stop