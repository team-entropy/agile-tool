@extends('app')

{{--use the scripts sections to add any script to the index page of projects--}}
@section('scripts')
    <script src="/js/projects/delete-requests.js"></script>
@stop

{{--use the sections to add breadcrumbs--}}
@section('breadcrumbs')
    {!! Breadcrumbs::render('pm-projects-delete-requests')!!}
@stop

@section('content')
    <div class="row">
        <div class="col-md-12">
            <h4 class="card" style="padding: 10px;">
                <b>User Story Delete Requests</b>
            </h4>
        </div>
        <div class="col-md-12" id="deleteRequestsArea">
        @if(count($deleteRequests) == 0)
            <div class="col-md-12 well" style="padding: 10px;margin-top: 10px">
                <h5><b><i>You don't have any requests for now, please check again later</i></b></h5>
            </div>
        @else
            <div class="col-md-12 panel panel-default" style="margin-top: 10px;">
                <table class="table" id="deleteRequests">
                    <thead>
                    <tr>
                        <th>
                            #
                        </th>
                        <th>
                            Developer
                        </th>
                        <th>
                            Details of the Request
                        </th>
                        <th>
                            Requested On
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($deleteRequests as $request)
                        <tr id="{{$request->id}}">
                            <td>
                                {{$request->us_id}}
                            </td>
                            <td>
                                {{$request->name}}
                            </td>
                            <td>
                                {{$request->delete_reason}}

                            </td>
                            <td>
                                {{$request->requested_on}}
                            </td>
                            <td style="width: 20%;">
                                <input type="button" id="{{$request->id}}" name="declined" class="btn btn-xs btn-danger saveData" value="Deny" data-toggle="modal" data-target="#confirm" style="float: right;margin-left: 5px;">
                                <input type="button" id="{{$request->id}}" name="approved" class="btn btn-xs btn-success sendRequest saveData" value="Confirm" style="float: right;">
                            </td>
                        </tr>
                    @endforeach
                    </thead>
                    <tbody>
                </table>
            </div>
            @endif
        </div>
    </div>
    {{--Modal for denying--}}
    <div id="confirm" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Confirm Denying</h4>
                </div>
                <div class="modal-body">
                    <p>Reason for denying the request.</p>
                    <textarea id="reason" class="form-control" rows="5"></textarea>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-danger sendRequest" data-dismiss="modal">Deny</button>
                </div>
            </div>
        </div>
    </div>
@stop
