@extends('app')

@section('content')

    <div class="row breadcrumb">
        {!! Breadcrumbs::render('admin-home') !!}
    </div>

    <div class="row">
        <h1>Welcome Administrator.</h1>
    </div>
    <div class="row">
        <h3>Your information:</h3>
        <p>Name: {{ $user->name }}</p>
        <p>Email: {{ $user->email }}</p>
    </div>

@stop