@extends('app')

@section('links')
    <link type="text/css" rel="stylesheet" href="/css/jquery.dataTables.min.css">
    <link type="text/css" rel="stylesheet" href="/css/admin/viewUser.css">
@stop

@section('content')
    <div class="row breadcrumb">
        {!! Breadcrumbs::render('view-users') !!}
    </div>

    <div class="row card" id="view-users-header">
        <h4>View Users</h4>
    </div>
    <div class="row" id="view-users-container">
        <div class="alert alert-danger" id="view-users-errors" style="display: none">

        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                View and manage user information
            </div>
            <div class="panel-body">
                <table class="table table-hover" id="view-users-table">

                </table>
            </div>
        </div>
    </div>
@stop

@section('scripts')
    <script type="application/javascript" src="/js/jquery.dataTables.min.js"></script>
    <script type="application/javascript" src="/js/admin/viewUser.js"></script>
@stop