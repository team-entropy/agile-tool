@extends('app')

@section('links')

@stop

@section('content')

<div class="row breadcrumb">
    {!! Breadcrumbs::render('edit-users') !!}
</div>

<div class="row card" id="view-users-header">
    <h4>Edit User</h4>
</div>

<div class="row" id="view-users-container">
    <div class="row alert alert-danger" id="user-edit-alert-error" style="display: none">

    </div>
    <div class="row alert alert-success" id="user-edit-alert-success" style="display: none">

    </div>
    <div class="panel panel-default">
        <div class="panel-heading">
            Edit selected user's information
        </div>
        <div class="panel-body">
            <form style="padding: 10px;" role="form" class="form-horizontal form" method="POST" action="/admin/user-management/edit" id="user-edit-form">
                <div class="form-group col-md-10">
                    <label for="name">Name</label>
                    <input value="{{ $user->name }}" type="text" class="form-control" name="name">
                </div>
                <div class="form-group col-md-10">
                    <label for="email">Email</label>
                    <input value="{{ $user->email }}" type="email" class="form-control" name="email">
                </div>
                <div class="form-group">
                    <div class="col-md-4">
                        <label for="type">Type</label>
                        <select class="form-control" name="type">
                            @if (strcasecmp($user->type, "dev") == 0)
                                <option value="dev" selected> Developer </option>
                                <option value="pm"> Project Manager </option>
                                <option value="client"> Client </option>
                                <option value="admin"> System Administrator </option>
                            @elseif (strcasecmp($user->type, "pm") == 0)
                                <option value="dev"> Developer </option>
                                <option value="pm" selected> Project Manager </option>
                                <option value="client"> Client </option>
                                <option value="admin"> System Administrator </option>
                            @elseif (strcasecmp($user->type, "client") == 0)
                                <option value="dev"> Developer </option>
                                <option value="pm"> Project Manager </option>
                                <option value="client" selected> Client </option>
                                <option value="admin"> System Administrator </option>
                            @elseif (strcasecmp($user->type, "admin") == 0)
                                <option value="dev"> Developer </option>
                                <option value="pm"> Project Manager </option>
                                <option value="client"> Client </option>
                                <option value="admin" selected> System Administrator </option>
                            @endif
                        </select>
                    </div>
                </div>
                <div class="form-group col-md-10">
                    <label for="bitbucketUsername">Bitbucket Username</label>
                    <input value="{{ $user->bitbucket_username }}" type="text" class="form-control" name="bitbucketUsername">
                </div>
                <hr/>
                <div class="form-group">
                    <div class="col-md-3">
                        <button type="submit" class="btn btn-sm btn-primary" id="user-edit-submit">Save</button>
                    </div>
                    <div class="pull-right">
                        <button type="submit" class="btn btn-sm btn-danger" id="user-delete-submit">Delete</button>
                    </div>
                </div>
                <input value="{{ $user->id }}" type="hidden" name="user_id">
            </form>
        </div>
    </div>

    <div class="row alert alert-danger" id="reset-password-alert-error" style="display: none">

    </div>
    <div class="row alert alert-success" id="reset-password-alert-success" style="display: none">

    </div>
    <div class="panel panel-default">
        <div class="panel-heading">
            Reset user's password
        </div>
        <div class="panel-body">
            <div class="alert alert-danger" style="display: none" id="password-mismatch-error">
                <p>The passwords you have entered do not match. Please correct this to continue.</p>
            </div>
            <form id="user-password-reset-form">
                <div class="form-group col-md-10">
                    <label for="password">Password</label>
                    <input type="password" class="form-control" name="password" id="password">
                </div>
                <div class="form-group col-md-10">
                    <label for="confirm-password">Confirm Password</label>
                    <input type="password" class="form-control" name="confirm-password" id="confirm-password">
                </div>
                <div class="form-group">
                    <div class="col-md-3">
                        <button type="submit" class="btn btn-sm btn-danger" id="user-reset-password">Reset Password</button>
                    </div>
                </div>
                <input value="{{ $user->id }}" type="hidden" name="user_id">
            </form>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading">
            Connect with BitBucket
        </div>
        <div class="panel-body">
            <p>To connect your account with Bitbucket please click on the button below.</p>
            <form id="user-connect-bitbucket" action="https://bitbucket.org/site/oauth2/authorize" method="GET">
                <div class="form-group col-md-10">
                    <input type="hidden" name="client_id" value="rsHLNBmL8Q74YxKc8W" />
                    <input type="hidden" name="response_type" value="token" />
                    <button type="submit" class="btn btn-sm btn-primary" id="authorise-submit">Connect</button>
                </div>
            </form>
        </div>
    </div>
</div>
@stop

@section('scripts')
<script type="application/javascript" src="/js/admin/editUser.js"></script>
@stop