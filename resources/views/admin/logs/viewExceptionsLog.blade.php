@extends('app')

@section('links')
<link type="text/css" rel="stylesheet" href="/css/jquery.dataTables.min.css">
@stop

@section('content')
<div class="row breadcrumb">
    {!! Breadcrumbs::render('exception-log') !!}
</div>

<div class="row card" id="view-exceptions-header">
    <h4>View exceptions log</h4>
</div>
<div class="row" id="view-exceptions-container">
    <div class="alert alert-danger" id="view-exceptions-errors" style="display: none">

    </div>
    <div class="panel panel-default">
        <div class="panel-heading">
            View a record of all the exceptions
        </div>
        <div class="panel-body">
            <table class="table table-hover" id="view-exception-logs-table">

            </table>
        </div>
    </div>
</div>
@stop

@section('scripts')
<script type="application/javascript" src="/js/jquery.dataTables.min.js"></script>
<script type="application/javascript" src="/js/admin/logs/viewExceptionLog.js"></script>
@stop