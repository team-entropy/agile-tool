@extends('app')

@section('links')

@stop

@section('content')
<div class="alert alert-danger" id="bitbucket-register-user-errors" style="display: none;">
</div>
<div class="alert alert-success" id="bitbucket-register-user-success" style="display: none;">
</div>
@stop

@section('scripts')
<script type="application/javascript" src="/js/admin/registerBitbucket.js"></script>
@stop