@extends('app')

@section('links')
    <link type="text/css" rel="stylesheet" href="/css/admin/createUser.css">
@stop

@section('content')

    <div class="row breadcrumb">
        {!! Breadcrumbs::render('create-users') !!}
    </div>

    <div class="row card" id="create-user-header">
        <h4>Create a user</h4>
    </div>
    <div class="row" id="create-user-form-container">
        <div class="alert alert-danger" id="create-user-errors" style="display: none;">
        </div>
        <div class="alert alert-success" id="create-user-success" style="display: none;">
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                Enter user information and create a user
            </div>
            <div class="panel-body">
                <form method="POST" action="/admin/create-user" class="form-horizontal" id="create-user-form">
                    {!! csrf_field() !!}
                    <!--  Check if any validation errors are being passed into the view  -->
                    <div class="form-group">
                        <label for="name" class="col-md-2 control-label">Name</label>
                        <div class="">
                            <input type="text" name="name" value="{{ old('name') }}" class="col-md-8" id="name">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="email" class="col-sm-2 control-label">Email</label>
                        <div class="">
                            <input type="email" name="email" id="email" class="col-md-8">
                        </div>
                    </div>
                    <div class="alert alert-danger" style="display: none" id="password-mismatch-error">
                        <p>The passwords you have entered do not match. Please correct this to continue.</p>
                    </div>
                    <div class="form-group">
                        <label for="password" class="col-sm-2 control-label">Password</label>
                        <div class="">
                            <input type="password" name="password" id="password" class="col-md-8">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="confirmPassword" class="col-sm-2 control-label">Confirm</label>
                        <div class="">
                            <input type="password" name="confirmPassword" id="confirm-password" class="col-md-8">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="bitbucketUsername" class="col-sm-2 control-label">Bitbucket Username</label>
                        <div class="">
                            <input type="text" name="bitbucketUsername" id="bitbucket-username" class="col-md-8">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="type" class="col-sm-2 control-label">User Type</label>
                        <div class="">
                            <!--                    <input type="email" name="email" id="email" class="col-md-8">-->
                            <select id="type" name="type">
                                <option value="" >Select a type of user</option>
                                <option value="pm" >Project Manager</option>
                                <option value="dev" >Developer</option>
                                <option value="client" >Client</option>
                                <option value="admin" >System Administrator</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                            <button class="btn btn-default" type="submit" id="submit-create-user">Create User</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

@stop

@section('scripts')
    <script type="application/javascript" src="/js/admin/createUser.js"></script>
@stop