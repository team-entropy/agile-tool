@extends('app')

@section('scripts')
   <!--add your scripts here-->

@stop
@section('links')


@stop
@section('breadcrumbs')
    {!! Breadcrumbs::render('client-projects') !!}
@stop

@section('content')
<div class="content" >
    <table class="table table-hover" id="dev-table">
        <thead>
        <tr>
            <th>#</th>
            <th>Title</th>
            <th>Description</th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        {{--dynamically loads table data--}}
        @foreach($projects as $project)
            <tr id="{{$project->id}}row">
                <td>{{$project->id}}</td>
                <td id="{{$project->id}}">
                    <a href="/client/project/{{$project->id}}" class="clickable" style="text-decoration: none;color: black;display:block;width:100%;">
                        {{$project->title}}
                    </a>
                </td>
                <td id="description{{$project->id}}">
                    <a href="/client/project/{{$project->id}}" class="clickable" style="text-decoration: none;color: black;display:block;width:100%;">
                        {{$project->description}}
                    </a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>


@stop