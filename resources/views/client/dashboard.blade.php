@extends('app')

@section('scripts')
        <!--add your scripts here-->
{{--<script src="/js/client/charts.js" type="application/javascript"></script>--}}
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script src="/js/client/jquery.stepProgressBar.js" type="application/javascript"></script>

<script src="/js/client/clientProjects.js" type="application/javascript"></script>

@stop
@section('links')

    <link href="/css/client/jquery.stepProgressBar.css"  type="text/css" rel="stylesheet"/>
    <link href="/css/projects/circle.css"  type="text/css" rel="stylesheet"/>

@stop

@section('breadcrumbs')
    {!! Breadcrumbs::render('client-dashboard') !!}
@stop

@section('content')
    <div class="content" >
        <div class="page-header">
            <h2>Dashboard</h2>
            <div style="margin-top: 3em;">
                <h5>Number of Days left <span class="badge" id="days-left"></span></h5>
            </div>
        </div>
     <div class="col-md-10 well-lg">
         @include('client.partials.projectProgress')

     </div>



     <div class="col-md-8 ">
         @include('client.partials.projectBurnDown')
     </div>

    </div>
@stop