<!-- Sidebar -->
<div id="sidebar-wrapper">
    <ul class="sidebar-nav">
        @if($userData == "pm")
            @include('includes.partials.pm')
        @elseif($userData == "dev")
            @include('includes.partials.dev')
        @elseif($userData == "client")
           @include('includes.partials.client')
        @elseif($userData == "admin")
            @include('includes.partials.admin')
        @endif
    </ul>
</div>