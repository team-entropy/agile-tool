<li class="sidebar-brand">
    <a href="#">Agile Tool</a>
</li>
<li>
    User Management
    <ul style="list-style: none">
       <li> <a href="/admin/user-management/create">Create user</a> </li>
        <li> <a href="/admin/user-management/view">View users</a> </li>
    </ul>
</li>
<li>
    Log Reports
    <ul style="list-style: none">
        <li> <a href="/admin/logs/login">View login log</a> </li>
        <li> <a href="/admin/logs/exceptions">View Exceptions log</a> </li>
    </ul>
</li>
