<li class="sidebar-brand">
    <a href="#">Agile Tool</a>
</li>
<li>
    <a href="/dev/dashboard">Dashboard</a>
</li>
<li>
    <a href="/dev/project">Projects</a>
</li>
@if($assignedProjectId)
    <li>
        <a href="/dev/user-story/create">Create User Story</a>
    </li>
    <li>
        <a href="/dev/product-backlog">Product Backlog</a>
    </li>

    <li>
        <a href="/dev/sprint-backlog">Sprint Backlog</a>
    </li>
    {{--<li>--}}
        {{--<a href="/sprint-backlog">Sprint Backlog</a>--}}
    {{--</li>--}}
@endif
<li>
    <a href="/dev/requests">Request</a>
    <a  href="/notification-history">Notifications</a>
    <a href="/dev/my-profile/{{ Auth::user()->id }}">My Profile</a>
</li>

@section('notifications')
    @include('includes.notification')
@stop

