
<li class="sidebar-brand">
    <a href="#">Agile Tool</a>
</li>
<li>
    <a  href="/pm">Projects</a>
</li>

@if(!empty($projectId))
<li>
    <a  href="/pm/sprint-backlog">Sprint Backlog</a>
</li>
<li>
    <a  href="/pm/sprints-timeline">Sprint Durations</a>
</li>
<li>
    <a  href="/pm/product-backlog">Product Backlog</a>
</li>
<li>
    <a  href="/pm/user-story/create">Create User Story</a>
</li>
<li>
    <a  href="/pm/project-burndown">Project Burndown</a>
</li>
@if(strpos(Request::url(),'/pm/projects/requests/edit'))
<li>
    <a  href="/pm/projects/requests/edit/">Edit Requests</a>
</li>
@else
<li>
    <a  href="/pm/projects/requests/edit/">Edit Requests <span class="badge" style="display: initial">{{$editRequestCount}}</span></a>
</li>
@endif

@if(strpos(Request::url(),'/pm/projects/requests/delete'))
<li>
    <a  href="/pm/projects/requests/delete/">Delete Requests</a>
</li>
@else
<li>
    <a  href="/pm/projects/requests/delete/">Delete Requests <span class="badge" style="display: initial">{{$deleteRequestCount}}</span></a>
</li>
@endif
<li>
    <a  href="/pm/bitbucket/">Bitbucket Commits</a>
</li>
<li>
    <a  href="/pm/projects/delete-team">Project Team</a>
</li>
<li>
    <a  href="/pm/projects/add-team">Add Developers</a>
</li>
@endif
<li>
    <a  href="/notification-history">Notifications</a>
</li>
<li>
    <a href="/pm/my-profile/{{ Auth::user()->id }}">My Profile</a>
</li>
{{--use this section to add right sidebar--}}
@section('notifications')
@include('includes.notification')
@stop

