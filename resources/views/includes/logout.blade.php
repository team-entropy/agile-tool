@if(Auth::User())
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12" style="background-color: black">
                <div style="text-align: right; color: darkgray;">
                    <p>
                        {{ Auth::User()->name }}<br>
                        <a href="/auth/logout" style="color: darkgray">
                            <button class="btn btn-xs btn-default">Sign Out</button>
                        </a>
                    </p>
                </div>
            </div>
        </div>
    </div>
@endif