</body>

<footer>

    <script src="/js/jquery.js"></script>
    {{--add scripts from different pages , for faster loading--}}
    <!--    <script src="http://malsup.github.com/jquery.form.js"></script>-->
    <!--  JQuery Form JS has been added as a local file instead of fecting from the remote repo.
          Uncomment above line if there are any issues
    -->
    <script src="/js/jquery.form.js"></script>
    <script src="/js/createProject.js"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="/js/bootstrap.min.js"></script>

    <!-- Menu Toggle Script -->

    <!--    <script src="http://malsup.github.com/jquery.form.js"></script>-->
    <!--  JQuery Form JS has been added as a local file instead of fecting from the remote repo.
          Uncomment above line if there are any issues
    -->
    <script src="/js/jquery.form.js"></script>
    <script>
        $("#menu-toggle").click(function(e) {
            e.preventDefault();
            $("#wrapper").toggleClass("toggled");
        });
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    </script>
    <script src="/js/notification.js"></script>
    @yield('scripts')
</footer>