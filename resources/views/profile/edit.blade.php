@extends('app')

{{--scripts--}}
@section('scripts')
    <script src="/js/jquery.validate.js"></script>
    <script src="/js/dev/userstory-validation.js"></script>
    <script src="/js/bootbox.js"></script>
    <script src="/js/my-profile/profile.js"></script>
@stop

{{--css files--}}
@section('links')
    <link href="/css/projects/project-custom.css" rel="stylesheet">
@stop

{{--breadcrumb--}}
@section('breadcrumbs')
    {!! Breadcrumbs::render('my-profile-edit', $userDetails[0]->id, $type) !!}
@stop

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    {{--display developer/pm/client selected individual project details--}}
                    @include('profile.partials.form', ['formType' => 'edit'])
                </div>
            </div>
        </div>
    </div>
@stop