@extends('app')

{{--css files--}}
@section('links')
@stop

{{--breadcrumb--}}
@section('breadcrumbs')
    {!! Breadcrumbs::render('my-profile', $userDetails[0]->id, $type) !!}
@stop

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    {{--display developer selected individual project details--}}
                    @include('profile.partials.form', ['formType' => 'index'])
                </div>
            </div>
        </div>
        @if(Auth::user()->type == "pm")
        <div class="row" id="view-projects-container">
            <div class="alert alert-danger" id="view-user-repos-errors" style="display: none">

            </div>
            <div class="panel panel-default">
                <div class="panel-heading">
                    Project repo information
                </div>
                <div class="panel-body">
                    <p>These are the public repositories that you current have on your Bitbucket account</p>
                    <table class="table table-hover" id="view-user-repos-table">

                    </table>
                </div>
            </div>
        </div>
        @endif
    </div>
@stop

{{--scripts--}}
@section('scripts')
@if(Auth::user()->type == "pm")
    <script type="application/javascript" src="/js/jquery.dataTables.min.js"></script>
    <script src="/js/my-profile/profile-bitbucket.js"></script>
@endif
@stop