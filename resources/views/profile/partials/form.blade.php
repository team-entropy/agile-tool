<div class="panel-heading">
    @if( $formType == 'index')
        <a href="/{{ $type }}/my-profile/{{ $userDetails[0]->id }}/edit"><i class="glyphicon glyphicon-edit pull-right"></i></a>
    @endif
    <h3 class="panel-title">My Profile - {{ $userDetails[0]->name }}</h3>
</div>
<form id="userStoryForm" method="PUT">
    {{--user details panel--}}
    <div class="panel-body">
        <label>User ID :</label>
        <input type="text" class="form-control" id="userId" value="{{ $userDetails[0]->id }}" readonly>
        <br>
        <label>E-mail :</label>
        <input type="text" class="form-control" id="userEmail" value="{{ $userDetails[0]->email }}" readonly>
        <br>
        <label>Full Name :</label>
        @if( $formType == 'index')
            <input type="text" class="form-control" value="{{ $userDetails[0]->name }}" readonly>
        @else
            <input type="text" name="userName" id="fullName" class="form-control" value="{{ $userDetails[0]->name }}" placeholder="Full Name">
        @endif
        <br>
        <label>Post :</label>
        <input type="text" class="form-control" value="{{ $userDetails[0]->type }}" readonly>
        <br>
        @if( $formType == 'edit')
            <button type="button" class="btn btn-info btn-xs" data-toggle="modal" data-target="#passwordModal">
                Change Password
            </button>
            <br/><br/>
            <div>
                <a href="/{{ $type }}/my-profile/{{ $userDetails[0]->id }}">
                    <button type="button" class="btn btn-default">Cancel</button>
                </a>
                <button id="profileSubmit" type="button" class="btn btn-primary pull-right">Update</button>
            </div>
        @endif
        <input id="type" type="hidden" value={{ $type }}>
    </div>
    {{--password change modal--}}
    <div id="passwordModal" class="modal fade" role="dialog">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Change Password</h4>
          </div>
          <div class="modal-body">
            <label>Current Password : </label>
            <input type="password" id="currentPass" name="currentPassword" class="form-control">
            <br/>
            <label>New Password : </label>
            <input type="password" id="newPass" name="newPassword" class="form-control">
            <br/>
            <label>Confirm New Password : </label>
            <input type="password" id="confirmPass" name="confirmPassword" class="form-control">
            <label style="display: none" class="error" id="error">New password do not match</label>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
            <button type="button" id="passwordConfirm" class="btn btn-primary pull-right">Confirm</button>
          </div>
        </div>
      </div>
    </div>
</form>
