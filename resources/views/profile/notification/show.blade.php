@extends('app')

@section('links')
    <link type="text/css" rel="stylesheet" href="/css/jquery.dataTables.min.css">
@stop

@section('scripts')
    <script type="application/javascript" src="/js/jquery.dataTables.min.js"></script>
    <script type="application/javascript" src="/js/history.js"></script>
@stop

@section('content')
    <div class="row">
        <div class="col-md-12">
            <h4 class="card" style="padding: 10px;">
                <b>Notification History</b>
            </h4>
        </div>
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    View a record of all the notifications
                </div>
                <div class="panel-body">
                    <table class="table table-hover" id="notificationHistory">

                    </table>
                </div>
            </div>
        </div>
    </div>
@stop