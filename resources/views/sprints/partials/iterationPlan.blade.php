
<div class="content-wrapper">
    <div class="col-md-3" id="userStory" style="margin-top: 2em;" >
        <p>
            @include('sprints.partials.sprintView')
        </p>
    </div>
    <div class="col-md-8" id="usDetails" style="margin-top: 2em;">
       <div style="margin-bottom: 0.5em;">
        <span class="label label-default">USER STORY</span><span style="font-size: 1.2em; margin-left: 0.3em;" id="usTitle" ></span>
       </div>
        <ul class="nav nav-tabs" role="tablist" style="margin-bottom: 2em;">
            <li role="presentation" class="active"><a href="#usDescriptionTab" aria-controls="tabDesc" role="tab" data-toggle="tab">Description</a></li>
            <li role="presentation"><a href="#usTasksTab" aria-controls="tabTask" role="tab" data-toggle="tab">Tasks</a></li>
            <li role="presentation"><a href="#usInfo" id="" aria-controls="tabInfo" role="tab" data-toggle="tab">Info</a></li>
        </ul>
        <div class="tab-content">
            {{--tab related to user story description goes here --}}
            <div class="col-md-12 tab-pane" role="tabpanel1" id="usDescriptionTab">
                <div id="usDescriptionMessages">

                </div>
                <div id="usDescription">

                </div>

            </div>
            {{--tab related to user story tasks goes here--}}
            <div class="col-md-12 tab-pane" role="tabpanel1" id="usTasksTab">

                <div class="popover-markup" id="markupAddTasks">
                    <span id="AddTaskButton" class="trigger">
                        <span id='addTaskUS' style='border-bottom: 1px dotted lightgray; cursor: pointer;'  >Add Tasks</span>
                    </span>
                    <div class="head hide"><b>Details</b></div>
                    <div class="content hide">
                        <div id="taskDisError">

                        </div>
                        <div class="form-group" >
                            <input type="text" class="form-control" id="tasksTitle" placeholder="Title">
                        </div>
                        <button type="submit" onclick="addTasksCallback()" class="btn btn-default btn-block" id="Addtasks">
                            Save
                        </button>
                    </div>
                </div>
                <div id="taskMessages">

                </div>
                <div id="usTasks">

                </div>

            </div>
            <div class="col-md-12 tab-pane" role="tabpanel1" id="usInfo">
                <div id="assignSprint">

                </div>
                <div id="usProgress" class="col-md-12">

                </div>
                <div id="UsProgressChange">
                        <p>Current Progress :</p>
                        <select class="selectpicker" id="usChange">
                            <option >
                                completed
                            </option>
                            <option >
                                in progress
                            </option>
                            <option >
                                testing
                            </option>
                        </select>
                </div>

            </div>
        </div>
    </div>
</div>