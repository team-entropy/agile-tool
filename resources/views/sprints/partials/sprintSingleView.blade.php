

<div class="content-wrapper" >
    <input type="hidden" id="sprintValue" onclick="dataTable()" value="">
    <div id="dropdownElement" class="dropdown" style="margin-top: 1.5em; margin-bottom: 1.5em;">
        <button class="btn btn-default dropdown-toggle" type="button" id="sprintNo"  data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
            Select Sprint
            <span class="caret"></span>
        </button>
        <ul class="dropdown-menu" id="sprintDropDown" aria-labelledby="dropdownMenu1">
            @for($i = 1 ; $i <= $projectsDetails["sprintNo"] ; $i++)
                <li class="sprintCount" id="{{$i}}">Sprint #{{$i}}</li>
            @endfor
        </ul>
    </div>

    <div class="alert alert-warning" id="messageGoesHere" style="margin-top: 4em;">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        Oh Snap You dont have User Stories Assigned For this Sprint <a class="assignUS" href="/pm/sprint-backlog#iterationPlan">Click Here to Assign</a>
    </div>


        <div id="singleSprintView">

                {{--add loop to include sprints--}}

                <div class="row">
                    <div class="card">
                        <h4>Sprint Backlog</h4>
                    </div>
                    <div class="alert alert-danger" id="product-backlog-errors" style="display: none">

                    </div>
                    <div class="panel panel-default" id="main-header">
                        <div class="panel-heading">
                            User stories included in Sprint
                        </div>
                        <div class="panel-body">
                            <table class="table table-hover" id="product-backlog-table">
                            </table>
                        </div>
                    </div>
                </div> <!-- main-header-->

        </div>


</div>

