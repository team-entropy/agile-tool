
<ul class="nav nav-tabs" role="tablist">
    <li role="presentation" class="active"><a href="#sprintAdd" aria-controls="add" role="tab" data-toggle="tab">Add Sprints</a></li>
    <li role="presentation"><a href="#sprintView" aria-controls="view" role="tab" data-toggle="tab">View  Sprints</a></li>
    <li role="presentation"><a href="#iterationPlan" id="tabIteration" aria-controls="view" role="tab" data-toggle="tab">Iteration Plan</a></li>
</ul>
