@extends('app')

@section('scripts')
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script src="/js/client/burndown.js"></script>
@stop
@section('links')

@stop
{{--breadcrumb--}}
@section('breadcrumbs')
    {!! Breadcrumbs::render('sprint-backlog') !!}
@stop

@section('content')
    @include('client.partials.projectBurnDown')
@stop