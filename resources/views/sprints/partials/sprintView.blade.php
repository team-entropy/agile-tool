
<div class="content-wrapper">

    @if($allUserStories != null)

    {{--add loop to include sprints--}}

        @foreach($allUserStories as $item)

                <button class="card card-block"  style="width: 12em; padding: 0.2em; margin: 0.5em; font-size: 0.8em; cursor: pointer;"  id="{{$item->title}}" name="{{$item->id}}" >
                        <p >
                            <p class="card-text">
                                @if($item->progress == "in progress")
                                    <span class="label label-primary">{{$item->progress}}</span>
                                @elseif($item->progress == "testing")
                                    <span class="label label-warning">{{$item->progress}}</span>
                                @elseif($item->progress == "completed")
                                    <span class="label label-success">{{$item->progress}}</span>
                                @endif

                                <b>{{$item->title}}</b>
                            </p>

                    <span><cite title="Source Title">{{$item->effort}}</cite> hr</span>
                        </p>
                </button>

        @endforeach
    @else
        <div class="alert alert-warning">
            Oh Snap You dont have User Stories to display
        </div>
    @endif
</div>

