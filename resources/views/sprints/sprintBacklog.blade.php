@extends('app')

@section('scripts')
    <script src="/js/sprint-backlog/bootstrap-datepicker.min.js"></script>
    <script src="/js/sprint-backlog/sprintBacklog.js"></script>
    <script src="/js/sprint-backlog/bootstrap-select.js"></script>
    <script src="/js/sprint-backlog/tinymce.min.js"></script>
    <script src="/js/bootbox.js"></script>
    <script src="/js/sprint-backlog/iteration.js"></script>
    <script src="/js/sprint-backlog/tasks.js"></script>
    <script type="application/javascript" src="/js/jquery.dataTables.min.js"></script>
    <script src="/js/sprint-backlog/plugins/sprintView.js"></script>

@stop
@section('links')
    <link href="/css/sprint-backlog/bootstrap-datepicker3.min.css" rel="stylesheet">
    <link href="/css/sprint-backlog/bootstrap-datepicker.min.css" rel="stylesheet">
    <link href="/css/sprint-backlog/sprint-backlog.css" rel="stylesheet">
    <link href="/css/projects/circle.css" rel="stylesheet">
    <link type="text/css" rel="stylesheet" href="/css/jquery.dataTables.min.css">
    <link type="text/css" rel="stylesheet" href="/css/projects/product-backlog.css">
    <link type="text/css" rel="stylesheet" href="/css/bootstrap-select.min.css">
@stop
{{--breadcrumb--}}
@section('breadcrumbs')
    {!! Breadcrumbs::render('sprint-backlog') !!}
@stop

@section('content')

  @include('sprints.partials.sprintTab')
  <div class="tab-content">
    <div class="well col-md-8 tab-pane active" role="tabpanel1" id="sprintAdd" style="margin-left: 3em; margin-top: 3em;">

        @if(session('Success'))
            <div class="alert alert-success">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                {{session('Success')}}
            </div>
        @elseif(session('Error'))
            <div class="alert alert-danger">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                {{session('Error')}}
            </div>
        @elseif($errors->any())
            <ul class="alert alert-danger">
                @foreach($errors->all() as $error)
                    <li>{{$error}}</li>
                @endforeach
            </ul>
        @endif
        {{--{!! Form::open(['file' => true, 'id' => 'form', 'url' => '/sprint-backlog/create', 'enctype' => 'multipart/form-data','method'=>'GET']) !!}--}}
        <form action="/pm/sprint-backlog/create" id="form" enctype="multipart/form-data" method="GET">

            <input type="hidden" name="_token" value="{{ csrf_token() }}">

            <div class="form-group">
                <h3>Add Sprint No : {{$projectsDetails["sprintNo"]}} to {{$projectsDetails["Name"]}}</h3>
                <input type="hidden" name="sprint" value={{$projectsDetails['sprintNo']}}>
            </div>
            <div class="form-group">
                {{--{!! Form::label('project','Project :') !!}--}}
                <label>Project :</label>
                <label for="projectName">{{$projectsDetails["Name"]}}</label>
            </div>
            <div class="form-group" id="sprint-content">

                    @if(empty($members))
                        <div class="alert alert-danger">Opps you havent added members to your project <a href="/pm/projects/add-team">Click here to add</a></div>
                    @else
                    {{--{!! Form::label("title_scrum_master","Select scrum master :") !!}--}}
                    <label>Select Scrum Master</label>
                    <input type="hidden" id="scrumMaster" name="scrum_master" value="">
                    <div class="dropdown">
                        <button class="btn btn-default dropdown-toggle" type="button" id="scrum" name="scrum_master" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                            Scrum Master
                            <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu" id="scrumMasterDropDown" aria-labelledby="dropdownMenu1">
                            @for($i = 0 ; $i < count($members) ; $i++)
                                <li>{{$members[$i]->name}}</li>
                            @endfor
                        </ul>
                    </div>
                    @endif

                <div class="form-group">
                    {{--{!! Form::label("Sprint-Period","Sprint Duration :") !!}--}}
                    <label>Sprint Duration</label>
                    <div class="sprint-daterange input-group" id="datepicker">
                        <input type="text" readonly="" id="start_date" class="input-sm form-control" name="start_date" />
                        <span class="input-group-addon">to</span>
                        <input type="text" id="end_date" readonly="" class="input-sm form-control" name="end_date" />
                    </div>
                </div>
            </div>
            @if(!empty($members))
                <div class="btn-group">
                    {{--{!! Form::submit('Save',['class' =>'btn btn-primary','id'=>'saveSprint']) !!}--}}
                    <button class="btn btn-default" id="saveSprint">Save</button>
                </div>
            @endif

        </form>
    </div>
    <div class="col-md-12 tab-pane" role="tabpanel1" id="sprintView">
        @include('sprints.partials.sprintSingleView')
    </div>
      <div class="col-md-12 tab-pane" role="tabpanel1" id="iterationPlan">
          @include('sprints.partials.iterationPlan')

          @include('hidden.dataFields')
      </div>
  </div>
@stop