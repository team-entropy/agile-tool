@extends('app')

@section('scripts')
    <script type="application/javascript" src="/js/jquery.dataTables.min.js"></script>

    <script type="text/javascript" src="/js/client/sprintProgress.js"></script>
@stop
@section('links')
    <link type="text/css" rel="stylesheet" href="/css/jquery.dataTables.min.css">
    <link href="/css/client/jquery.stepProgressBar.css"  type="text/css" rel="stylesheet"/>
@stop
{{--breadcrumb--}}
@section('breadcrumbs')
    {!! Breadcrumbs::render('pm-sprintDuration') !!}
@stop

@section('content')
    <div class="row">
        <div class="card">
            <h4>Sprint Durations</h4>
        </div>
        <div class="alert alert-danger" id="product-backlog-errors" style="display: none">

        </div>
        <div class="panel panel-default" id="main-header">
            <div class="panel-heading">
                Days Left In the Project <span class="badge">{{$startDate}}</span>
            </div>
            <div class="panel-body">
                <table class="table table-hover" id="product-backlog-table">
                </table>
            </div>
        </div>
    </div> <!-- main-header-->
@stop