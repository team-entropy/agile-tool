$.ajax({
    url: "/notification-history-log",
    method: "GET",
    success: function(data, status, requestObj) {
        //If there are no errors then a JSON object with a custom message is sent to the client
        console.log(data);

        $("#notificationHistory").DataTable({
            data: data,
            columns: [
                {data: "id", title: "ID"},
                {data: "title", title: "Project"},
                {data: "type", title: "Type"},
                {data: "time", title: "Time"}
            ],
            order: []
        });
    },
    error: function(requestObj, status, error) {
        console.log(status);
        console.log(error);
    }
});