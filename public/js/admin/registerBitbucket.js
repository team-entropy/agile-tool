/**
 * Author: Deleepa
 * Created: 03rd March 2016
 * Description: This JS file contains the code that will control the elements on the
 *              edit user page under the admin section.
 */

var url = window.location.href;
var accessToken = url.split("#")[1];
accessToken = accessToken.split("&")[0];
accessToken = accessToken.split("=")[1];

var data = {
    "accessToken" : accessToken
}
console.log(data);
$.ajax({
    url: "/bitbucket-auth/register",
    method: "post",
    data: JSON.stringify(data),
    success: function(data, status, requestObj) {
        //If there are no errors then a JSON object with a custom message is sent to the client
        console.log(data);
        $("#bitbucket-register-user-success").html(data.message);
        $("#bitbucket-register-user-success").show();

        window.setTimeout( function(){
            window.location = data.redirectUri;
        }, 5000);

        window.location = data.redirectUri + "?";
    },
    error: function(requestObj, status, error) {

        //console.log(requestObj);
        console.log(status);
        console.log(error);
        //Laravel returns a 422 error code if there is a validation error
        //If the error is not of type 422 (Unprocessable Entity) then there is some other issue
        //Then print that error code and the default status that is sent from the framework
        if(requestObj.status == 422) {
            var errors = JSON.parse(requestObj.responseText);
            var errorString = "<ul>";
            $.each(errors, function(error) {
                errorString += "<li>" + errors[error] + "</li>";
            });
            errorString += "</ul>";
            $("#bitbucket-register-user-errors").html(errorString);
            $("#bitbucket-register-user-errors").show();
        }
        else {
            console.log(requestObj);

            var errorString = "<li>" + requestObj.status + " : "  + requestObj.statusText + "</li>";
            $("#bitbucket-register-user-errors").html(errorString);
            $("#bitbucket-register-user-errors").show();
        }

    }
});
