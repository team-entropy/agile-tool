/**
 * Author: Deleepa
 * Created: 03rd March 2016
 * Description: This JS file contains the code that will control the elements on the
 *              edit user page under the admin section.
 */

//ajax call to save editted information
$("#user-edit-submit").click(function(event) {
    event.preventDefault();

    //turns off display of all alerts at first load
    $(".alert").css("display", "none")

    var userData = $("#user-edit-form").serialize();

    $.ajax({
        url: "/admin/user-management/edit",
        method: "post",
        data: userData,
        success: function(data, status, requestObj) {
            //If there are no errors then a JSON object with a custom message is sent to the client
            console.log(data);
            $("#user-edit-alert-success").html(data.message);
            $("#user-edit-alert-success").show();

        },
        error: function(requestObj, status, error) {

            //console.log(requestObj);
            console.log(status);
            console.log(error);
            //Laravel returns a 422 error code if there is a validation error
            //If the error is not of type 422 (Unprocessable Entity) then there is some other issue
            //Then print that error code and the default status that is sent from the framework
            if(requestObj.status == 422) {
                var errors = JSON.parse(requestObj.responseText);
                var errorString = "<ul>";
                $.each(errors, function(error) {
                    errorString += "<li>" + errors[error] + "</li>";
                });
                errorString += "</ul>";
                $("#user-edit-alert-error").html(errorString);
                $("#user-edit-alert-error").show();
            }
            else {
                console.log(requestObj);

                var errorString = "<li>" + requestObj.status + " : "  + requestObj.statusText + "</li>";
                $("#user-edit-alert-error").html(errorString);
                $("#user-edit-alert-error").show();
            }

        }
    });
});

//checks the confirm password while typing
$("#confirm-password").keyup(function(event) {
    console.log("confirm password lost focus");

    if($("#password").val() !== $("#confirm-password").val()) {
        $("#password-mismatch-error").show();
        $("#user-reset-password").prop('disabled', true);
    }
    else if($("#password").val() == $("#confirm-password").val()) {
        $("#password-mismatch-error").hide();
        $("#user-reset-password").prop('disabled', false);
    }
});

//ajax call to reset user password
$("#user-reset-password").click(function(event) {
    event.preventDefault();

    //turns off display of all alerts at first load
    $(".alert").css("display", "none")

    var passwordData = $("#user-password-reset-form").serialize();

    $.ajax({
        url: "/admin/user-management/password-reset",
        method: "post",
        data: passwordData,
        success: function(data, status, requestObj) {
            //If there are no errors then a JSON object with a custom message is sent to the client
            console.log(data);
            $("#reset-password-alert-success").html(data.message);
            $("#reset-password-alert-success").show();

        },
        error: function(requestObj, status, error) {

            //console.log(requestObj);
            console.log(status);
            console.log(error);
            //Laravel returns a 422 error code if there is a validation error
            //If the error is not of type 422 (Unprocessable Entity) then there is some other issue
            //Then print that error code and the default status that is sent from the framework
            if(requestObj.status == 422) {
                var errors = JSON.parse(requestObj.responseText);
                var errorString = "<ul>";
                $.each(errors, function(error) {
                    errorString += "<li>" + errors[error] + "</li>";
                });
                errorString += "</ul>";
                $("#reset-password-alert-error").html(errorString);
                $("#reset-password-alert-error").show();
            }
            else {
                console.log(requestObj);

                var errorString = "<li>" + requestObj.status + " : "  + requestObj.statusText + "</li>";
                $("#reset-password-alert-error").html(errorString);
                $("#reset-password-alert-error").show();
            }

        }
    });
});

//ajax call to delete selected user
$("#user-delete-submit").click(function(event) {
    event.preventDefault();

    //turns off display of all alerts at first load
    $(".alert").css("display", "none")

    var userData = $("#user-edit-form").serialize();

    $.ajax({
        url: "/admin/user-management/delete",
        method: "post",
        data: userData,
        success: function(data, status, requestObj) {
            //If there are no errors then a JSON object with a custom message is sent to the client
            console.log(data);
            $("#user-edit-alert-success").html(data.message);
            $("#user-edit-alert-success").show();

        },
        error: function(requestObj, status, error) {

            //console.log(requestObj);
            console.log(status);
            console.log(error);
            //Laravel returns a 422 error code if there is a validation error
            //If the error is not of type 422 (Unprocessable Entity) then there is some other issue
            //Then print that error code and the default status that is sent from the framework
            if(requestObj.status == 422) {
                var errors = JSON.parse(requestObj.responseText);
                var errorString = "<ul>";
                $.each(errors, function(error) {
                    errorString += "<li>" + errors[error] + "</li>";
                });
                errorString += "</ul>";
                $("#user-edit-alert-error").html(errorString);
                $("#user-edit-alert-error").show();
            }
            else {
                console.log(requestObj);

                var errorString = "<li>" + requestObj.status + " : "  + requestObj.statusText + "</li>";
                $("#user-edit-alert-error").html(errorString);
                $("#user-edit-alert-error").show();
            }

        }
    });
});