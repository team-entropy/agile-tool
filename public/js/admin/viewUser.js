/**
 * Author: Deleepa
 * Created: 29th February 2016
 * Description: This JS file contains the code that will control the elements on the
 *              view user page under the admin section.
 */

//ajax call to pull in user information at first load
$.ajax({
    url: "/admin/get-all-users",
    method: "GET",
    success: function(data, status, requestObj) {
        //If there are no errors then a JSON object with a custom message is sent to the client
        console.log(data);

        var usersTable = $("#view-users-table").DataTable({
            data: data,
            columns: [
                {data: "name", title: "Name"},
                {data: "email", title: "Email"},
                {data: "type", title: "Type"},
                {data: "project_id", title: "Project #"},
                {data: "id", title: "id", visible: false}
            ],

        });

        $('#view-users-table tbody').on( 'click', 'tr', function () {
            console.log( usersTable.row( this ).data().id );
            window.location.assign("/admin/user-management/edit/" + usersTable.row( this ).data().id);
        } );
    },
    error: function(requestObj, status, error) {

        //console.log(requestObj);
        console.log(status);
        console.log(error);
        //Laravel returns a 422 error code if there is a validation error
        //If the error is not of type 422 (Unprocessable Entity) then there is some other issue
        //Then print that error code and the default status that is sent from the framework
        if(requestObj.status == 422) {
            var errors = JSON.parse(requestObj.responseText);
            var errorString = "<ul>";
            $.each(errors, function(error) {
                errorString += "<li>" + errors[error] + "</li>";
            });
            errorString += "</ul>";
            $("#view-users-errors").html(errorString);
            $("#view-users-errors").show();
        }
        else {
            console.log(requestObj);

            var errorString = "<li>" + requestObj.status + " : "  + requestObj.statusText + "</li>";
            $("#view-users-errors").html(errorString);
            $("#view-users-errors").show();
        }

    }
});

