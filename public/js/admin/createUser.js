/**
 * Author: Deleepa
 * Created: 29th February 2016
 * Description: This JS file contains the code that will control the elements on the
 *              create user page under the admin section.
 */

//validation to check the password and confirm password
$("#confirm-password").keyup(function(event) {
    console.log("confirm password lost focus");

    if($("#password").val() !== $("#confirm-password").val()) {
        $("#password-mismatch-error").show();
        $("#submit-create-user").prop('disabled', true);
    }
    else if($("#password").val() == $("#confirm-password").val()) {
        $("#password-mismatch-error").hide();
        $("#submit-create-user").prop('disabled', false);
    }
});

//ajax call to create a user
$("#submit-create-user").click(function(event) {
    event.preventDefault();

    //turns off display of all alerts at first load
    $(".alert").css("display", "none");

    var userData = $("#create-user-form").serialize();
    console.log(userData);
    $.ajax({
        url: "/admin/user-management/create",
        method: "POST",
        data: userData,
        success: function(data, status, requestObj) {
            //If there are no errors then a JSON object with a custom message is sent to the client
            console.log(data);
            $("#create-user-success").html(data.message);
            $("#create-user-success").show();
        },
        error: function(requestObj, status, error) {

            //Laravel returns a 422 error code if there is a validation error
            //If the error is not of type 422 (Unprocessable Entity) then there is some other issue
            //Then print that error code and the default status that is sent from the framework
            if(requestObj.status == 422) {
                var errors = JSON.parse(requestObj.responseText);
                var errorString = "<ul>";
                $.each(errors, function(error) {
                    errorString += "<li>" + errors[error] + "</li>";
                });
                errorString += "</ul>";
                $("#create-user-errors").html(errorString);
                $("#create-user-errors").show();
            }
            else {
                console.log(requestObj);

                var errorString = "<li>" + requestObj.status + " : "  + requestObj.statusText + "</li>";
                $("#create-user-errors").html(errorString);
                $("#create-user-errors").show();
            }

        }
    });
});