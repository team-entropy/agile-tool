/**
 * Author: Deleepa
 * Created: 29th February 2016
 * Description: This JS file contains the code that will control the elements on the
 *              view exception log under the admin section.
 */

$.ajax({
    url: "/admin/get-all-exceptions",
    method: "GET",
    success: function(data, status, requestObj) {
        //If there are no errors then a JSON object with a custom message is sent to the client
        console.log(data);

        var loginLogTable = $("#view-exception-logs-table").DataTable({
            data: data,
            columns: [
                {data: "user_id", title: "User #"},
                {data: "name", title: "User"},
                {data: "time", title: "Occurred At"},
                {data: "exception", title: "Exception"}
            ],
            order: []
        });
    },
    error: function(requestObj, status, error) {

        console.log(status);
        console.log(error);
        //Laravel returns a 422 error code if there is a validation error
        //If the error is not of type 422 (Unprocessable Entity) then there is some other issue
        //Then print that error code and the default status that is sent from the framework
        if(requestObj.status == 422) {
            var errors = JSON.parse(requestObj.responseText);
            var errorString = "<ul>";
            $.each(errors, function(error) {
                errorString += "<li>" + errors[error] + "</li>";
            });
            errorString += "</ul>";
            $("#view-exceptions-errors").html(errorString);
            $("#view-exceptions-errors").show();
        }
        else {
            console.log(requestObj);

            var errorString = "<li>" + requestObj.status + " : "  + requestObj.statusText + "</li>";
            $("#view-exceptions-errors").html(errorString);
            $("#view-exceptions-errors").show();
        }

    }
});