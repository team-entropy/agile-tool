/*
 modals(alert,confirm,prompt...) are displayed using Bootbox Plugin
 */

$(document).ready(function() {

    //hides edit button for completed user stories
    if ($('#usProgress').val() == 'completed') {
        $('button.btn.btn-xs.btn-info.editBtn').css('display', 'none');
        $('#deleteUs').css('display', 'none');
    }

    //AJAX request to insert project requests (to DeveloperProjectController.store from dev.project.show)
    $('#projectRequest').click(function() {
        $.ajax({
            url: '/dev/project/projectid',
            type: 'POST',
            data: {
                project_id : $('#projectId').val(),
                pm_id : $('#projectManager').attr('name'),
                type : 'pending'
            },
            success: function(data) {
                bootbox.alert({
                    title : 'Information',
                    message : data
                });
            },
            error: function(jqXHR, textStatus) {
                bootbox.alert({
                    title : 'Information',
                    message : 'Request was not successful, Please try again!'
                });
            }
        });
    });

    //controls edit button response in dev.product-backlog.show
    showEdit = function (edit) {
        if ($('#' +edit).css('display') == 'none') {
            $('#' +edit).show(1000);
        }
        else {
            $('#' +edit).hide(1000);
        }
    }

    //AJAX request to insert/update user story requests (DeveloperBacklogController.store from dev.product-backlog.show)
    updateField = function(field, value, edit, glyph) {
        $('#extra').remove();
        if($('#' +field).valid()) {
            if($('#' +field).val() == $('#' +value).val()) {
                $('#' +field).after("<label class='error' id='extra' style='float: none;'>You have provided with already existing details!</label>");
            }
            else {
                $.ajax({
                    url: '/dev/product-backlog/userStoryId/edit',
                    type: 'PUT',
                    data: {
                        us_id: $('#usId').val(),
                        user_id: $('#userId').val(),
                        pm_id: $('#pmId').val(),
                        project_id: $('#projectId').val(),
                        field: field,
                        value: $('#' + value).val(),
                        edited_value: $('#' + field).val(),
                        type: 'pending'
                    },
                    success: function (data) {
                        bootbox.alert({
                            title : 'Information',
                            message : data
                        });
                        $('#' + edit).hide(1000);
                        $('#' + glyph).show(1000);
                    },
                    error: function (jqXHR, textStatus) {
                        bootbox.alert({
                            title : 'Information',
                            message : 'Request was not successful, Please try again!'
                        });
                    }
                });
            }
        }
    }

    //show delete reason textarea for userstory in dev.product-backlog.show
    $('#deleteUs').click(function() {
        $('#delUsReason').show(1000);
        $('#deleteUs').hide(1000);
    });

    //hide delete reason textarea for userstory in dev.product-backlog.show
    $('#delCancel').click(function() {
        $('#delUsReason').hide(1000);
        $('#deleteUs').show(1000);
    });

    //AJAX request for insert to DeveloperBacklogController.storeDel dev.product-backlog.show
    $('#delConfirm').click(function() {
        if($('textarea[name=delete]').valid()) {
            bootbox.confirm({
                title : 'Confirmation',
                message : 'Are you sure that you want to request to delete the User Story?',
                callback : function(result){
                    if(result == true) {
                        $.ajax({
                            url: '/dev/product-backlog/userStoryId/delete',
                            type: 'PUT',
                            data: {
                                us_id : $('#usId').val(),
                                user_id: $('#userId').val(),
                                pm_id: $('#pmId').val(),
                                project_id: $('#projectId').val(),
                                delete_reason: $('#deleteReasonText').val(),
                                type : 'pending'
                            },
                            success: function(data) {
                                bootbox.alert({
                                    title : 'Information',
                                    message : data
                                });
                                $('#delUsReason').hide(1000);
                                $('#deleteUs').show(1000);
                            },
                            error: function(jqXHR, textStatus) {
                                bootbox.alert({
                                    title : 'Information',
                                    message : 'Request was not successful, Please try again!'
                                });
                            }
                        });
                    }
                }
            });
        }
    });

    //AJAX request to delete pending request to DeveloperRequestController.destroy from dev.requests.index
    deletePendingRequest = function($this) {
        bootbox.confirm({
            title : 'Confirmation',
            message : 'Are you sure that you want to Delete your request?',
            callback : function(result) {
                if(result == true) {
                    $.ajax({
                        url: '/dev/requests',
                        type: 'DELETE',
                        data: {
                            id : $this.id,
                            table : $this.name
                        },
                        success: function(data) {
                            $('table#dev-table tr#' + $this.id).remove();
                            bootbox.alert({
                                title : 'Information',
                                message : data
                            });
                        },
                        error: function(jqXHR, textStatus) {
                            bootbox.alert({
                                title : 'Information',
                                message : 'Request was not successful, Please try again!'
                            });
                        }
                    });
                }
            }
        });
    }
});
