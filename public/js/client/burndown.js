/**
 * Created by rifhan on 5/3/16.
 */
/**
 * Created by rifhan on 3/25/16.
 * plugin used to display project Progress
 * http://www.jqueryscript.net/other/Animated-Step-Progress-Indicator-With-jQuery-StepProgressBar.html
 */

$(document).ready(function(){
    google.charts.load('current', {'packages':['timeline','corechart']});
    var xhr = $.ajax({
        url:"/pm/project-analytics",
        type:"get",

    })
    xhr.success(function(e){
        var value = JSON.parse(e);
        console.log(value)
        setupProjectBurnDown(value['burn-down']);
    })

    xhr.error(function(e){

    })
})


function setupProjectBurnDown(data){

    google.charts.setOnLoadCallback(function(){
        console.log(JSON.parse(data))
        var value = new google.visualization.DataTable();
        value.addColumn({type:"string",id:"Time"})
        value.addColumn({type:"number",id:"Effort"})
        value.addRows(JSON.parse(data))

        var options = {
            chart:{title : 'Project Burndown Chart',
                subtitle:'project time period against user story effort '},
            legend: { position: 'bottom' },
            width: 700,
            height: 500
        };

        var chart = new google.visualization.LineChart(document.getElementById('project-burndown'));

        chart.draw(value, options);

    });


}


