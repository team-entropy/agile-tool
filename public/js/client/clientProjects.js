/**
 * Created by rifhan on 3/25/16.
 * plugin used to display project Progress
 * http://www.jqueryscript.net/other/Animated-Step-Progress-Indicator-With-jQuery-StepProgressBar.html
 */

$(document).ready(function(){
    google.charts.load('current', {'packages':['timeline','corechart']});
   var xhr = $.ajax({
            url:"/client/project-analytics",
            type:"get",

        })
    xhr.success(function(e){
        var value = JSON.parse(e);
        console.log(value)
        setupProjectProgress(value['time-line']);
        setupProjectBurnDown(value['burn-down']);
    })

    xhr.error(function(e){

    })
})

function setupProjectProgress(data){
    var data = JSON.parse(arguments[0]);
    document.getElementById('days-left').innerHTML = data["total_days"] - data["no_of_days"];

    google.charts.setOnLoadCallback(function(){
        var container = document.getElementById('project-timeline');
        var chart = new google.visualization.Timeline(container);
        var dataTable = new google.visualization.DataTable();

        dataTable.addColumn({ type: 'string', id: 'Project' });
        dataTable.addColumn({ type: 'date', id: 'Start' });
        dataTable.addColumn({ type: 'date', id: 'End' });
        dataTable.addRows([
            ["Project Expected",new Date(data["start_date"]),new Date(data["end_date"])],
            ["Current" , new Date(data["start_date"]), new Date(data["current"])]
        ]);
        var options = {
            width : 700,
        }
        chart.draw(dataTable,options);
    })

}

function setupProjectBurnDown(data){

    google.charts.setOnLoadCallback(function(){
        console.log(JSON.parse(data))
        var value = new google.visualization.DataTable();
        value.addColumn({type:"string",id:"Time"})
        value.addColumn({type:"number",id:"Effort"})
        value.addRows(JSON.parse(data))

        var options = {
            chart:{title : 'Project Burndown Chart',
                    subtitle:'project time period against user story effort '},
            legend: { position: 'bottom' },
            width: 700,
            height: 500
        };

        var chart = new google.visualization.LineChart(document.getElementById('project-burndown'));

        chart.draw(value, options);

    });


}


