/**
 * Created by rifhan on 5/3/16.
 */

$(document).ready(function(){
    var xhr = $.ajax({
        url:"/pm/sprints-timeline-data",
        type:"get",

    })
    xhr.success(function(e){
        var value = JSON.parse(e);
        console.log(value)
        setupProjectProgress(value)
    })

    xhr.error(function(e){
        console.log("error")
    })
})

function setupProjectProgress(data){
    console.log(data)
    var productBacklogTable = $("#product-backlog-table").DataTable({
        data: data,
        columns: [
            {data: "sprint_id", title: "Sprint No"},
            {data: "start_date", title: "Start Date"},
            {data: "end_date", title: "End Date"},

        ],
        order: [

        ]
    });

}
