var notifications = [];
var red = '#FF7B91';
var green = '#80FF8F';
var blue = '#7BAFD4';

/* format notifications */
function generateNotification(id, diff, message, color) {
    var section = '<div class="row notification" id="notification_'+ id +'">' +
        '<div class="card" style="padding: 10px;display: none;background-color: white">' +
        '<table>' +
        '<tbody>'+
        '<tr>'+
        '<td style="background-color: ' + color + ';width: 10px;"></td>' +
        '<td style="padding-left: 5px"><b>' + message + '</b></td>' +
        '<span style="float: right;"><i class="glyphicon glyphicon-remove-circle clickable" id="'+ id +'"></i></span>' +
        '</tr>'+
        '<tr>'+
        '<td id="time_'+ id +'" colspan="3" style="text-align: right; color: #999999;font-size: smaller;">' + diff + '</td></tr>'+
        '</tbody>' +
        '</table>' +
        '</div>' +
        '</div>';
    $('#all-caught-up').remove().find('div.card').fadeOut('slow');
    $('#notification-bar').prepend(section).find('div.card').fadeIn('slow');
}

function isAlreadyDisplayed(id) {
    return notifications.indexOf(id);
}

/* handles the incoming notifications and the time
*  use cases to match type with the database
*  change the last parameter of the method call to change the color
* */
function dataHandler(type, msg){
    if(msg != 0) {
        $(msg).each(function(row,data) {
            if(isAlreadyDisplayed(data['id']) == -1) {
                notifications.push(data['id']);
                switch (data['type']) {
                    case 'add-developer'://chamara
                        generateNotification(data['id'], data['diff'], 'You have been added to a project by ' + data['name'] + '.', green);
                        break;
                    case 'remove-developer'://chamara!
                        generateNotification(data['id'], data['diff'], 'You have been removed from the project by ' + data['name'] + '.', red);
                        break;
                    case 'confirm-project-request'://chamara!
                        generateNotification(data['id'], data['diff'], 'Your project request been accepted by ' + data['name'] + '.', green);
                        break;
                    case 'decline-project-request'://chamara!
                        generateNotification(data['id'], data['diff'], 'Your request was rejected by ' + data['name'] + '.', red);
                        break;
                    case 'confirm-user-story-delete-request'://chamara!
                        generateNotification(data['id'], data['diff'], 'Your user story delete request was accepted by ' + data['name'] + '.', green);
                        break;
                    case 'decline-user-story-delete-request'://chamara!
                        generateNotification(data['id'], data['diff'], 'Your user story delete request was rejected by ' + data['name'] + '.', red);
                        break;
                    case 'confirm-user-story-edit-request'://chamara!
                        generateNotification(data['id'], data['diff'], 'Your user story edit request was accepted by ' + data['name'] + '.', green);
                        break;
                    case 'decline-user-story-edit-request'://chamara!
                        generateNotification(data['id'], data['diff'], 'Your user story edit request was rejected by ' + data['name'] + '.', red);
                        break;
                    case 'request-project'://saad
                        generateNotification(data['id'], data['diff'], 'You have got a new Project Request from ' + data['name'], blue);
                        break;
                    case 'request-edit_user_story'://saad
                        generateNotification(data['id'], data['diff'], 'You have got a new User Story Edit Request from ' + data['name'] + ' for User Story ID ' + data['us_id'], blue);
                        break;
                    case 'request-delete_user_story'://saad
                        generateNotification(data['id'], data['diff'], 'You have got a new User Story Delete Request from ' + data['name'] + ' for User Story ID ' + data['us_id'], blue);
                        break;
                    case 'self-profile_update'://saad
                        generateNotification(data['id'], data['diff'], 'You have updated your Profile', green);
                        break;
                    case 'self-password_change'://saad
                        generateNotification(data['id'], data['diff'], 'You have updated your Password', green);
                        break;
                    default:
                        console.log(type + ':' + msg);
                }
            } else {
                var id = 'time_' + data['id'];
                $('#'+id).html(data['diff']);
            }
        });
    }
}

/* push message as read to the server */
function markAsRead(id) {
    $.ajax({
        type: 'PATCH',
        url: '/notifications',
        data: {
            notificationId : id
        },
        dataType: 'json',
        async: true,

        error: function(XMLHttpRequest, textStatus, errorThrown){
            console.log('error' + textStatus + ' (' + errorThrown + ')');
        }
    });
}

/* fetch notification from the server */
function waitForNotifications(){
    $.ajax({
        type: 'GET',
        url: '/notifications',

        async: true,
        cache: false,
        timeout:50000,

        success: function(data){
            dataHandler('new', data);
            setTimeout(
                waitForNotifications,
                5000
            );
        },
        error: function(XMLHttpRequest, textStatus, errorThrown){
            dataHandler('error', textStatus + ' (' + errorThrown + ')');
            setTimeout(
                waitForNotifications,
                15000);
        }
    });
};

/* mark notification as read */
$(document).on('click','.glyphicon-remove-circle.clickable', function(){
    var id = 'notification_' + this.id;
    var index = notifications.indexOf(this.id);

    $('#' + id).remove().fadeOut('slow');
    notifications.splice(index,1);
    markAsRead(this.id);

    if($('.row.notification').length == 0) {
        var section = '<div class="row notification" id="all-caught-up">'+
            '<div class="card" style="padding: 10px;display: none;background-color: white">'+
            '<img src="/images/ic_notifications.png" style="display:block;margin:auto">'+
            '<p style="text-align: center"><b >All caught up!</b></p>'+
            '</div>'+
            '</div>';
        $('#notification-bar').append(section).find('div.card').fadeIn('slow');
    }
});

/* Start the initial request */
$(document).ready(function() {
    if($('.row.notification').length == 0) {
        var section = '<div class="row notification" id="all-caught-up">'+
            '<div class="card" style="padding: 10px;display: none;background-color: white">'+
            '<img src="/images/ic_notifications.png" style="display:block;margin:auto">'+
            '<p style="text-align: center"><b >All caught up!</b></p>'+
            '</div>'+
            '</div>';
        $('#notification-bar').append(section).find('div.card').fadeIn('slow');
    }

    waitForNotifications();
});