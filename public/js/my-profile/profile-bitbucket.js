/**
 * Author: Deleepa
 * Created: 03rd March 2016
 * Description: This JS file contains the code that will control the elements on the
 *              edit user page under the admin section.
 */

//ajax call to pull in user information at first load
$.ajax({
    url: "/pm/bitbucket/repo-data",
    method: "GET",
    success: function(data, status, requestObj) {
        //If there are no errors then a JSON object with a custom message is sent to the client
        console.log(data.data);

        var usersTable = $("#view-user-repos-table").DataTable({
            data: data.data,
            columns: [
                {data: "createdOn", title: "Created On", width: "15%"},
                {data: "name", title: "Project Name"},
                {data: "language", title: "Language / Technology"},
                {data: "repoSlug", title: "Repo Slug"},
                {data: "repoOwner", title: "Repo Owner"}
            ],
            order: []
        });
    },
    error: function(requestObj, status, error) {

        //console.log(requestObj);
        console.log(status);
        console.log(error);
        //Laravel returns a 422 error code if there is a validation error
        //If the error is not of type 422 (Unprocessable Entity) then there is some other issue
        //Then print that error code and the default status that is sent from the framework
        if(requestObj.status == 422) {
            var errors = JSON.parse(requestObj.responseText);
            var errorString = "<ul>";
            $.each(errors, function(error) {
                errorString += "<li>" + errors[error] + "</li>";
            });
            errorString += "</ul>";
            $("#view-user-repos-errors").html(errorString);
            $("#view-user-repos-errors").show();
        }
        else {
            console.log(requestObj);

            var errorString = "<li>" + requestObj.status + " : "  + requestObj.statusText + "</li>";
            $("#view-user-repos-errors").html(errorString);
            $("#view-user-repos-errors").show();
        }

    }
});
