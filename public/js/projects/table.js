//dynamic search function
(function(){
    'use strict';
    var $ = jQuery;
    $.fn.extend({
        filterTable: function(){
            return this.each(function(){
                $(this).on('keyup', function(e){
                    $('.filterTable_no_results').remove();
                    var $this = $(this),
                        search = $this.val().toLowerCase(),
                        target = $this.attr('data-filters'),
                        $target = $(target),
                        $rows = $target.find('tbody tr');

                    if(search == '') {
                        $rows.show();
                    } else {
                        $rows.each(function(){
                            var $this = $(this);
                            $this.text().toLowerCase().indexOf(search) === -1 ? $this.hide() : $this.show();
                        })
                        if($target.find('tbody tr:visible').size() === 0) {
                            var col_count = $target.find('tr').first().find('td').size();
                            var no_results = $('<tr class="filterTable_no_results"><td colspan="'+col_count+'">No results found</td></tr>')
                            $target.find('tbody').append(no_results);
                        }
                    }
                });
            });
        }
    });
    $('[data-action="filter"]').filterTable();
})(jQuery);

$(function(){
    // attach table filter plugin to inputs
    console.log();
    $('[data-action="filter"]').filterTable();

    //filter controller
    $('.col-md-10').on('click',  '.panel-heading span.filter', function(e){
        var $this = $(this),
            $panel = $this.parents('.panel');

        $panel.find('.panel-body').slideToggle();
        if($this.css('display') != 'none') {
            $panel.find('.panel-body input').focus();
        }
    });

    //gathering information on trash click
    var deleteId;
    $('.glyphicon.glyphicon-trash').click(function(){
        deleteId = this.id;
    });
    //ajax delete request on confirmation
    $('.btn.btn-danger.btn-ok').on('click', function(e){
        var request = $.ajax({
            url: '/pm/projects/delete',
            type: 'DELETE',
            data: {
                id : deleteId
            },
            dataType: 'json'
        });

        request.done(function(msg) {
            $('#confirm-delete').modal('toggle');
            $('#'+msg+'row').remove();
        });

        request.fail(function(jqXHR, textStatus) {
            alert( "Request failed: " + textStatus );
        });
    });

    $('[data-toggle="tooltip"]').tooltip();

    //popovers initializer
    $('.popover-markup>.trigger').popover({
        html: true,
        title: function () {
            return $(this).parent().find('.head').html();
        },
        content: function () {
            return $(this).parent().find('.content').html();
        }
    });
})