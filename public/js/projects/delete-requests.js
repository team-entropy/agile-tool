/**
 * Handles responds to the client user story delete requests
 */
var id;
var name;
var reason;
var dev;
$(document).on('click','.saveData', function() {
    id = $(this).attr('id');
    name = $(this).attr('name');
    dev = $(this).attr('data-dev');
});


$(document).on('click','.sendRequest', function() {
    if (id == '' || name == '') {
        id = $(this).attr('id');
        name = $(this).attr('name');
        dev = $(this).attr('data-dev');
    }

    reason = $('#reason').val();
    $('#reason').val('');

    var request = $.ajax({
        url: '/pm/projects/requests/update-request',
        type: 'PATCH',
        data: {
            id : id,
            type : name,
            deleteReason : reason,
            developer : dev
        },
        dataType: 'json'
    });

    request.done(function(msg) {
        $('tr#' + msg.id).remove();
        if ($('#deleteRequests tr').length == 1) {
            var data =
            '<div class="col-md-12 well" style="padding: 10px;margin-top: 10px">'+
                "<h5><b><i>You don't have any requests for now, please check again later</i></b></h5>"+
            '</div>';

            $('#deleteRequestsArea').html(data);
        }
    });

    request.fail(function(jqXHR, textStatus) {
        console.log(textStatus);
    });
});
