/**
 * Handles responds to the developers requests
 */
var userId;
var name;
var reason;

$(document).on('click','.saveData', function() {
    userId = $(this).attr('id');
    name = $(this).attr('name');
});


$(document).on('click','.sendRequest', function() {
    if (userId == '' || name == '') {
        userId = $(this).attr('id');
        name = $(this).attr('name');
    }

    reason = $('#reason').val();
    $('#reason').val('');

    var request = $.ajax({
        url: '/pm/projects/requests/update-project',
        type: 'PATCH',
        data: {
            userId : userId,
            type : name,
            deleteReason : reason
        },
        dataType: 'json'
    });

    request.done(function(msg) {
        console.log(msg);
        $('tr' + '#' + msg.userId + '.deleteRequest').remove();
        if ($('#deleteRequests tr').length == 1) {
            var data =
                '<div class="col-md-12 well" style="padding: 10px;margin-top: 10px">'+
                "<h5><b><i>You don't have any requests for now, please check again later</i></b></h5>"+
                '</div>'

            $('#deleteRequestsArea').html(data);
        }
    });

    request.fail(function(jqXHR, textStatus) {
        console.log(textStatus);
    });
});
