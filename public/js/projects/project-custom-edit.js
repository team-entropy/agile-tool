var technologies = $('#tagData').val();
var array = [];
var index = technologies.indexOf('##');

if(index > 0) {
    var subString = technologies.substring(index -1 , index + 1);
    array.push(subString);
    technologies = technologies.replace(subString+'#','');
}

$(technologies.split('#')).each(function(index, item) {
    array.push(item);
});

$(array).each(function(index, item) {
    if(item != '') {
        var id = Math.floor(Math.random() * 90000) + 10000;
        var close ='<span id="'+ id +'" class="glyphicon glyphicon-remove-circle" name="'+ item +'"></span>';
        var label = '<label id="' + id + '" class="addedTags" name="'+ item +'">#'+ item + close +'</label>';

        $('#hashTags').append(label);
    }
});
