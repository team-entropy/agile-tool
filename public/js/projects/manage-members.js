/**
 * Handles responds to the member requests
 */
var userId;
var name;
var technologies;

$(document).on('click','.saveData', function() {
    userId = $(this).attr('id');
    name = $(this).attr('name');
    technologies = $(this).attr('data-technology');
});


$(document).on('click','.sendRequest', function() {
    if (userId == '' || name == '') {
        userId = $(this).attr('id');
        name = $(this).attr('name');
    }

    var request = $.ajax({
        url: '/pm/projects/team/update-team',
        type: 'PATCH',
        data: {
            userId : userId,
            type : name,
            technologyTags : technologies
        },
        dataType: 'json'
    });

    request.done(function(msg) {
        var data =
            '<div class="col-md-12  well" style="padding: 10px;margin-top: 10px">'+
            "<h5><b><i>You don't have any members on your project, please add members</i></b></h5>"+
            '</div>';

        var members =
            '<div class="well" style="padding: 10px;margin-top: 10px">'+
            "<h5><b><i>No available members for now, please check again later</i></b></h5>"+
            '</div>';

        if(msg.type == 'add') {
            $('tr' + '#' + msg.userId + '.addRequest').remove();
        }
        else if(msg.type == 'remove') {
            $('tr' + '#' + msg.userId + '.removeRequest').remove();
        }

        if ($('#removeRequests tr').length == 1) {
            $('#removeRequestsArea').html(data);
        }
        else if($('#dev-table tr').length == 1) {
            $('#addRequestsArea').html(members);
        }
    });

    request.fail(function(jqXHR, textStatus) {
        console.log(textStatus);
    });
});