/**
 * Author: Deleepa
 * Created: 02nd March 2016
 * Description: This JS file contains the code that will control the elements on the
 *              product backlog page under the pm section.
 */

//ajax request to populate product backlog table
$.ajax({
    url: "/pm/product-backlog-all",
    method: "get",
    success: function(data, status, requestObj) {
        //If there are no errors then a JSON object with a custom message is sent to the client
        console.log(data);

        var productBacklogTable = $("#product-backlog-table").DataTable({
            data: data,
            columns: [
                {data: "title", title: "Title"},
                {data: "description", title: "Description"},
                {data: "progress", title: "Progress"},
                {data: "priority", title: "Priority"},
                {data: "effort", title: "Effort"},
                {data: "id", title: "ID", visible: false}
            ],
            order: [

            ],
            createdRow: function ( row, data, index ) {
                if(data.progress === "Completed") {
                    //console.log("is completed");
                    console.log(row);
                    console.log($(row).addClass("is-completed"));
                }
            }
        });

        $('#product-backlog-table tbody').on( 'click', 'tr', function () {
            console.log( productBacklogTable.row( this ).data().id );
            window.location.assign("/pm/product-backlog/edit/" + productBacklogTable.row( this ).data().id);
        } );
    },
    error: function(requestObj, status, error) {

        //console.log(requestObj);
        console.log(status);
        console.log(error);
        //Laravel returns a 422 error code if there is a validation error
        //If the error is not of type 422 (Unprocessable Entity) then there is some other issue
        //Then print that error code and the default status that is sent from the framework
        if(requestObj.status == 422) {
            var errors = JSON.parse(requestObj.responseText);
            var errorString = "<ul>";
            $.each(errors, function(error) {
                errorString += "<li>" + errors[error] + "</li>";
            });
            errorString += "</ul>";
            $("#product-backlog-errors").html(errorString);
            $("#product-backlog-errors").show();
        }
        else {
            console.log(requestObj);

            var errorString = "<li>" + requestObj.status + " : "  + requestObj.statusText + "</li>";
            $("#product-backlog-errors").html(errorString);
            $("#product-backlog-errors").show();
        }

    }
});

