/*
    validations are handled here using jQuery Validation Plugin
 */

//custom method to check whether the tag is already there
jQuery.validator.addMethod("alreadyExists", function(value, element) {
    if($('#tag').val() == '') {
        return true;
    }
    return ($('#tagData').val().indexOf($('#tag').val()+'/') == -1);
}, "Tag is already there");

//custom method to check whether all inputs are spaces
jQuery.validator.addMethod("allWhiteSpaces", function(value, element) {
    if($(element).val() == '') {
        return true;
    }
    return ($.trim( $(element).val() ) != '');
}, "Can't have only spaces");

//custom method to check whether all inputs are spaces
jQuery.validator.addMethod("tagsHaveHashes", function(value, element) {
    if($(element).val().indexOf('#') != 0) {
        return true;
    }
}, "Can't start with hashes");

//custom method to check spaces
jQuery.validator.addMethod("hasSpaces", function(value, element) {
    if($(element).val().indexOf(' ') === -1) {
        return true;
    }
}, "Can't have spaces");

//custom method to check whether all inputs are spaces
jQuery.validator.addMethod("tagsHaveTrailingHashes", function(value, element) {
    console.log("Length"  + $(element).val().split('#').length);
    console.log($(element).val().indexOf('#'));
    if($(element).val().split('#').length < 3) {
        return true;
    }
}, "Can't end with more than one hash");

//initializing the validator, rules and custom messages
$().ready(function() {
    $('#tag').keyup(function(){
        if($(this).val().length !=0)
            $('#addButton').attr('disabled', false);
        else
            $('#addButton').attr('disabled',true);
    });

    $('#createProjectForm').validate({
        ignore: ".ignore",
        rules : {
            title : {
                required : true,
                minlength : 5,
                allWhiteSpaces : true
            },
            description : {
                required : true,
                minlength : 20,
                allWhiteSpaces : true
            },
            tags : {
                minlength : 1,
                alreadyExists : true,
                allWhiteSpaces : true,
                tagsHaveHashes : true,
                tagsHaveTrailingHashes : true
            },
            repo_slug : {
                hasSpaces : true
            },
            repo_owner : {
                hasSpaces : true
            }
        },
        messages : {
            title : {
                required : "Title is required"
            },
            description : {
                required : "Description is required"
            }
        },
        errorPlacement : function(error, element) {
            if(element.attr('Name') != 'tags') {
                error.insertAfter(element);
            }
            else if(element.attr('Name') === 'tags') {
                error.insertAfter($('.errorText'));
            }
        }
    });
});

//function to handle add button click and tag-data
$('#addButton').click('onClick',function() {
    if($('#tag').valid()) {
        var id = Math.floor(Math.random() * 90000) + 10000;
        var value = $('#tag').val();
        var close ='<span id="'+ id +'" class="glyphicon glyphicon-remove-circle" name="'+ value +'"></span>';
        var label = '<label id="' + id + '" class="addedTags" name="'+ value +'">#'+ value + close +'</label>';
        var data = $('#tagData').val();

        $('#tag').val('');
        $('#addButton').attr('disabled',true);
        $('#hashTags').append(label);
        $('#tagData').val(data + "#" + value);
    }
});

//to remove tags
$(document).on("click", ".glyphicon.glyphicon-remove-circle", function() {
    var id = $(this).attr('id');
    var value = '#' + $(this).attr('Name');
    $('#'+id).remove();
    $('#tagData').val($('#tagData').val().replace(value, ''));
});

