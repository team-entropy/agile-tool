/**
 * Author: Deleepa
 * Created: 02nd March 2016
 * Description: This JS file contains the code that will control the elements on the
 *              product backlog page under the pm section.
 */

//ajax call to submit user story edits
$("#user-story-edit-submit").click(function(event) {
    event.preventDefault();

    //turns off display of all alerts at first load
    $(".alert").css("display", "none");

    var userStoryData = $("#user-story-edit-form").serialize();

    $.ajax({
        url: "/pm/product-backlog/update",
        method: "post",
        data: userStoryData,
        success: function(data, status, requestObj) {
            //If there are no errors then a JSON object with a custom message is sent to the client
            console.log(data);
            $("#user-story-edit-alert-success").html(data.message);
            $("#user-story-edit-alert-success").show();

        },
        error: function(requestObj, status, error) {

            //console.log(requestObj);
            console.log(status);
            console.log(error);
            //Laravel returns a 422 error code if there is a validation error
            //If the error is not of type 422 (Unprocessable Entity) then there is some other issue
            //Then print that error code and the default status that is sent from the framework
            if(requestObj.status == 422) {
                var errors = JSON.parse(requestObj.responseText);
                var errorString = "<ul>";
                $.each(errors, function(error) {
                    errorString += "<li>" + errors[error] + "</li>";
                });
                errorString += "</ul>";
                $("#user-story-edit-alert-error").html(errorString);
                $("#user-story-edit-alert-error").show();
            }
            else {
                console.log(requestObj);

                var errorString = "<li>" + requestObj.status + " : "  + requestObj.statusText + "</li>";
                $("#user-story-edit-alert-error").html(errorString);
                $("#user-story-edit-alert-error").show();
            }

        }
    });
});

//ajax call to delete selected user story
$("#user-story-delete-submit").click(function(event) {
    event.preventDefault();

    //turns off display of all alerts at first load
    $(".alert").css("display", "none");

    var userStoryData = $("#user-story-edit-form").serialize();

    $.ajax({
        url: "/pm/product-backlog/delete",
        method: "post",
        data: userStoryData,
        success: function(data, status, requestObj) {
            //If there are no errors then a JSON object with a custom message is sent to the client
            console.log(data);
            $("#user-story-edit-alert-success").html(data.message);
            $("#user-story-edit-alert-success").show();

        },
        error: function(requestObj, status, error) {

            //console.log(requestObj);
            console.log(status);
            console.log(error);
            //Laravel returns a 422 error code if there is a validation error
            //If the error is not of type 422 (Unprocessable Entity) then there is some other issue
            //Then print that error code and the default status that is sent from the framework
            if(requestObj.status == 422) {
                var errors = JSON.parse(requestObj.responseText);
                var errorString = "<ul>";
                $.each(errors, function(error) {
                    errorString += "<li>" + errors[error] + "</li>";
                });
                errorString += "</ul>";
                $("#user-story-edit-alert-error").html(errorString);
                $("#user-story-edit-alert-error").show();
            }
            else {
                console.log(requestObj);

                var errorString = "<li>" + requestObj.status + " : "  + requestObj.statusText + "</li>";
                $("#user-story-edit-alert-error").html(errorString);
                $("#user-story-edit-alert-error").show();
            }

        }
    });
});