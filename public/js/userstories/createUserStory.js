$(document).ready(function() {

//dropdown selection change
    $('#us_progress li a').click(function(){
        $('#us_dropdown:first-child').text($(this).text());
        $('#us_dropdown:first-child').val($(this).text());
        var progress = $('#us_dropdown').val();
        $("input[name='progress']").val(progress);
    });

//assign members div visibility
    $('#assignClick').click(function () {
        $('#assignContainer').slideDown(1000);
        $('html,body').animate({scrollTop: $("#scroll").offset().top}, 'slow');
    });

//enbale assign button if member selected
    $("input[name='user_id']").change(function() {
        $('#memberAssign').prop('disabled', false);
    });

//hide assign members div
    $("#memberAssign").click(function () {
        var mem = $('#radioUserId:checked').val();
        $('#userId').val(mem);
        $('#assignContainer').slideUp(1000);
    });

});