/**
 * Created by rifhan on 2/21/16.
 * The editor plugin used in this file can be found @ https://www.tinymce.com/ v4
 * The popver used is from bootstrap v3
 */
//add all event listeners for the DOM here
$(document).ready(function(){
    $(".card").click(cardClickCallback);
    $("#addTaskUS").hide();
    $("#messageGoesHere").hide();
    //popover init
    $('.popover-markup>.trigger').popover({
        html: true,
        title: function () {
            return $(this).parent().find('.head').html();
        },
        content: function () {
            return $(this).parent().find('.content').html();
        }
    });

    $("#markupAddTasks").on('click','Addtasks',addTasksCallback);
    $(".assignUS").click(function(){
        $("#tabIteration").trigger('click')
    });

    $('.selectpicker').selectpicker({
        style: 'btn-info',
        size: 4
    });

    $("#usChange").on('changed.bs.select',function(e){
       var valueChanged = $.trim($(".selectpicker").selectpicker('val'))
       updateUsProgress(valueChanged)
    })

});
//end of adding event listeners


var desc ="";
function cardClickCallback(){
    $("#usDescription").empty();
    $("#loader").show();
    $("#addTaskUS").show();
    $("#AddTaskButton").on('click','AddTasksLink',addTasksCallback);
    var id = arguments[0].currentTarget.id;
    var name = arguments[0].currentTarget.name;

    $("#usTitle").empty();
    $("#usTitle").append(id);
    $("#usTitle").attr("name",name);
    $.get('/pm/sprint-backlog/'+name,null,loadDetailsCallback)
}

function loadDetailsCallback(){
     window.data = JSON.parse(arguments[0]);
    getUserStoryProgress(data["userStories"][0]["id"]);
    $("#loader").hide();
    loadHtml(data)
}
//this method loads all the data when a card click event occurs in the iteration plan tab in pm/sprint-backlog/
//data variable has tasks and user stories details for the relevant selected user story
function loadHtml(data){
    $(".selectpicker").selectpicker('val', $.trim(data["userStories"][0]["progress"]))

    window.desc = data["userStories"][0]["description"];
    window.task = data["tasks"];
    var sprintId;
    if( desc != "") {
        var description = "<div class ='content' style='margin-top: 2em;'>" +


            "<button class='edit-desc btn btn-danger pull-right ' id='edit-description' style='font-size: 0.9em;'>edit description</button><div id='USdesc'>"+desc+"</div>";


        " </div>"
    }else{
        var description = "<div class='content' style='margin-top: 2em;'>" +
            "<button class='content' id='add-description' style='font-size: 0.9em; border: 1px dotted lightgray; width: 100%; height: 3em; border-radius: 0.3em; background-color: inherit;'>click to add description...</button>";

        " </div>"
    }

    if(task[0] != undefined){
        var display = "<table class='table table-hover'><thead style='font-size: 0.9em;'><th>Name</th> <th>Progress</th> <th>Edit</th> <th>Delete</th> ";
        var progressClass = "";
        var n = "task";
        var x = "progress";
        var id = "task_id";
        var us_id = "us_id";
        var dropDown;
        for(var i = 0 ; i < task.length ; i++){
            if(task[i]["progress"] == "Testing" )
                progressClass = "danger";
            else if( task[i]["progress"] == "In Progress" )
                progressClass = "info";
            else if(task[i]["progress"] == "Completed" ) {
                progressClass = "success";
                dropDown = "<td><button class='btn btn-default dropdown-toggle taskStatus' type='button' id='taskStatus-"+i+"'  data-toggle='dropdown' aria-haspopup='true' aria-expanded='true'> "+
                    task[i][x]+
                    "<span class='caret'></span> " +
                    "</button> </td>" +"<td><span class='glyphicon glyphicon-edit' style='cursor: pointer;'  onclick='saveTask(this)' name = "+task[i][id]+'-'+task[i][us_id]+"> </span> </td>" +
                    "<td><span class='glyphicon glyphicon-trash' style='cursor: pointer;'  onclick='deleteTask(this)' name = "+task[i][id]+'-'+task[i][us_id]+"></span></td></tr>"
            }else
                progressClass = "warning"

            if(task[i]["progress"] != "Completed"){
                dropDown = "<td><input type='hidden' id='taskValue' name="+task[i][id]+'-'+task[i][us_id]+"  value=''> " +
                "<div id='dropdownElement' class='dropdown' style=''> " +
                "<button class='btn btn-default dropdown-toggle taskStatus' type='button' id='taskStatus-"+i+"'  data-toggle='dropdown' aria-haspopup='true' aria-expanded='true'> "+
                task[i][x]+
                "<span class='caret'></span> " +
                "</button> " +
                "<ul class='dropdown-menu taskStatusDropDown' id='taskStatusDropDown-"+i+"' aria-labelledby='dropdownMenu1'> " +
                "<li>Completed</li>" +
                "<li>In Progress</li>" +
                "<li>Testing</li>" +
                "</ul> " +
                "</div></td>"+"<td><span class='glyphicon glyphicon-edit' style='cursor: pointer;' hidden="+task[i]["progress"] +" onclick='saveTask(this)' name = "+task[i][id]+'-'+task[i][us_id]+"> </span> </td>" +
                    "<td><span class='glyphicon glyphicon-trash' style='cursor: pointer;'  onclick='deleteTask(this)' name = "+task[i][id]+'-'+task[i][us_id]+"></span></td></tr>"

            }
            //here to
            /* the below code is well associated and tightly coupled with the drop down function Note: when making changes */
            display += "<tr class='"+progressClass+" ' ><td><span  id="+'id'+task[i][id]+'-'+task[i][us_id]+">"+task[i][n]+" </span></td>" + dropDown



        }

        $("#usTasks").empty();
        $("#usTasks").append(display).fadeIn(4000);
        $(function(){
            $(".taskStatusDropDown li ").click(function(element){
                console.log($(this).parent().prev().parent().prev().attr('name'))
                var id = $(this).parent().prev().first()
                $(id).text($(this).text());
                $(id).val($(this).text());
                $("#taskValue").val($(this).text());
                $(id).append("<span style='margin-left: 0.2em' class='caret'></span>")
                changeTaskState($(this).parent().prev().parent().prev().attr('name').split('-') , $(this).text())
            });
        })

    }else{
        $("#usTasks").empty();
    }

    if(data["userStories"][0]["sprint_id"] != ""){
        sprintId = data["userStories"][0]["sprint_id"];
    }else{
        sprintId = 1;
    }


    $("#usDescription").empty();
    $("#usDescription").append(description);
    getAvailableSprints(sprintId,data["userStories"][0]["id"]);

    //bind events to description edit and add
    $(".content").on('click','.edit-desc',function(e){
        descriptionEditor(e,$("#USdesc").next().get(0))
    });

    $(".content").on('click','#add-description',descriptionEditor);
}


// this is function is triggered to add new tasks to user stories
function addTasksCallback(){
    var title = $("#tasksTitle").val()
    var userStoryId = $("#usTitle").attr('name')

    if($.trim(title) == ""){
        $("#taskDisError").empty()
        $("#taskDisError").append("<p class='text-danger'>Enter Tasks Title</p>");
    }else{
        $("#taskDisError").empty()

        var xhr = $.ajax({
            url:"/tasks/store",
            type:"post",
            data:{
                "title" : title,
                "us_id" : userStoryId
            },
            dataType : 'json',
        });
        xhr.success(function(e){
            $("#taskMessages").empty()
            $("#taskMessages").append('<div class="alert alert-success" id="taskMessageAlert" style="margin-top: 0.2em;">' +
            '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' +
            e+
            '</div>').fadeIn(1000).delay(3000).fadeOut(1000);
            $('[name = '+$('#usTitle').attr('name')+']').trigger('click')
            $(".popover-markup>.trigger").popover('hide');
        });
        xhr.error(function(e){
            alert("Error.. Unable to process request please try again");
        });
    }
}
//loads the user stories relevant to the given sprint in /pm/sprint-backlog#sprintView method is called in sprintBacklog.js drop down value trigger method
function loadSprintUserStories(SprintId){
    var id = $.trim(SprintId);
    var xhrReqObject = $.get('/pm/sprint-backlog/sprint/'+id,null,function(data){
        console.log(data)
        loadUserStories(data,"singleSprintView","messageGoesHere")
    });

    xhrReqObject.success(function(){
        //success message goes here
    });

    xhrReqObject.error(function(){
        alert("error! Please ")
    })

}
function loadUserStories(data,viewId,errorId){
    var result = JSON.parse(data);
    $("#"+viewId).empty();
    //$("#errorSprint").empty()
    if(result.length > 0) {
        $("#"+errorId).hide();
        result.forEach(function (value) {
            if ($.trim(value.progress) == "In Progress")
                var progress = "<span class='label label-primary'>" + value.progress + "</span>";
            else if (value.progress == "Testing")
                var progress = "<span class='label label-warning'>" + value.progress + "</span>";
            else
                var progress = "<span class='label label-success'>" + value.progress + "</span>";

            var loadData = "<button class='card card-block'  style='width: 12em; padding: 0.2em; margin: 0.5em; font-size: 0.8em; cursor: pointer;'  id='" + value.title + "'  > <p> <p class='card-text'> " + progress + " <b>" + value.title + "</b> </p>" +
                "<span><cite title='Source Title'>" + value.effort + "</cite> hr</span> </p>" +
                "</button>";
            $("#"+viewId).append(loadData)
        })
    }else{
        $("#"+errorId).show()
    }
}
function descriptionEditor(e,text){
    var editor = "<textarea id='UsDescriptionText'name='UsDescriptionText' style='margin-top: 2em;'></textarea>" +
        "<div id='usDescError'></div><button class='btn btn-default' id='confirmDesc' onclick='addUSDescription(this)' name='"+ e.currentTarget.id + "' style='margin-top: 0.9em;'>Save</button>";
    $("#usDescription").empty();
    $("#usDescription").append(editor);
    tinymce.init({
        selector: '#UsDescriptionText',
        height:100,
        browser_spellcheck: true,
        setup: function(editor) {
            editor.on('keyup',checkWhiteSpaceCallback);
            if($.trim(e.currentTarget.id) == "edit-description" ){
                editor.on("init",function(e){
                    tinyMCE.activeEditor.setContent(new tinyMCE.html.Serializer().serialize(new tinyMCE.html.DomParser().parse(window.desc)));
                })
            }
        }
    })



    if($.trim(e.currentTarget.id) == "edit-description" ){
        console.log(text)
        tinyMCE.activeEditor.setContent(text);
        //set content here solve bug
    }

}
function addUSDescription(element){
    console.trace(element)
    var reqType = element.getAttribute('name');
    var content = tinyMCE.activeEditor.getContent();
    console.log(reqType)
    if(checkWhiteSpaceCallback()){
        var description = new tinyMCE.html.Serializer().serialize(new tinyMCE.html.DomParser().parse(content));
        var id = $("#usTitle").attr("name");
        var data = {
            "description": $.trim(description),
            "id": $.trim(id)
        }
        var url;
        if ($.trim(reqType) == "add-description"){
            url = "/add";
        }else if ($.trim(reqType) == "edit-description"){
            url = "/edit";
        }
        console.log("sending")
        $.ajax({
            url:"/pm/sprint-backlog/desc"+url,
            type:"Put",
            data: data,
            dataType:'json',
            success : function (e){
                $("#usDescription").empty();
                $("#usDescriptionMessages").empty();
                $("#usDescriptionMessages").append('<div class="alert alert-success"  style="margin-top: 4em;"> ' +
                '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' +
                e +
                '</div>').fadeIn(2000).delay(2000).fadeOut(1000);
                setTimeout(callbackToLoadUS($("#usTitle").attr('name')),2000);
            },
            error : function(e){
                console.log(e)
            }
        })

    }
}
function callbackToLoadUS(val){
    $("[name="+val+"]").trigger('click');
}
function checkWhiteSpaceCallback(){
    var text = tinyMCE.activeEditor.getContent({
        format: 'text'
    });

    if ($.trim(text) == '' ){
        $("#usDescError").empty();
        $("#usDescError").append("<p class='text-danger'>You Cant have Empty Description!!</p>");
        return false
    }else{
        $("#usDescError").empty();
        var textWithoutSpaces = $.trim(text);
        if(textWithoutSpaces.length <= 20){
            $("#usDescError").append("<p class='text-danger'>You need to have atleast 20 Characters for the description</p>");
            return false
        }else{
            return true
        }
    }
}

function getAvailableSprints(sprintId,usId){
    var xhr = $.ajax({
        url:"/pm/sprint-backlog/sprints/available",
        type:"get"
    })

    if(sprintId == null){
        sprintId = "Not Assigned Yet";
    }

    xhr.success(function(status){
        var sprints = "";
        var data = JSON.parse(status)

        if($.trim(status) != -1) {
            for(var i = 0 ; i < data.length ; i++){

                sprints += "<li>sprint# "+data[i]["sprint_id"]+"</li>"
            }
            $("#assignSprint").empty();
            $("#assignSprint").append("<label>Current Sprint :</label><div id='dropdownElement' class='dropdown' style=''> " +
            "<input type='hidden' id='sprintValue' value=''>" +
            "<button class='btn btn-default dropdown-toggle taskStatus' type='button'   data-toggle='dropdown' aria-haspopup='true' aria-expanded='true'> Sprint# " +
            sprintId +
            "<span class='caret'></span> " +
            "</button> " +
            "<ul class='dropdown-menu assignSprint' aria-labelledby='dropdownMenu1'> " +
            sprints +
            "</ul> " +
            "</div>");

            $(function () {
                $(".assignSprint li ").click(function (element) {

                    var id = $(this).parent().prev().first()
                    $(id).text($(this).text());
                    $(id).val($(this).text());
                    $("#sprintValue").val($(this).text());
                    $(id).append("<span style='margin-left: 0.2em' class='caret'></span>")
                    //change sprint selected
                    changeSprintForUs($("#sprintValue").val().split('#'),usId);

                });
            })

        }else{
            $("#assignSprint").empty();
            $("#assignSprint").append("<p class='text text-danger'> No Available Sprints</p>");
        }

    })
}

function changeSprintForUs(sprintId,usId){
    var xhr = $.ajax({
        url:"/pm/sprint-backlog/sprints/update",
        type:"put",
        data:{
            "sprint_id": $.trim(sprintId[1]),
            "us_id":usId,
        },
        dataType:"json"
    })
    xhr.success(function(status){
        console.log(status)
    })

    xhr.error(function(e){
        console.log(e)
    })
}

function updateUsProgress(value){
    var taskArr = []
    if(value == $.trim("completed")){
        task.forEach(function(data){
            if($.trim(data["progress"]) != $.trim("Completed")){
                taskArr.push(data)
            }
        })

        if(taskArr.length > 0){
            var message = "<ul>"
            taskArr.forEach(function(data){
                message += "<li>"+ data["task"] + ": <span> "+data["progress"]+"</span>"
            })
            bootbox.alert({
                title : "Complete the Following tasks to Close the User Story as Completed",
                message:message
            })
        }else{
            updateChange(data["userStories"][0]["id"],value)
        }

    }else{
        updateChange(data["userStories"][0]["id"],value)
    }
}

function updateChange(id,value){
    console.log(value)
    var xhr = $.ajax({
                url :"/pm/user-story-progress/edit",
                type:"put",
                data:{
                    "id":id,
                    "value":value
                },

            })
    xhr.success(function(status){
        $('[name = '+$('#usTitle').attr('name')+']').trigger('click')
    })
}