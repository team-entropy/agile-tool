/**
 * Created by rifhan on 2/29/16.
 * this file contains all js related to tasks in a user story, except for task adding which is included
 * in the iteration.js file
 */
//register all my click events here call this file after iteration.js
$(document).ready(function(){

})
//call back for edit task from iteration.js line no 89 registered under .edit-task class


function saveTask(e){
    console.log(e)
    var value = e.getAttribute('name').split('-');
    var text = $("#id"+e.getAttribute('name')).text()
    bootbox.prompt({
        title:"Edit Task",
        value: text,
        closeButton: true,
        size:'small',
        callback: function(result) {

            validateEditTask(result,value);
        }
    });

}

function validateEditTask(val,ids){
    if($.trim(val) == ""){
        $("#taskMessages").empty();
        $("#taskMessages").append('<div class="alert alert-danger" id="taskMessageAlert" style="margin-top: 0.2em;">' +
        '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' +
        'You cant have Empty Title'+
        '</div>').fadeIn(2000).delay(3000).fadeOut(1000);
    }else{
        console.log(ids[0])
        var xhr = $.ajax({
            url:"/tasks/update",
            type:"PUT",
            data:{
                'title': $.trim(val),
                'task_id':ids[0],
                'us_id':ids[1]
            },
            dataType:'json'

        })

        xhr.success(function(e){
            $("#taskMessages").empty();
            $("#taskMessages").append('<div class="alert alert-success" id="taskMessageAlert" style="margin-top: 0.2em;">' +
            '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' +
            e+
            '</div>').fadeIn(2000).delay(3000).fadeOut(1000);
            setTimeout(callbackToLoadUS($("#usTitle").attr('name')),2000);
        })

        xhr.error(function(err){
            $("#taskMessages").empty();
            $("#taskMessages").append('<div class="alert alert-danger" id="taskMessageAlert" style="margin-top: 0.2em;">' +
            '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' +
            err+
            '</div>').fadeIn(2000).delay(3000).fadeOut(1000);

        })
    }
}

function deleteTask(element){
    var value = element.getAttribute('name').split('-');
    bootbox.confirm({
        size: 'small',
        message: "<p class='text text-danger'>Are you sure you want to delete task :</p>" +
        "<p class='text text-primary'>"+$("#id"+element.getAttribute('name')).text()+"</p>",
        callback: function(result){
            validateDeleteTask(result,value);
        }
    })
}
function validateDeleteTask(result,value){
    if(result == true){
        var xhr =  $.ajax({
            url:"/tasks/delete-task",
            type:"delete",
            data:{
                "us_id":value[1],
                "task_id":value[0]
            },
            dataType:'json'
        })
        xhr.success(function(message){
            $("#taskMessages").empty();
            $("#taskMessages").append('<div class="alert alert-success" id="taskMessageAlert" style="margin-top: 0.2em;">' +
            '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' +
            message+
            '</div>').fadeIn(2000).delay(3000).fadeOut(1000);
            setTimeout(callbackToLoadUS($("#usTitle").attr('name')),2000);
        })

        xhr.error(function(error){
            $("#taskMessages").empty();
            $("#taskMessages").append('<div class="alert alert-success" id="taskMessageAlert" style="margin-top: 0.2em;">' +
            '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' +
            error+
            '</div>').fadeIn(2000).delay(3000).fadeOut(1000);

        })
    }
}

function changeTaskState(name,state){
    console.log(name[0])
    var xhr = $.ajax({
        url:"/tasks/status/progress",
        type:"PUT",
        data:{
            "us_id":name[1],
            "task_id":name[0],
            "progress": $.trim(state)
        },
        dataType:'json'
    })

    xhr.success(function(status){
        if($.trim(status) == "success" || $.trim(status)== "0"){
            setTimeout(callbackToLoadUS($("#usTitle").attr('name')),2000);
        }else{
            bootbox.alert({
                size: 'small',
                message: status,
                callback: function(){ /* your callback code */ }
            })
        }
    })

    xhr.error(function(error){
        bootbox.alert({
            size: 'small',
            message: "hello",
            callback: function(){ /* your callback code */ }
        })
    })

}

function getUserStoryProgress(usId){
    var xhr =$.ajax({
        url:"/tasks/progress/"+usId,
        type:"get"
    })
    xhr.success(function(status){
        if($.trim(status) != -1){

            $("#usProgress").empty();
            $("#usProgress").append("<div><p class='text text-primary'>Completion Percentage of User Story</p>" +
            "<div class='c100 p"+parseInt(status)+" green' style='margin-top: 30px'> " +
            "<span>"+parseInt(status)+"%</span> " +
            "<div class='slice'> " +
            "<div class='bar'></div> " +
            "<div class='fill'></div> " +
            "</div></div></div>");
        }
    })
}
