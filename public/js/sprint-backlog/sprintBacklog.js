/**
 * Created by rifhan on 1/21/16.
 */
/* plugin used for the date picker - https://github.com/eternicode/bootstrap-datepicker */

 $(document).ready(function() {
     var now = new Date();
     var today = (now.getMonth()+1)+"/"+(now.getDate())+"/"+now.getFullYear();
     $(function(){
         $("#scrumMasterDropDown li ").click(function(){
             $("#scrum:first-child").text($(this).text());
             $("#scrum:first-child").val($(this).text());
             $("#scrumMaster").val($(this).text());
             $("#scrum").append("<span style='margin-left: 0.2em' class='caret'></span>")
         });
     })
     $(function(){
         $("#sprintDropDown li ").click(function(){
             $("#sprintNo:first-child").text($(this).text());
             $("#sprintNo:first-child").val($(this).text());
             $("#sprintValue").val($(this).text());
             $("#sprintNo").append("<span style='margin-left: 0.2em' class='caret'></span>")
             var value = $(this).attr("id")
             dataUpdate(value)
         });
     })
     $('#start_date').datepicker({
            startDate:today,
            autoclose:true,
            clearBtn:true,
            keyboardNavigation:true,


     })
     $('#end_date ').datepicker({
            autoclose:true,
            clearBtn:true,
            keyboardNavigation:true
     })

     $("#start_date").on("changeDate",function(){
         var startDate = $("#start_date").datepicker("getFormattedDate");
         $("#end_date").datepicker("setStartDate",startDate);
     })

    //checking if dates are null


 });

//this method validates the sprint date fields

