<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    protected $table = 'project';
    protected $primaryKey = 'id';
    protected $fillable = ['title', 'project_owner', 'description', 'technology' , 'created_on', 'repo_slug', 'repo_owner'];
    public  $timestamps = false;

}
