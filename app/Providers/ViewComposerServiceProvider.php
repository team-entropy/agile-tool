<?php

namespace App\Providers;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\ServiceProvider;
use Auth;

class ViewComposerServiceProvider extends ServiceProvider
{
    private $userData;
    private $assignedProjectId;
    private $projectId;
    private $editRequestCount;
    private $deleteRequestCount;

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('includes.sidebar',function($view){

            if(Auth::User()) {
                $this->userData = Auth::User()->type;
                $this->assignedProjectId = $this->getAssignedProjectId();
                $this->projectId = Session::get('project_id');
                $this->editRequestCount = $this->getEditRequestCount($this->projectId);
                $this->deleteRequestCount = $this->getDeleteRequestCount($this->projectId);
            }
            $view->with(['userData' => $this->userData,
                'assignedProjectId' => $this->assignedProjectId,
                'editRequestCount' => $this->editRequestCount,
                'deleteRequestCount' => $this->deleteRequestCount,
                'projectId' => $this->projectId]);

        });

    }

    /**
     * @return mixed
     */
    private function getAssignedProjectId() {
        $projectId = DB::table('users')
            ->where('email', Auth::user()->email)
            ->select('project_id')
            ->get();
        return $projectId[0]->project_id;
    }

    /**
     * @param $id
     * @return mixed
     * @description counts the number of pending edit user story requests
     */
    private function getEditRequestCount($id) {
        $editRequestCount = DB::table('edit_us_request')
            ->where(['type' =>'pending', 'project_id' => $id])
            ->count();

        return  $editRequestCount;
    }

    /**
     * @param $id
     * @return mixed
     * @description counts the number of pending delete user story requests
     */
    private function getDeleteRequestCount($id) {
        $deleteRequestCount = DB::table('delete_us_request')
            ->where(['type' =>'pending', 'project_id' => $id])
            ->count();

        return  $deleteRequestCount;
    }

    private function showProjectManagerSidebar() {
        $type = Session::get('type');
        if($type == 'pm') {
            view()->composer('includes.partials.dynamicSidebar.pm', function ($view) {
                $projectId = Session::get('project_id');
                $editRequestCount = $this->getEditRequestCount($projectId);
                $deleteRequestCount = $this->getDeleteRequestCount($projectId);

                $view->with([
                    'editRequestCount' => $editRequestCount,
                    'deleteRequestCount' => $deleteRequestCount,
                    'projectId' => $projectId
                ]);
            });
        }
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }


}
