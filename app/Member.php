<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Member extends Model
{
    public $table = 'member';
    protected $primaryKey = 'member_id';
    public $timestamps = false;
    protected  $fillable = [
        'name'
    ];

}
