<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Redirect;

class ClientAnalyticsController extends Controller
{
    //
    private $_projectId = 1;
    const ErrorValue = -1;
    const UsCheck = "completed";
    const zeroValue = 0;
    private $_userId;

    /**
     * ClientAnalyticsController constructor.
     * @internal param $id
     */
    public function __construct()
    {
        $this->_projectId = Session::get('project_id');

        //check session set else redirect to projects page
        if(empty($this->_projectId)){
            //send flash message to select a project
            Redirect::to('/client')->send();
        }else{
            $this->_userId = Auth::user()->id;
        }
    }

    /**
     * @description this method returns the view for the project progress graph
     */
    public function projectProgressTimeLine(){
        try{
            $startDate = $this->getProjectStartDate();
            $endDate = $this->getProjectEndDate();
            $start = Carbon::parse($startDate);
            $end = Carbon::parse($endDate);
            $current = $start->diffInDays(Carbon::today());

            $projectDetails = [
              "start_date" => $start->format('d-M-Y'),
              "end_date" => $end->format('d-M-Y'),
              "current" => Carbon::today()->format('d-M-Y'),
              "no_of_days" => $current,
            "total_days" => $start->diffInDays($end),
                "today" => Carbon::today()
            ];

            if($startDate != self::ErrorValue || $endDate != self::ErrorValue) {
                return json_encode($projectDetails);
            }else{
                return self::ErrorValue;
                //return view with project hasnt started yet
            }
        }catch(QueryException $exception){
            $exceptionData['user_id'] = $this->userId;
            $exceptionData['exception'] = $exception->getMessage();
            $exceptionData['time'] = Carbon::now()->toDateTimeString();

            return abort(500,"Cannot Access Database Please Try again \n".$exception);
        }
    }

    /**
     * @return int
     * @internal param $id
     * @description this method returns the end date of the project by querying the max date out of
     * sprints
     */
    public function getProjectEndDate(){
        try{
            $projectEndDate = DB::table('sprint')->where('project_id','=',$this->_projectId)->max('end_date');
            if($projectEndDate != null){
                return $projectEndDate;
            }else{
                return self::ErrorValue;
            }
        }catch(QueryException $exception){
            $exceptionData['user_id'] = $this->userId;
            $exceptionData['exception'] = $exception->getMessage();
            $exceptionData['time'] = Carbon::now()->toDateTimeString();

            return abort(500,"Cannot Access Database Please Try again \n".$exception);
        }
    }


    /**
     * @return int|void
     * @description this method returns the start date of the project
     */
    public function getProjectStartDate(){
        try{
            $projectEndDate = DB::table('sprint')->where('project_id','=',$this->_projectId)->min('start_date');
            if($projectEndDate != null){
                return $projectEndDate;
            }else{
                return self::ErrorValue;
            }
        }catch(QueryException $exception){
            $exceptionData['user_id'] = $this->userId;
            $exceptionData['exception'] = $exception->getMessage();
            $exceptionData['time'] = Carbon::now()->toDateTimeString();

            return abort(500,"Cannot Access Database Please Try again \n".$exception);
        }
    }

    /**
     * @return float|int|void
     * @description this method returns the total completion of the project as a percentage taking completed
     * user stories against total user stories to account.
     */
    public function projectCompletionPercentage(){
        try{
            $totalCompleted = DB::table('user_story')->where('project_id','=',$this->_projectId)
                ->where('progress','=',self::UsCheck)->count();
            $total = DB::table('user_story')->where('project_id','=',$this->_projectId)
                ->count();
            if($total != self::zeroValue ){
                return ($totalCompleted / $total) * 100.0;
            }else{
                return self::ErrorValue;
            }
        }catch(QueryException $exception){
            $exceptionData['user_id'] = $this->userId;
            $exceptionData['exception'] = $exception->getMessage();
            $exceptionData['time'] = Carbon::now()->toDateTimeString();

            return abort(500,"Cannot Access Database Please Try again \n".$exception);
        }
    }

    /**
     *
     * @description return data for burndown chart
     */
    public function burnDownChartData(){
        //graphs data model
        $data = collect();
        try{
            $totalEffort = DB::table('user_story')->where('project_id','=',$this->_projectId)->sum('effort');
            //start date as the 1st value in the y axis is the project start date and x value is total effort hrs
            $arr = [$this->getProjectStartDate(),(int)$totalEffort];
            $data->push($arr);

            $completedUs = DB::table('user_story_meta_data')
                ->join('user_story','user_story.id','=','user_story_meta_data.user_story_id')
                ->select('user_story_meta_data.time_changed','user_story.effort')
                ->get();
            //add each timestamp and totaleffort - effort to the graph data model
            foreach($completedUs as $value){
                $array = [$value->time_changed,($totalEffort -= $value->effort)];
                $data->push($array);
            }
            //checks if the end date is already in the data error
            if(!$this->checkIfExist(Carbon::parse($this->getProjectEndDate()))){
                $temp = [$this->getProjectEndDate(),null];
                $data->push($temp);
                $temp = [null,0];
                $data->push($temp);
            }

            return json_encode($data);
        }catch(QueryException $exception){
            dd($exception);
        }
    }

    /**
     * @param Carbon $date
     * @return bool
     * @return bool
     * @description checks if any completed user stories exist on the given date inorder to add a cumulative
     */
    public function checkIfExist(Carbon $date){
        try{
            $query = DB::table('user_story_meta_data')->where('time_changed','=',$date)->get();

            if(!empty($query)){
                return true;
            }else{
                return false;
            }
        }catch(QueryException $exception){

        }
    }

}
