<?php
namespace App\Http\Controllers;

use Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use App\Project;
use App\UserStory;
use App\ExceptionsLog;
use App\Http\Requests;

/**
 * Class ProjectsController
 * @package App\Http\Controllers
 * @description handling all CRUD operations and actions related to projects
 */
class ProjectsController extends Controller
{
    private $userId;
    private $notification;

    /**
     * ProjectsController constructor.
     */
    public function __construct(NotificationController $notificationController)
    {
        $this->userId = Auth::user()->id;
        $this->notification = $notificationController;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|
     * \Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function index()
    {
        $projects = Project::where('project_owner', $this->userId)
            ->get();
        Session::forget('project_id');
        if($projects->isEmpty()) {
            return redirect('/pm/projects/create');
        }
        else {
            return view('projects.index', compact('projects'));
        }
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('projects.create');
    }

    /**
     * @param $id
     * @return int
     * @description calculates the project progress by looking at user stories
     */
    private function calcProjectProgress($id)
    {
        $userStory = UserStory::where('project_id', $id)->get();
        $count = 0;
        $completed = 0;
        foreach($userStory as $story) {
            $progress = $story->progress;
            if($progress == 'completed') {
                $completed++;
            }
            $count++;
        }
        if($count > 0) {
            return (integer)($completed/$count*100);
        } else {
            return 0;
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|void
     * @description shows the view composed with project details and pending requests
     */
    public function show($id)
    {
        Session::put('project_id',$id);
        try {
            $project = Project::where('project_owner', $this->userId)
                ->where('id', $id)
                ->first();

            $developerRequests = DB::table('project_request')
                ->join('users', 'users.id', '=', 'project_request.user_id')
                ->where(['project_request.project_id' => $id, 'project_request.type' => 'pending'])
                ->get(array('project_request.*', 'users.name', 'users.technology_tags'));

            if (!empty($project->id)) {
                //project progress calculation function call
                $projectPercentage = $this->calcProjectProgress($id);

                return view('projects.show', compact('userStory',
                    'project',
                    'projectPercentage',
                    'developerRequests'));
            } else {
                return abort(403, 'Requested Project is not available');
            }
        } catch(\Exception $exception){
            $exceptionData['user_id'] = $this->userId;
            $exceptionData['exception'] = $exception->getMessage();
            $exceptionData['time'] = Carbon::now()->toDateTimeString();

            ExceptionsLog::create($exceptionData);
        }
    }

    /**
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @description stores information with the timestamp
     */
    public function store()
    {
        $input = Request::all();
        $input['created_on'] = Carbon::now()->format('Y-m-d');
        $input['project_owner'] = $this->userId;

        try {
            Project::create($input);
        } catch(\Exception $exception){
            $exceptionData['user_id'] = $this->userId;
            $exceptionData['exception'] = $exception->getMessage();
            $exceptionData['time'] = Carbon::now()->toDateTimeString();

            ExceptionsLog::create($exceptionData);
        }

        return redirect('/pm');
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @description updates project details
     */
    public function update($id)
    {
        try {
            $project = Project::findOrFail($id);
            $project->update(Request::all());

            return redirect('/pm');
        } catch(\Exception $exception){
            $exceptionData['user_id'] = $this->userId;
            $exceptionData['exception'] = $exception->getMessage();
            $exceptionData['time'] = Carbon::now()->toDateTimeString();

            ExceptionsLog::create($exceptionData);
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @description redirects to the edit page with relevant projects data
     */
    public function edit($id)
    {
        $project = Project::findOrFail($id);
        return view('projects.edit',compact('project'));
    }

    /**
     * @return mixed
     * @description deletes selected project
     */
    public function delete()
    {
        $input = Input::all();
        try {
            $project = Project::findOrFail($input['id']);
            $project->delete();

            return $input['id'];
        } catch(\Exception $exception){
            $exceptionData['user_id'] = $this->userId;
            $exceptionData['exception'] = $exception->getMessage();
            $exceptionData['time'] = Carbon::now()->toDateTimeString();

            ExceptionsLog::create($exceptionData);
        }
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|void
     * @internal param $id
     * @description displays pending delete user story requests
     */
    public function showDeleteRequests()
    {
        $id = Session::get('project_id');
        try {
            $project = Project::where('project_owner', $this->userId)
                ->where('id', $id)
                ->first();

            if (!empty($project->id)) {
                $deleteRequests = DB::table('delete_us_request')
                    ->join('users', 'users.id', '=', 'delete_us_request.user_id')
                    ->where(['delete_us_request.project_id' => $id, 'delete_us_request.type' => 'pending'])

                    ->get(array('delete_us_request.*', 'users.name', 'users.id AS user_id'));

                return view('projects.requests.delete.show', compact('project', 'deleteRequests'));
            } else {
                return abort(403, 'Requested Project is not available!');
            }
        } catch(\Exception $exception){
            $exceptionData['user_id'] = $this->userId;
            $exceptionData['exception'] = $exception->getMessage();
            $exceptionData['time'] = Carbon::now()->toDateTimeString();

            ExceptionsLog::create($exceptionData);
        }
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|void
     * @internal param $id
     * @description displays pending edit user story requests
     */
    public function showEditRequests()
    {
        $id = Session::get('project_id');
        try {
            $project = Project::where('project_owner', $this->userId)
                ->where('id', $id)
                ->first();

            if (!empty($project->id)) {
                $editRequests = DB::table('edit_us_request')
                    ->join('users', 'users.id', '=', 'edit_us_request.user_id')
                    ->where(['edit_us_request.project_id' => $id, 'edit_us_request.type' => 'pending'])

                    ->get(array('edit_us_request.*', 'users.name', 'users.id AS user_id'));

                return view('projects.requests.edit.show', compact('project', 'editRequests'));
            } else {
                return abort(403, 'Requested Project is not available!');
            }
        } catch(\Exception $exception){
            $exceptionData['user_id'] = $this->userId;
            $exceptionData['exception'] = $exception->getMessage();
            $exceptionData['time'] = Carbon::now()->toDateTimeString();

            ExceptionsLog::create($exceptionData);
        }
    }

    /**
     * @return mixed
     * @description updates delete user story requests
     */
    public function updateDeleteRequest()
    {
        $projectId = Session::get('project_id');
        $input = Input::all();
        try {
            DB::table('delete_us_request')
                ->where(['id' => $input['id'], 'project_id' => $projectId])
                ->update(['type' => $input['type'],
                    'reason' => $input['deleteReason']]);

            if ($input['type'] === 'approved') {
                $this->notification->addNotification($projectId, 0, $input['developer'], 'confirm-user-story-delete-request');
            } else if ($input['type'] === 'declined') {
                $this->notification->addNotification($projectId, 0, $input['developer'], 'decline-user-story-delete-request');
            }

            return $input;
        } catch(\Exception $exception){
            $exceptionData['user_id'] = $this->userId;
            $exceptionData['exception'] = $exception->getMessage();
            $exceptionData['time'] = Carbon::now()->toDateTimeString();

            ExceptionsLog::create($exceptionData);
        }
    }

    /**
     * @return mixed
     * @description updates project requests
     */
    public function updateProjectRequest()
    {
        $projectId = Session::get('project_id');
        $input = Input::all();
        try {
            DB::table('project_request')
                ->where(['user_id' => $input['userId'], 'project_id' => $projectId])
                ->update(['type' => $input['type'],
                    'reason' => $input['deleteReason']]);

            if ($input['type'] == 'approved') {
                DB::table('users')
                    ->where(['id' => $input['userId']])
                    ->update(['project_id' => $projectId]);


                $this->notification->addNotification($projectId, 0, $input['userId'], 'confirm-project-request');

            } else if ($input['type'] == 'declined') {
                $this->notification->addNotification($projectId, 0, $input['userId'], 'decline-project-request');
            }

            return $input;
        } catch(\Exception $exception){
            $exceptionData['user_id'] = $this->userId;
            $exceptionData['exception'] = $exception->getMessage();
            $exceptionData['time'] = Carbon::now()->toDateTimeString();

            ExceptionsLog::create($exceptionData);
        }
    }

    /**
     * @return mixed
     * @description updates edit user story requests
     */
    public function updateEditRequest()
    {
        $projectId = Session::get('project_id');
        $input = Input::all();
        try {
            DB::table('edit_us_request')
                ->where(['id' => $input['id'], 'project_id' => $projectId])
                ->update(['type' => $input['type'],
                    'reason' => $input['deleteReason']]);

            if ($input['type'] === 'approved') {
                if ($input['field'] === 'desc') {
                    $input['field'] = 'description';
                }
                DB::table('user_story')
                    ->where('id', $input['userStoryId'])
                    ->update([$input['field'] => $input['editedValue']]);

                $this->notification->addNotification($projectId, $input['userStoryId'], $input['developer'], 'confirm-user-story-edit-request');
            } else if ($input['type'] === 'declined') {
                $this->notification->addNotification($projectId, $input['userStoryId'], $input['developer'], 'decline-user-story-edit-request');
            }
            return $input;
        } catch(\Exception $exception){
            $exceptionData['user_id'] = $this->userId;
            $exceptionData['exception'] = $exception->getMessage();
            $exceptionData['time'] = Carbon::now()->toDateTimeString();

            ExceptionsLog::create($exceptionData);
        }
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|void
     * @description shows available team members that can be assigned to a project
     */
    public function showAvailableMembers() {
        $projectId = Session::get('project_id');
        try {
            $project = Project::where('project_owner', $this->userId)
                ->where('id', $projectId)
                ->first();

            if (!empty($project->id)) {
                $availableMembers = DB::table('users')
                    ->where(['project_id' => null,
                        'type' => 'dev'])
                    ->get();

                return view('projects.team.add.show', compact('project',
                    'availableMembers'));
            } else {
                return abort(403, 'Requested Project is not available!');
            }
        } catch(\Exception $exception){
            $exceptionData['user_id'] = $this->userId;
            $exceptionData['exception'] = $exception->getMessage();
            $exceptionData['time'] = Carbon::now()->toDateTimeString();

            ExceptionsLog::create($exceptionData);
        }
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|void
     * @description shows assigned team members to the selected project
     */
    public function showTeamMembers()
    {
        $projectId = Session::get('project_id');
        try {
            $project = Project::where('project_owner', $this->userId)
                ->where('id', $projectId)
                ->first();

            if (!empty($project->id)) {
                $teamMembers = DB::table('users')
                    ->where(['project_id' => $projectId])
                    ->get();

                return view('projects.team.delete.show', compact('project',
                    'teamMembers'));
            } else {
                return abort(403, 'Requested Project is not available!');
            }
        } catch(\Exception $exception){
            $exceptionData['user_id'] = $this->userId;
            $exceptionData['exception'] = $exception->getMessage();
            $exceptionData['time'] = Carbon::now()->toDateTimeString();

            ExceptionsLog::create($exceptionData);
        }
    }

    /**
     * @return mixed
     * @description updates details about team details
     */
    public function updateTeamDetails()
    {
        $projectId = Session::get('project_id');
        $input = Input::all();
        try {
            if ($input['type'] == 'add') {
                DB::table('users')
                    ->where(['id' => $input['userId']])
                    ->update(['project_id' => $projectId]);
                
                $user = DB::table('users')
                    ->where(['id' => $input['userId']])
                    ->first();

                $this->notification->addNotification($projectId, 0, $input['userId'], 'add-developer');
                $this->mailMethod('mail.add-user' , $user);

            } elseif ($input['type'] == 'remove') {
                DB::table('users')
                    ->where(['id' => $input['userId']])
                    ->update(['project_id' => null]);

                $this->notification->addNotification($projectId, 0, $input['userId'], 'remove-developer');
            }

            return $input;
        } catch(\Exception $exception){
            $exceptionData['user_id'] = $this->userId;
            $exceptionData['exception'] = $exception->getMessage();
            $exceptionData['time'] = Carbon::now()->toDateTimeString();

            ExceptionsLog::create($exceptionData);
        }
    }

    /**
     * @param $path
     * @param $user
     * @description sending mail to the relevant developer upon project detail update
     */
    private function mailMethod($path,$user) {
        $data = array(
            'name' => $user->name,
            'time' => Carbon::now(),
        );

        try {
            Mail::send($path, $data, function ($message) {
                $message->from('sliitentropy@gmail.com', 'Agile Management');
                $message->to('chamara654@gmail.com')->subject('Project Notification');
            });
        } catch(\Exception $exception){
            $exceptionData['user_id'] = $this->userId;
            $exceptionData['exception'] = $exception->getMessage();
            $exceptionData['time'] = Carbon::now()->toDateTimeString();

            ExceptionsLog::create($exceptionData);
        }
    }
}
