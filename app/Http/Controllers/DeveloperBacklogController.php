<?php
namespace App\Http\Controllers;


use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use App\Http\Requests;
use App\ExceptionsLog;
use Carbon\Carbon;

/**
 * Class DeveloperBacklogController
 * @package App\Http\Controllers
 * @description handling product backlog actions
 */
class DeveloperBacklogController extends Controller
{
    //logged in users email, working project, user id should be assigned
    private $user;
    private $projectId;
    private $userId;
    //NotificationController implementation
    private $notification;

    /**
     * @description assigning user's email and project id
     * @param NotificationController $notificationController
     */
    public function __construct(NotificationController $notificationController)
    {
        $this->user = Auth::user()->email;
        $this->userId = Auth::user()->id;
        $this->notification = $notificationController;

        try {
            $this->projectId = DB::table('users')
                ->where('email', $this->user)
                ->select('project_id')
                ->get();
        } catch(\Exception $exception) {
            $exceptionData['user_id'] = $this->userId;
            $exceptionData['exception'] = $exception->getMessage();
            $exceptionData['time'] = Carbon::now()->toDateTimeString();

            ExceptionsLog::create($exceptionData);
        }

        if ($this->projectId[0]->project_id == null) {
            Redirect::to('/dev/project')->send();
        }
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @description displaying user stories in product backlog
     */
    public function index()
    {
        try {
            //logged in users' projects' user stories
            $userStories = DB::table('user_story')
                ->join('users', 'user_story.user_id', '=', 'users.id')
                ->select('user_story.*', 'users.name', 'users.email')
                ->orderBy('user_story.id')
                ->get();

            //project name of the user assigned with
            $projectName = DB::table('project')
                ->join('users', 'project.id', '=', 'users.project_id')
                ->select('project.title')
                ->get();

            $user = $this->user;

            return view('dev.product-backlog.index', compact('userStories', 'projectName', 'user'));
        } catch (\Exception $exception) {
            $exceptionData['user_id'] = $this->userId;
            $exceptionData['exception'] = $exception->getMessage();
            $exceptionData['time'] = Carbon::now()->toDateTimeString();

            ExceptionsLog::create($exceptionData);
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @description displaying selected individual user story
     */
    public function show($id)
    {
        try {
            //selected user story details
            $userStory = DB::table('user_story')
                ->join('users', 'user_story.user_id', '=', 'users.id')
                ->where('user_story.id', $id)
                ->select('user_story.*', 'users.name', 'users.email')
                ->get();

            if (empty($userStory[0])) {
                return abort(403, 'Requested User Story is not available');
            } else {
                //project name of the user assigned with
                $projectName = DB::table('project')
                    ->join('users', 'project.id', '=', 'users.project_id')
                    ->select('project.title', 'project.project_owner', 'project.id')
                    ->get();

                $fileNames = explode('::', $userStory[0]->filename);

                return view('dev.product-backlog.show', compact('userStory', 'projectName', 'fileNames'));
            }
        } catch(\Exception $exception) {
            $exceptionData['user_id'] = $this->userId;
            $exceptionData['exception'] = $exception->getMessage();
            $exceptionData['time'] = Carbon::now()->toDateTimeString();

            ExceptionsLog::create($exceptionData);
        }
    }

    /**
     * @param $id
     * @return string
     * @description storing user story edit requests
     */
    public function store($id)
    {
        //get all edited inputs from AJAX
        $request = Input::all();

        try {
            $result = DB::table('edit_us_request')
                ->where('us_id', $request['us_id'])
                ->where('user_id', $request['user_id'])
                ->where('field', $request['field'])
                ->where('type', 'pending')
                ->get();

            //updating the previous edited value with new if type is pending
            if ($result) {
                DB::table('edit_us_request')
                    ->where('id', $result[0]->id)
                    ->update(['edited_value' => $request['edited_value']]);
                return 'Edit Request updated successfully!';
            } else {
                $request['requested_on'] = Carbon::now()->toDateString();
                //insert edit user story request details
                DB::table('edit_us_request')->insert($request);
                //insert notification
                $this->notification->addNotification($request['project_id'], $request['us_id'],
                    $request['pm_id'], 'request-edit_user_story');
                return 'Edit Request was successful!';
            }

        } catch (\Exception $exception) {
            $exceptionData['user_id'] = $this->userId;
            $exceptionData['exception'] = $exception->getMessage();
            $exceptionData['time'] = Carbon::now()->toDateTimeString();

            ExceptionsLog::create($exceptionData);
        }
    }

    /**
     * @param $id
     * @return string
     * @description storing user story delete requests
     */
    public function storeDel($id)
    {
        //get delete user story request inputs from AJAX
        $request = Input::all();

        try {
            $result = DB::table('delete_us_request')
                ->where('us_id', $request['us_id'])
                ->where('user_id', $request['user_id'])
                ->where('type', 'pending')
                ->get();

            if ($result) {
                return 'Already a User Story delete request has been sent, Please remain patience!';
            } else {
                $request['requested_on'] = Carbon::now()->toDateString();
                //insert edit user story request details
                DB::table('delete_us_request')->insert($request);
                //insert notification
                $this->notification->addNotification($request['project_id'], $request['us_id'],
                    $request['pm_id'], 'request-delete_user_story');
                return 'User Story Delete request was successful!';
            }
        } catch (\Exception $exception) {
            $exceptionData['user_id'] = $this->userId;
            $exceptionData['exception'] = $exception->getMessage();
            $exceptionData['time'] = Carbon::now()->toDateTimeString();

            ExceptionsLog::create($exceptionData);
        }
    }
}
