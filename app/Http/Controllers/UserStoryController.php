<?php
namespace App\Http\Controllers;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Http\Requests;
use App\ExceptionsLog;

/**
 * Class UserStoryController
 * @package App\Http\Controllers
 * @description handling all CRUD operations and actions related to create user story
 */
class UserStoryController extends Controller
{
    //logged in users email should be assigned
    private $user;
    //if pm set this value to selected project
    private $projectId;
    //logged in users type(pm/dev/client)
    private $userType;
    private $userId;

    /**
     * @description assigning logged in user's email and assigning project id according to the user type
     */
    public function __construct()
    {
        $this->user = Auth::user()->email;
        $this->userType = Auth::user()->type;
        $this->userId = Auth::user()->id;

        //set pm selected project id if the user is pm
        if ($this->userType == 'pm') {
            $this->projectId = Session::get('project_id');
            if($this->projectId == null) {
                Redirect::to('/pm')->send();
            }
        } else {
            try {
                $projectId = DB::table('users')
                    ->where('email', $this->user)
                    ->select('project_id')
                    ->get();
                if ($projectId[0]->project_id == null) {
                    Redirect::to('/dev/project')->send();
                }
            } catch (\Exception $exception) {
                $exceptionData['user_id'] = $this->userId;
                $exceptionData['exception'] = $exception->getMessage();
                $exceptionData['time'] = Carbon::now()->toDateTimeString();

                ExceptionsLog::create($exceptionData);
            }
        }
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @description display create user story view according to logged in user type
     */
    public function create()
    {
        try {
            if ($this->userType == 'pm') {
                //retrieve developers who are working on the PM selected project
                $members = DB::table('users')
                    ->join('project', 'users.project_id', '=', 'project.id')
                    ->where('type', 'dev')
                    ->where('users.project_id', $this->projectId)
                    ->select('users.*')
                    ->get();
            } else {
                //retrieve developers who are working on the same project as of the logged in developer
                $members = DB::table('users')
                    ->join('project', 'users.project_id', '=', 'project.id')
                    ->where('type', 'dev')
                    ->where('users.project_id', Auth::user()->project_id)
                    ->select('users.*')
                    ->get();
            }

            return view('userstories.create', compact('members'));
        } catch (\Exception $exception) {
            $exceptionData['user_id'] = $this->userId;
            $exceptionData['exception'] = $exception->getMessage();
            $exceptionData['time'] = Carbon::now()->toDateTimeString();

            ExceptionsLog::create($exceptionData);
        }
    }

    /**
     * @return Redirect
     * @description storing created user story in to database
     */
    public function store()
    {
        //get input details to store
        $userStory = Request::except('_token', 'upload', 'userId');

        if ($this->userType == 'pm') {
            $userStory['project_id'] = $this->projectId;
        } else {
            $projectId = Auth::user()->project_id;
            $userStory['project_id'] = $projectId;
        }

        //get the file names of the uploaded files
        $filename = $this->fileHandling(Input::file('upload'));
        $userStory['filename'] = $filename;

        try {
            //insert created user story details while checking it is success
            if (DB::table('user_story')->insert($userStory)) {
                //move file to the provided location
                foreach (Input::file('upload') as $file) {
                    if ($file != null) {
                        $filename = $file->getClientOriginalName();
                        $file->move('public/uploads', $filename);
                    }
                }
            }
        } catch (\Exception $exception) {
            $exceptionData['user_id'] = $this->userId;
            $exceptionData['exception'] = $exception->getMessage();
            $exceptionData['time'] = Carbon::now()->toDateTimeString();

            ExceptionsLog::create($exceptionData);
        }

        if ($this->userType == 'pm'){
            return redirect('/pm/user-story/create');
        } elseif($this->userType == 'dev') {
            return redirect('/dev/user-story/create');
        }
    }

    /**
     * @param $uploads
     * @return null|string
     * @description checking whether there are any uploaded files and and concatenating into a single name
     */
    private function fileHandling($uploads) {
        $filename = null;

        //assign names to the uploaded files
        foreach ($uploads as $file) {
            if ($file != null) {
                $filename = $filename . "::" . $file->getClientOriginalName();
            }
        }

        return $filename;
    }

}
