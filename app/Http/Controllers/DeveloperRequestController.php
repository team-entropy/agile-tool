<?php
namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests;
use App\ExceptionsLog;
use Carbon\Carbon;

/**
 * Class DeveloperRequestController
 * @package App\Http\Controllers
 * @description handling logged in user request views
 */
class DeveloperRequestController extends Controller
{
    //logged in users email, user id, working project should be assigned
    private $user;
    private $projectId;
    private $userId;

    /**
     * @description assigning logged in user's email and the project working with
     */
    public function __construct()
    {
        $this->user = Auth::user()->email;
        $this->userId = Auth::user()->id;
        try {
            $this->projectId = DB::table('users')
                ->where('email', $this->user)
                ->select('project_id')
                ->get();
        } catch (\Exception $exception) {
            $exceptionData['user_id'] = $this->userId;
            $exceptionData['exception'] = $exception->getMessage();
            $exceptionData['time'] = Carbon::now()->toDateTimeString();

            ExceptionsLog::create($exceptionData);
        }
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @description displaying requests related to logged in user
     */
    public function index()
    {
        try {
            //project requested details
            $projectReq = DB::table('project_request')
                ->join('users', 'project_request.pm_id', '=', 'users.id')
                ->join('project', 'project_request.project_id', '=', 'project.id')
                ->where('project_request.user_id', Auth::user()->id)
                ->select('project_request.*', 'users.name', 'project.title')
                ->get();

            //edit user story requested details
            $editUsReq = DB::table('edit_us_request')
                ->join('project', 'edit_us_request.project_id', '=', 'project.id')
                ->select('edit_us_request.*', 'project.title')
                ->get();

            //delete user story requested details
            $deleteUsReq = DB::table('delete_us_request')
                ->join('project', 'delete_us_request.project_id', '=', 'project.id')
                ->select('delete_us_request.*', 'project.title')
                ->get();

            $projectId = DB::table('users')
                ->where('email', Auth::user()->email)
                ->select('project_id')
                ->get();

            return view('dev.request.index', compact('projectReq', 'editUsReq', 'deleteUsReq', 'projectId'));
        } catch (\Exception $exception) {
            $exceptionData['user_id'] = $this->userId;
            $exceptionData['exception'] = $exception->getMessage();
            $exceptionData['time'] = Carbon::now()->toDateTimeString();

            ExceptionsLog::create($exceptionData);
        }
    }

    /**
     * @return string
     * @description deleting pending requests
     */
    public function destroy() {
        $request = Input::all();
        try {
            $requestValues = DB::table($request['table'])
                ->where('id', $request['id'])
                ->get();

            //delete the notification of the deleted pending request
            DB::table('notification')
                ->where('project_id', $requestValues[0]->project_id)
                ->where('us_id', $requestValues[0]->us_id)
                ->where('triggered_by', $requestValues[0]->user_id)
                ->where('affected_user', $requestValues[0]->pm_id)
                ->delete();

            //delete the pending request
            DB::table($request['table'])
                ->where('id', $request['id'])
                ->delete();

            return "Successfully removed Pending Request!";
        } catch (\Exception $exception) {
            $exceptionData['user_id'] = $this->userId;
            $exceptionData['exception'] = $exception->getMessage();
            $exceptionData['time'] = Carbon::now()->toDateTimeString();

            ExceptionsLog::create($exceptionData);
        }
    }
}
