<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests;
use App\ExceptionsLog;
use Carbon\Carbon;

/**
 * Class DeveloperDashboardController
 * @package App\Http\Controllers
 * @description handling developer dashboard display details
 */
class DeveloperDashboardController extends Controller
{
    //logged in users email, user id should be assigned
    private $user;
    private $userId;

    /**
     * @description assigning user email and user id
     */
    public function __construct()
    {
        $this->user = Auth::user()->email;
        $this->userId = Auth::user()->id;
    }

    /**
     * @description user story count and project percentage calculations
     */
    public function index() {
        try {
            //users project details
            $project = DB::table('users')
                ->join('project', 'project.id', '=', 'users.project_id')
                ->select('project.title')
                ->where('users.id', $this->userId)
                ->get();

            //users user story details
            $userStory = DB::table('user_story')
                ->select('user_story.progress')
                ->where('user_story.user_id', $this->userId)
                ->get();

            //total user stories of the project
            $projectTotalUserStory = DB::table('user_story')
                ->select('user_story.id')
                ->count();

            $userStoryCount = $this->userStoryCount($userStory);
            $percentage = $this->calcPercentage($project, $userStoryCount, $projectTotalUserStory);

            return view('dev.dashboard.index', compact('project', 'userStoryCount', 'percentage'));
        } catch (\Exception $exception) {
            $exceptionData['user_id'] = $this->userId;
            $exceptionData['exception'] = $exception->getMessage();
            $exceptionData['time'] = Carbon::now()->toDateTimeString();

            ExceptionsLog::create($exceptionData);
        }
    }

    /**
     * @param $userStory
     * @return array
     * @description calculate user story count details
     */
    private function userStoryCount($userStory) {
        $userStoryInProgress = 0;
        $userStoryTesting = 0;
        $userStoryCompleted = 0;
        $totalUserStory = 0;

        if($userStory) {
            foreach ($userStory as $story) {
                if ($story->progress == 'in progress') {
                    $userStoryInProgress++;
                } elseif ($story->progress == 'testing') {
                    $userStoryTesting++;
                } else {
                    $userStoryCompleted++;
                }
                $totalUserStory++;
            }
        }
        $userStoryCount = array($totalUserStory, $userStoryInProgress, $userStoryTesting, $userStoryCompleted);

        return $userStoryCount;
    }

    /**
     * @param $project
     * @param $userStoryCount
     * @param $projectTotalUserStory
     * @return array
     * @description calculate project percentage details
     */
    private function calcPercentage($project, $userStoryCount, $projectTotalUserStory) {
        if(($project) && $userStoryCount[0]!=0 && $projectTotalUserStory!=0) {
            $projectPercentage = ceil(($userStoryCount[3] / $userStoryCount[0]) * 100.0);
            $projectResponsibility = ceil(($userStoryCount[0] / $projectTotalUserStory) * 100.0);
            $percentage = array($projectPercentage, $projectResponsibility);
        } else{
            $percentage = array($userStoryCount[0], $userStoryCount[0]);
        }

        return $percentage;
    }
}
