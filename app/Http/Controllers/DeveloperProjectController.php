<?php
namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use App\Http\Requests;
use App\ExceptionsLog;
use App\Project;
use App\User;
use Carbon\Carbon;

/**
 * Class DeveloperProjectController
 * @package App\Http\Controllers
 * @description
 */
class DeveloperProjectController extends Controller
{
    //logged in users email, user id should be assigned
    private $user;
    private $userId;
    //NotificationController implementation
    private $notification;

    /**
     * @description assigning logged in user's email
     * @param NotificationController $notificationController
     */
    public function __construct(NotificationController $notificationController)
    {
        $this->user = Auth::user()->email;
        $this->userId = Auth::user()->id;
        $this->notification = $notificationController;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @description displaying all available company projects
     */
    public function index()
    {
        //retrieve all available company projects
        $projects = Project::all();
        try {
            $projectId = User::select('project_id')
                ->where('email', $this->user)
                ->get();

            return view('dev.project.index', compact('projects', 'projectId'));
        } catch (\Exception $exception) {
            $exceptionData['user_id'] = $this->userId;
            $exceptionData['exception'] = $exception->getMessage();
            $exceptionData['time'] = Carbon::now()->toDateTimeString();

            ExceptionsLog::create($exceptionData);
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @description displaying individual selected project
     */
    public function show($id)
    {
        try {
            //retrieve selected project, project details
            $project = Project::where('id', $id)->get();
            if (empty($project[0])) {
                return abort(403, 'Requested Project is not available');
            } else {
                $pm = User::where('id', $project[0]['project_owner'])->get();
                $projectId = User::select('project_id')->where('email', $this->user)->get();
                return view('dev.project.show', compact('project', 'pm', 'projectId'));
            }
        } catch (\Exception $exception) {
            $exceptionData['user_id'] = $this->userId;
            $exceptionData['exception'] = $exception->getMessage();
            $exceptionData['time'] = Carbon::now()->toDateTimeString();

            ExceptionsLog::create($exceptionData);
        }
    }

    /**
     * @param $id
     * @return string
     * @description storing projects requests by developers to relevant project managers
     */
    public function store($id)
    {
        $userId = Auth::user()->id;
        try {
            //get details of the project and requested developers' details
            $request = Input::all();
            $checkRequest = DB::table('project_request')
                ->where('user_id', $userId)
                ->where('type', 'pending')
                ->get();

            //checking whether the user has already made a request to PM on the project
            if ($checkRequest != null) {
                return ("Already a Project request has been made, Please remain patience!");
            } else {
                $request['user_id'] = $userId;
                $request['requested_on'] = Carbon::now()->toDateString();
                //insert project request details
                DB::table('project_request')->insert($request);
                //insert notification
                $this->notification->addNotification($request['project_id'], 0, $request['pm_id'], 'request-project');

                return ("Request has been made to the Project Manager successfully!");
            }
        } catch (\Exception $exception) {
            $exceptionData['user_id'] = $this->userId;
            $exceptionData['exception'] = $exception->getMessage();
            $exceptionData['time'] = Carbon::now()->toDateTimeString();

            ExceptionsLog::create($exceptionData);
        }
    }
}
