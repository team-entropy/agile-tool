<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;

class UserStoryCompletionController extends Controller
{
    //
    private $user_type;
    private $project_id;
    const completed = "completed";
    const success  = 1;
    public function __construct()
    {
        $this->project_id = Session::get('project_id');
    }

    /**
     * @return string|void
     * @description verifies and updates user stories
     */
    function update(){
        $data = Input::all();
        $insert = null;
        try {
            $query = DB::table('user_story')->where("project_id", '=', $this->project_id)->where('id', '=', $data["id"])
                ->update(["progress" => $data["value"]]);
            if($data["value"] == self::completed){
                $insert = DB::table('user_story_meta_data')
                    ->insert(["status"=>self::completed , "time_changed"=> Carbon::today()->toDateString("m/d/y"),
                        "project_id" => $this->project_id , "user_story_id"=>$data["id"]]);
            }
            if($query == self::success && $insert == self::success ){
                return json_encode("1");
            }else{
                return json_encode("0");
            }
        }catch(QueryException $ex){
            $exceptionData['user_id'] = $this->userId;
            $exceptionData['exception'] = $exception->getMessage();
            $exceptionData['time'] = Carbon::now()->toDateTimeString();

            return abort(500,"Cannot Access Database Please Try again \n".$exception);
        }


    }
}
