<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use App\Http\Requests;
use App\ExceptionsLog;
use Carbon\Carbon;

/**
 * Class ProfileController
 * @package App\Http\Controllers
 * @description handling logged in user profile
 */
class ProfileController extends Controller
{
    //logged in users email, user id should be assigned
    private $user;
    private $userId;
    //NotificationController implementation
    private $notification;

    /**
     * @param NotificationController $notificationController
     * @description assigning user id, user email
     */
    public function __construct(NotificationController $notificationController)
    {
        $this->user = Auth::user()->email;
        $this->userId = Auth::user()->id;
        $this->notification = $notificationController;
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|void
     * @description displaying profile of the looged in user
     */
    public function index($id) {
        if($this->userId == $id) {
            //logged in users details
            $userDetails = $this->getUserDetails($id);
            $type = Auth::user()->type;
            return view('profile.index', compact('userDetails', 'type'));
        } else {
            return abort(403, 'Requested User Profile is not allowed to access');
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|void
     * @description displaying profile edit view to the logged in user
     */
    public function edit($id) {
        if($this->userId == $id) {
            //logged in users details
            $userDetails = $this->getUserDetails($id);
            $type = Auth::user()->type;
            return view('profile.edit', compact('userDetails', 'type'));
        } else {
            return abort(403, 'Requested User Profile is not allowed to access');
        }
    }

    /**
     * @param $id
     * @return string
     * @description updating profile details
     */
    public function update($id) {
        //get updated data through AJAX
        $request = Input::all();
        try{
            //update users table
            DB::table('users')
                ->where('id', $id)
                ->update(['name' => $request['name']]);

            //send mail to the relevant user on profile update
            $this->mailMethod('mail.profile');
            //insert notification on profile update
            $this->notification->addNotification(0, 0, $this->userId, 'self-profile_update');

        } catch (\Exception $exception) {
            $exceptionData['user_id'] = $this->userId;
            $exceptionData['exception'] = $exception->getMessage();
            $exceptionData['time'] = Carbon::now()->toDateTimeString();

            ExceptionsLog::create($exceptionData);
        }

        return 'Your profile has been successfully updated!';
    }

    /**
     * @param $id
     * @return string
     * @description updating password details
     */
    public function updatePass($id) {
        //get updated data through AJAX
        $request = Input::all();
        try{
            //get current password
            $password = DB::table('users')
                ->select('users.password')
                ->where('id', $id)
                ->get();

            //check current password in DB with entered current password
            if(Hash::check($request['password'], $password[0]->password)) {
                $newPassword = bcrypt($request['newPassword']);
                //update users table with new password
                DB::table('users')
                    ->where('id', $id)
                    ->update(['password' => $newPassword]);

                //send mail to the relevant user on password update
                $this->mailMethod('mail.password');
                //insert notification on password update
                $this->notification->addNotification(0, 0, $this->userId, 'self-password_change');

                return 'true';
            } else {
                return 'false';
            }
        } catch (\Exception $exception) {
            $exceptionData['user_id'] = $this->userId;
            $exceptionData['exception'] = $exception->getMessage();
            $exceptionData['time'] = Carbon::now()->toDateTimeString();

            ExceptionsLog::create($exceptionData);
        }
    }

    /**
     * @param $id
     * @return null
     * @description returning details related to logged in user
     */
    private function getUserDetails($id) {
        $userDetails = null;
        try{
            //logged in users details
            $userDetails = DB::table('users')
                ->select('users.*')
                ->where('id', $id)
                ->get();
        } catch (\Exception $exception) {
            $exceptionData['user_id'] = $this->userId;
            $exceptionData['exception'] = $exception->getMessage();
            $exceptionData['time'] = Carbon::now()->toDateTimeString();

            ExceptionsLog::create($exceptionData);
        }
        return $userDetails;
    }

    /**
     * @param $path
     * @description sending mail to the logged in user upon profile &/ password update
     */
    private function mailMethod($path) {
        $data = array(
            'name' => Auth::user()->name,
            'time' => Carbon::now(),
        );

        Mail::send($path, $data, function($message) {
            $message->from('sliitentropy@gmail.com', 'Agile Management');
            $message->to('saadbulto@gmail.com')->subject('Profile Update');
        });
    }

    /**
     * @description loads the notification history view
     */
    public function showNotifications() {
        try {
            return view('profile.notification.show');
        } catch (\Exception $exception) {
            $exceptionData['user_id'] = $this->userId;
            $exceptionData['exception'] = $exception->getMessage();
            $exceptionData['time'] = Carbon::now()->toDateTimeString();

            ExceptionsLog::create($exceptionData);
        }
    }

    /**
     * @return mixed
     * @description returns the notification log
     */
    public function getNotifications() {
        try {
            $notifications = DB::table('notification')
                ->join('project', 'project.id', '=', 'notification.project_id')
                ->where('affected_user', $this->userId)
                ->select('notification.*','project.title')
                ->orderBy('id', 'desc')
                ->get();

            return $notifications;
        } catch (\Exception $exception) {
            $exceptionData['user_id'] = $this->userId;
            $exceptionData['exception'] = $exception->getMessage();
            $exceptionData['time'] = Carbon::now()->toDateTimeString();

            ExceptionsLog::create($exceptionData);
        }
    }
}
