<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use App\Http\Controllers\ClientAnalyticsController as ClientAnalytics;
use App\Http\Controllers\ProductBacklogController as ProductBacklog;

class ClientProjectController extends Controller
{
    /**
     *@description  This class is responsible for clients projects
    */
    private $_projectId ;
    private $_userType;
    private $_userId;
    const ErrorValue = -1;
    protected $analytics;
    private $productBacklog;
    /**
     * ClientProjectController constructor.
     * @param ClientAnalyticsController $analytics
     * @inject takes ClientAnalyticsController service as a parameter
     * @description constructor
     */
    public function __construct(ClientAnalytics $analytics, ProductBacklog $productBacklog){
        $this->_projectId = Session::get('project_id');
        //check session set else redirect to projects page
        if(empty($this->_projectId)){
            //send flash message to select a project
            Redirect::to('/client')->send();
        }else{
            $this->analytics = $analytics;
            $this->_userType = Auth::user()->type;
            $this->_userId = Auth::user()->id;
            $this->productBacklog = $productBacklog;
        }
    }


    /**
     * @description this method returns the dashboard for the relevant selected project
     * by retrieving all components from ClientAnalyticsController
     * @return view dashboard
     * @internal param $id
     */
    public function getDashboard(){
        $percentage = $this->analytics->projectCompletionPercentage();
        return view('client.dashboard',compact('percentage'));
        //method calls to retrieve all components within the dashboard
    }

    /**
     * @description returns  timeline n burndown for client
     */
    public function getProjectAnalytics(){
        $projectTimeLine = $this->analytics->projectProgressTimeLine();
        $projectBurnDown = $this->analytics->burnDownChartData();

        $data = [
            "time-line"=> $projectTimeLine,
            "burn-down"=> $projectBurnDown
        ];
        return json_encode($data);

    }

    /**
     * @description returns the clients product backlog
     */
    public function getProductBacklog(){
            return view('client.partials.productBacklog');
    }


    /**
     * @return mixed
     * @description returns all userstories
     */
    public function getAllUserStories(){
        return $this->productBacklog->all();
    }

}
