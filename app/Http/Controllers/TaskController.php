<?php

namespace App\Http\Controllers;

use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class TaskController extends Controller
{

    private $project_id ;
    private $userType ;

    /**
     * TaskController constructor.
     */
    public function __construct()
    {
        $this->project_id = Session::get('project_id');
        //check if session is set else redirect to project page
        $this->userType = Auth::user('type');
    }
    /**
     * @return string|void
     * @description this method is used to store newly added tasks
     */
    function store(){
        $request = Input::all();
        try {
            $taskCount = DB::table('task')->where('project_id', '=', $this->project_id)->where('us_id', '=',
                $request['us_id'])->max('task_id');
            $taskId = $taskCount+1;
            $query = DB::table('task')->insert(['task_id' => $taskId, 'us_id' => $request['us_id'],
                'task' => $request['title'], 'progress' => 'Open', 'project_id' => $this->project_id]);

            if ($query) {
                return json_encode("Successfully Added Task");
            } else {
                return json_encode("Error Adding Task.. Please Try Again Later");
            }
        }catch(\Exception $exception){
            $exceptionData['user_id'] = $this->userId;
            $exceptionData['exception'] = $exception->getMessage();
            $exceptionData['time'] = Carbon::now()->toDateTimeString();
            return abort(500,"Cannot Access Database Please Try again \n".$exception);
        }
    }
    /**
     * @return string
     */
    function update(){
        $value = Input::all();
        try {
            $query = DB::table('task')->where('project_id', '=', $this->project_id)->where('us_id', '=', $value['us_id'])
                ->where('task_id', '=', $value['task_id'])->update(['task' => $value['title']]);
            if($query == 1){
                return json_encode("Successfully Updated Task");
            }else{
                return json_encode("No Changes Made to Task");
            }
        }catch(\Exception $exception){
            $exceptionData['user_id'] = $this->userId;
            $exceptionData['exception'] = $exception->getMessage();
            $exceptionData['time'] = Carbon::now()->toDateTimeString();
            return abort(500,"Cannot Access Database Please Try again \n".$exception);
        }
    }

    /**
     * @return string
     */
    function delete(){
        $value = Input::all();
        try {
            $query = DB::table('task')->where('project_id', '=', $this->project_id)->where('us_id', '=', $value['us_id'])
                ->where('task_id', '=', $value['task_id'])->delete();
            if($query == 1){
                return json_encode("Successfully Delete Task");
            }else{
                return json_encode("The Task is Already Deleted or Is not Available");
            }
        }catch(\Exception $exception){
            $exceptionData['user_id'] = $this->userId;
            $exceptionData['exception'] = $exception->getMessage();
            $exceptionData['time'] = Carbon::now()->toDateTimeString();
            return json_encode("Database Error Please Try Again");
        }
    }

    /**
     * @return string
     */
    function updateTaskStatus(){
        $value = Input::all();
        try {
            $query = DB::table('task')->where('project_id', '=', $this->project_id)->where('us_id', '=', $value['us_id'])
                ->where('task_id', '=', $value['task_id'])->update(['progress'=>$value['progress']]);
            if($query == "1"){
                return json_encode("success");
            }else{
                if($query == "0") {
                    return json_encode("0");
                }else{
                    return json_encode("Error Please Try Again");
                }
            }
        }catch(\Exception $exception){
            $exceptionData['user_id'] = $this->userId;
            $exceptionData['exception'] = $exception->getMessage();
            $exceptionData['time'] = Carbon::now()->toDateTimeString();
            return abort(500,"Cannot Access Database Please Try again \n".$exception);
        }
    }

    /**
     * @param $usId
     * @return float|int|void
     */
    function calculateUserStoryProgress($usId)
    {
            try {
                $querySuccessCount = DB::table('task')->where('project_id', '=', $this->project_id)->where('us_id', '=', $usId)
                    ->where('progress', '=', 'Completed')->count();
                $queryTotalCount = DB::table('task')->where('project_id', '=', $this->project_id)->where('us_id', '=', $usId)
                    ->count();
                if ($queryTotalCount == 0) {
                    return -1;
                } else {
                    return ($querySuccessCount / $queryTotalCount) * 100.0;
                }
            }catch(\Exception $exception){
                $exceptionData['user_id'] = $this->userId;
                $exceptionData['exception'] = $exception->getMessage();
                $exceptionData['time'] = Carbon::now()->toDateTimeString();
                return abort(500,"Cannot Access Database Please Try again \n".$exception);
            }
    }
}
