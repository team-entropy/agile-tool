<?php
namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\UserStory;
use Auth;
use Illuminate\Support\Facades\Session;

/**
 * Class ProductBacklogController
 * @description Handles the controller logic for the PM's product backlog
 * @package App\Http\Controllers
 */
class ProductBacklogController extends Controller
{
    /**
     * @description return product backlog view to user
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(Request $request) {
        return view('pages.productBacklog');
    }

    /**
     * @description return all relevant user stories
     * @return mixed
     */
    public function all()
    {
        $projectId = Session::get('project_id');

        $allUserStories = UserStory::where('project_id', $projectId)
                            ->orderBy('priority', 'asc')
                            ->get();

        return $allUserStories;
    }

    /**
     * @description return a single user story by id
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        $userStory = UserStory::where('id', $id)->first();
        return view('pages.productBacklogEdit', compact('userStory'));
    }

    /**
     * @description save the user story
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store()
    {

        $input = Request::all();
        //dd($input);
        $userStory = UserStory::where('us_id', $input['user-story-id'])->first();

        if ($userStory->update(['title' => $input['title']])) {

            return redirect()->action('ProductBacklogController@index');

        } else {
            abort(404);
        }
    }

    /**
     * @description delete user story
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function remove(Request $request)
    {

        $this->validate($request, [
            'user_story_id' => 'required'
        ]);

        $userStory = UserStory::where('id', $request->user_story_id)->first();

        if ($userStory->delete()) {

            return response()->json([
                'message' => 'The user story was removed successfully'
            ], 200);

        } else {
            return response()->json([
                'responseText' => 'A database error occurred when updating the record.'
            ], 500);
        }
    }

    /**
     * @description update user story
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request) {
        $this->validate($request, [
            'title' => 'required|min:5',
            'description' => 'required|min:20',
            'effort' => 'required',
            'progress' => 'required',
            'priority' => 'required'
        ]);

        try {
            $userStory = UserStory::find($request->user_story_id);

            $userStory->title = $request->title;
            $userStory->description = $request->description;
            $userStory->effort = $request->effort;
            $userStory->progress = $request->progress;
            $userStory->priority = $request->priority;

            if ($userStory->save()) {
                return response()->json([
                    'message' => 'The user story was updated successfully'
                ], 200);
            } else {
                return response()->json([
                    'responseText' => 'A database error occurred when updating the record.'
                ], 500);
            }
        } catch(\Exception $exception){
            $exceptionData['user_id'] = Auth::user()->id;
            $exceptionData['exception'] = $exception->getMessage();
            $exceptionData['time'] = Carbon::now()->toDateTimeString();

            ExceptionsLog::create($exceptionData);

            return response()->json([
                'responseText' => 'A system error occured.'
            ], 500);
        }
    }
}
