<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use App\User;

class DeveloperSprintController extends Controller
{
    private $user;
    private $userId;
    private $projectId;
    //
    public function __construct()
    {
        $this->user = Auth::user()->email;
        $this->userId = Auth::user()->id;
        $this->projectId = (integer)DB::table('users')->select('project_id')->where('email','=',$this->user)->get();
//        $this->projectId = User::select('project_id')
//            ->where('email', $this->user)
//            ->get();

    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @description this method presents the view for developer sprint backlog
     * @route /dev/sprint-backlog
     */
    function index(){
        $sprintNo = DB::table('sprint')->where('project_id','=',$this->projectId)->count();
        $title = DB::table('project')->select('title')->where('id', '=', $this->projectId)->get();
        $projectsDetails = [
             "sprintNo" => $sprintNo
        ];


        return view('dev.sprint-backlog.sprint-backlog',compact('projectsDetails'));
    }

    /**
     * @param $id
     * @return string
     * @description this method returns data to render sprint backlog datatable for the developer
     * @route /dev/sprint-backlog-all
     */
    function getUserStories($id){


        $allUserStories = DB::table('user_story')->where('project_id','=' ,$this->projectId)->where('sprint_id','=',$id)
            ->orderBy('priority', 'asc')
            ->get();

        return json_encode($allUserStories);
    }
}
