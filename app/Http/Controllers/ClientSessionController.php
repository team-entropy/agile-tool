<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use DB;

class ClientSessionController extends Controller
{
    //
    /**
     * ClientSessionController constructor.
     */
    private $_userId;
    const ErrorValue = -1;
    public function __construct()
    {
        $this->_userId = Auth::user()->id;
    }

    /**
     * @param $projectId
     * @description this method sets a session for the given projectId
     * @return redirect
     */
    public function setSession($projectId){
        $query = DB::table('project')->where('id','=',$projectId)->where('client_id','=',$this->_userId)->get();
        if(!empty($query)){
            Session::put('project_id',$projectId);
            return redirect('/client/dashboard');
        }else{
            //pass flash message to select project
            return redirect('/client');
        }


    }

    /**
     * @description - This method returns all available projects under the logged in client
     * @route - /client - this the index method for the client to choose a project before proceeding
     */
    public function getClientProjects(){
        try{
            $projects = DB::table('project')->where('client_id','=',$this->_userId)->get();

            if($projects != null){
                return view('client.index',compact('projects'));
            }else{
                //return partial sorry no ongoing projects yet...
                return $this::ErrorValue;
            }
        }catch(QueryException $exception){
            $exceptionData['user_id'] = $this->userId;
            $exceptionData['exception'] = $exception->getMessage();
            $exceptionData['time'] = Carbon::now()->toDateTimeString();

            return abort(500,"Cannot Access Database Please Try again \n".$exception);
        }
    }



}
