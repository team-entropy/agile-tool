<?php

namespace App\Http\Controllers;


use App\Project;
use App\Sprint;
use App\UserStory;
use Illuminate\Database\QueryException;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Request;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Symfony\Component\Translation\Util\ArrayConverter;
use App\Http\Controllers\ProductBacklogController as ProductBacklog;
use App\Http\Controllers\ClientAnalyticsController as Analytics;


/**
 * Class SprintController
 * @package App\Http\Controllers
 */
class SprintController extends Controller
{
    /**
     * @description This class handles all operations with regards to Sprints
     */


    private $_projectId ;
    private $_userType ;
    private $productBacklog;
    private $analytics;
    /**
     * SprintController constructor.
     */
    public function __construct(ProductBacklog $productBacklog , Analytics $analytics)
    {

        $this->_projectId = Session::get('project_id');
        //check session set else redirect to projects page
        if(empty($this->_projectId)){
            Redirect::to('/pm')->send();
        }else{
            $this->_userType = Auth::user()->type;
            $this->productBacklog = $productBacklog;
            $this->analytics = $analytics;
        }

    }


    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(){
        $user = $this->_userType;
        try {
            $sprintNo = Sprint::Project($this->_projectId)->count();

            // members relevant to the project will be retrieved using the cookie set for the project
            $members = DB::table('users')->select('name')->where('project_id', '=', $this->_projectId)->get();
            $title = DB::table('project')->select('title')->where('id', '=', $this->_projectId)->get();
            $projectsDetails = [
                "Name" => $title[0]->title, "sprintNo" => $sprintNo
            ];
            $allUserStories = UserStory::Project($this->_projectId)->get();
            //initially only user stories belonging to the first sprint of selected project will be displayed
            $singleSprint = DB::table('user_story')->where('project_id', '=', $this->_projectId)->where('sprint_id', '=', '1')
                ->get();

            return view('sprints.sprintBacklog', compact('projectsDetails', 'allUserStories', 'members', 'user'
                , 'singleSprint'));
        }catch(\Exception $exception){
            $exceptionData['user_id'] = $this->userId;
            $exceptionData['exception'] = $exception->getMessage();
            $exceptionData['time'] = Carbon::now()->toDateTimeString();

            return abort(500,"Cannot Access Database Please Try again \n".$exception);
        }
    }

    /**
     * @param \Illuminate\Http\Request|Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @description //description sort members
     */
    public function create(\Illuminate\Http\Request $request){
        $this->validate($request,['scrum_master'=>'required','start_date'=>'required','end_date'=>'required']);
        $sprint = Request::all();
        $result = DB::table('sprint')->whereBetween('start_date',[$sprint['start_date'],$sprint['end_date']])
            ->OrwhereBetween('end_date',[$sprint['start_date'],$sprint['end_date']])->get();
        $memberId = DB::table('users')->select('id')->where('project_id','=',$this->_projectId)
            ->where('name','=',$sprint['scrum_master'])->get();

        if($result == null) {
            $queryResult = DB::table('sprint')
                ->insert(['sprint_id' => $sprint['sprint'],
                    'member_id' => $memberId[0]->id, 'project_id' => $this->_projectId,
                    'start_date' => $sprint['start_date'],
                    'end_date' => $sprint['end_date']]);
            if($queryResult){
                //returns a flash message to the view
                return redirect('pm/sprint-backlog')->with('Success','Successfully Added Sprint');
            }else{
                return redirect('pm/sprint-backlog')->with('Error','Error Adding Sprint to Project');
            }
        }else{
            //returns a flash message saying sprint exists here
            return redirect('pm/sprint-backlog')->with('Error','A Sprint Already Exists Within the Given Period');
        }
    }

    /**
     * @param $usID
     * @return mixed
     * @description this method returns data(tasks,info,description) relevant to selected user story
     */
    function show($usID){
        try {
            $usId = DB::table('user_story')->where('project_id', '=', $this->_projectId)->where('id', '=', $usID)->get();
            $tasks = DB::table('task')->where('us_id', '=', $usId[0]->id)->get();
        } catch(\Exception $exception){
            $exceptionData['user_id'] = $this->userId;
            $exceptionData['exception'] = $exception->getMessage();
            $exceptionData['time'] = Carbon::now()->toDateTimeString();
            return abort(500,"Cannot Access Database Please Try again \n".$exception);
        }
        //add all details to the data array
        $data = [
            "tasks" => $tasks,
            "userStories" => $usId
        ];
       return json_encode($data);
    }

    /**
     * @param $sprintId
     * @return string
     * @description this method returns user stories relevant to selected single sprint in Sprint view
     */
    function getSprintUserStories($sprintId){
        try {
            $singleSprint = DB::table('user_story')->where('project_id', '=', $this->_projectId)
                ->where('sprint_id', '=', $sprintId)->get();
        } catch(\Exception $exception){
            $exceptionData['user_id'] = $this->userId;
            $exceptionData['exception'] = $exception->getMessage();
            $exceptionData['time'] = Carbon::now()->toDateTimeString();
            return abort(500,"Cannot Access Database Please Try again \n".$exception);
        }
        return json_encode($singleSprint);
    }

    /**
     * @return string
     */
    function addDescriptionToUserStories(){
        $result = Input::all();
        //checks if the user is a pm since only pm's can directly make changes to a description
        if ($this->_userType == 'pm') {
            try {
                $query = DB::table('user_story')->where('id', '=', $result['id'])
                    ->where('project_id', '=', $this->_projectId)
                    ->update(['description' => $result['description']]);

            }catch(\Exception $exception){
                $exceptionData['user_id'] = $this->userId;
                $exceptionData['exception'] = $exception->getMessage();
                $exceptionData['time'] = Carbon::now()->toDateTimeString();
                abort(500,"Cannot Access Database Please Try again \n".$exception);
            }

            if($query == "1"){
                return json_encode("Successfully Updated Description");
            }else {
                return json_encode("You have not made any changes to the description");
            }
        }else if($this->_userType == 'dev'){
            //send as a request to PM will be implemented later
            return json_encode("Request Sent to PM");
        }else{
            return json_encode("error");
        }

    }

    /**
     * @return int|string
     * @description gets the available sprints under the current project
     */
    function getAvailableSprints(){
        $query = DB::table('sprint')->select('sprint_id')->where('project_id','=',$this->_projectId)
            ->where('end_date','>',Carbon::now()->format('m/d/y'))->get();
        if(!empty($query)){
            return json_encode($query);
        }else{
            return json_encode($query);
        }
    }

    /**
     * @return string
     *
     */
    function updateUserStorySprint(){
        $request = Input::all();
        try {
            $query = DB::table('user_story')->where('project_id', '=', $this->_projectId)
                ->where('id', '=', $request['us_id'])->update(["sprint_id" => $request['sprint_id']]);
            if($query == 1){
                return json_encode("successfully updates ");
            }else{
                return json_encode("no changes made");
            }
        }catch(\Exception $exception){
            $exceptionData['user_id'] = $this->userId;
            $exceptionData['exception'] = $exception->getMessage();
            $exceptionData['time'] = Carbon::now()->toDateTimeString();
        }
    }

    /**
     * @param $id
     * @return mixed
     * @description this method returns all user stories in a given sprint
     */
    function getUserStories($id){
        try {
            $allUserStories = DB::table('user_story')->where('project_id', '=', $this->_projectId)->where('sprint_id', '=', $id)
                ->orderBy('priority', 'asc')
                ->get();
        }catch(QueryException $exception){
            $exceptionData['user_id'] = $this->userId;
            $exceptionData['exception'] = $exception->getMessage();
            $exceptionData['time'] = Carbon::now()->toDateTimeString();
        }
        return json_encode($allUserStories);
    }

    /**
     * @description this method returns all the sprint data
     */
    function allSprints(){
        $startDate = Carbon::parse(Carbon::today())
            ->diffInDays(Carbon::parse($this->analytics->getProjectEndDate()));
        return view('sprints.sprintsProgress',compact('startDate'));
    }

    /**
     * @description this method returns the sprints timeline data
     */
    function getSprintsTimeline(){
        try {
            $allSprints = DB::table('sprint')->select('sprint_id', 'start_date', 'end_date')
                ->where('project_id', '=', $this->_projectId)->get();

            return json_encode($allSprints);
        }catch(QueryException $exception){
            $exceptionData['user_id'] = $this->userId;
            $exceptionData['exception'] = $exception->getMessage();
            $exceptionData['time'] = Carbon::now()->toDateTimeString();
        }


    }

    /**
     * @return string
     * @description
     */
    function getBurnDown(){
        $data = ["burn-down" => $this->analytics->burnDownChartData()];
        return json_encode($data);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @description returns view for pm burndown
     */
    function renderBurnDown(){
        return view('sprints.partials.burndown');
    }
}
