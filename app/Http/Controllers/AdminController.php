<?php
namespace App\Http\Controllers;

use App\ExceptionsLog;
use App\User;
use App\UserStory;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Support\Facades\DB;

/**
 * Class AdminController
 * @description Handles most of the admin page logic
 * @package App\Http\Controllers
 */
class AdminController extends Controller
{
    /**
     * @description returns the admin dashboard view to the user
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index() {
        $user = Auth::user();
        return view('admin.index', compact('user'));
    }

    /**
     * @description returns the create user page
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showCreateUser() {
        return view('admin.createUser');
    }

    /**
     * @description returns the admin dashboard view to the user
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showViewUsers() {

        return view('admin.viewUsers');
    }

    /**
     * @description creates a user in the database and returns a status message
     * This function creates a user in the database from the credentials provided
     */
    public function createUser(Request $request) {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required|min:8',
            'type' => 'required|in:dev,client,pm,admin'
        ]);

        try {
            $newUser = new User();
            $newUser->name = $request->name;
            $newUser->email = $request->email;
            $newUser->password = bcrypt($request->password);
            $newUser->type = $request->type;
            $newUser->bitbucket_username = $request->bitbucketUsername;

            if($newUser->save()) {
                return response()->json([
                    'message' => 'User was created successfully'
                ], 200);
            }
            else {
                return response()->json([
                    'responseText' => 'A database error occurred when creating the user.'
                ], 500);
            };
        } catch(\Exception $exception){
            $exceptionData['user_id'] = Auth::user()->id;
            $exceptionData['exception'] = $exception->getMessage();
            $exceptionData['time'] = Carbon::now()->toDateTimeString();

            ExceptionsLog::create($exceptionData);

            return response()->json([
                'responseText' => 'A database error occurred when creating the user.'
            ], 500);
        }
    }

    /**
     * @description returns all the users in the system in JSON format
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function getAllUsers() {
        try {
            $users = User::all(['name', 'email', 'type', 'project_id', 'id']);
            return response($users);
        } catch(\Exception $exception){
            $exceptionData['user_id'] = Auth::user()->id;
            $exceptionData['exception'] = $exception->getMessage();
            $exceptionData['time'] = Carbon::now()->toDateTimeString();

            ExceptionsLog::create($exceptionData);

            return response()->json([
                'responseText' => 'A database error occurred when creating the user.'
            ], 500);
        }
    }

    /**
     * @description returns the edit user view
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showEdit($id) {
        try {
            $user = User::where('id', $id)->first();
            return view('admin.editUser', compact('user'));
        } catch(\Exception $exception){
            $exceptionData['user_id'] = Auth::user()->id;
            $exceptionData['exception'] = $exception->getMessage();
            $exceptionData['time'] = Carbon::now()->toDateTimeString();

            ExceptionsLog::create($exceptionData);

            return response()->json([
                'responseText' => 'A database error occurred when creating the user.'
            ], 500);
        }
    }

    /**
     * @description updates the user and returns a status message
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateUser(Request $request) {
        $user = User::find($request->user_id);

        if(strcasecmp($user->email, $request->email) == 0) {
            $this->validate($request, [
                'name' => 'required',
                'type' => 'required|in:dev,client,pm,admin'
            ]);
        }
        else {
            $this->validate($request, [
                'name' => 'required',
                'email' => 'required|email|unique:users',
                'type' => 'required|in:dev,client,pm,admin'
            ]);
        }

        try {
            $user = User::find($request->user_id);
            $user->name = $request->name;
            $user->email = $request->email;
            $user->type = $request->type;
            $user->bitbucket_username = $request->bitbucketUsername;

            if($user->save()) {
                return response()->json([
                    'message' => 'The user\'s information was updated successfully'
                ], 200);
            }
            else {
                return response()->json([
                    'responseText' => 'A database error occurred when updating the record.'
                ], 500);
            }
        } catch(\Exception $exception){
            $exceptionData['user_id'] = Auth::user()->id;
            $exceptionData['exception'] = $exception->getMessage();
            $exceptionData['time'] = Carbon::now()->toDateTimeString();

            ExceptionsLog::create($exceptionData);

            return response()->json([
                'responseText' => 'A database error occurred when creating the user.'
            ], 500);
        }
    }

    /**
     * @description updates the password and returns a status message
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updatePassword(Request $request) {
        $this->validate($request, [
            'password' => 'required|min:8',
        ]);
        try {
            $user = User::find($request->user_id);
            $user->password = bcrypt($request->password);

            if($user->save()) {
                return response()->json([
                    'message' => 'The password was updated successfully.'
                ], 200);
            }
            else {
                return response()->json([
                    'responseText' => 'A database error occurred when updating the record.'
                ], 500);
            }
        } catch(\Exception $exception){
            $exceptionData['user_id'] = Auth::user()->id;
            $exceptionData['exception'] = $exception->getMessage();
            $exceptionData['time'] = Carbon::now()->toDateTimeString();

            ExceptionsLog::create($exceptionData);

            return response()->json([
                'responseText' => 'A database error occurred when creating the user.'
            ], 500);
        }
    }

    /**
     * @description deletes the given user and returns a status message
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteUser(Request $request) {
        $this->validate($request, [
            'user_id' => 'required'
        ]);
        try {
            $user = User::where('id', $request->user_id)->first();
            if ($user->delete()) {

                return response()->json([
                    'message' => 'The user was removed successfully'
                ], 200);

            } else {
                return response()->json([
                    'responseText' => 'A database error occurred when updating the record.'
                ], 500);
            }
        } catch(\Exception $exception){
            $exceptionData['user_id'] = Auth::user()->id;
            $exceptionData['exception'] = $exception->getMessage();
            $exceptionData['time'] = Carbon::now()->toDateTimeString();

            ExceptionsLog::create($exceptionData);

            return response()->json([
                'responseText' => 'A database error occurred when creating the user.'
            ], 500);
        }

    }

    /**
     * @description returns all the login records joined with the user records
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function getLoginLog() {
        try {
            $allLogs = DB::table('login_log')
                ->join('users', 'users.id', '=', 'login_log.user_id')
                ->orderBy('login_log.logged_in_datetime', 'asc')
                ->get(array('login_log.*', 'users.*'));

            return response($allLogs);
        } catch(\Exception $exception){
            $exceptionData['user_id'] = Auth::user()->id;
            $exceptionData['exception'] = $exception->getMessage();
            $exceptionData['time'] = Carbon::now()->toDateTimeString();

            ExceptionsLog::create($exceptionData);

            return response()->json([
                'responseText' => 'A database error occurred when creating the user.'
            ], 500);
        }
    }

    /**
     * @description returns the login log view
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function viewLoginLog() {
        return view('admin.logs.viewLoginLog');
    }

    /**
     * @description returns the exception log view
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function viewExceptionsLog() {
        return view('admin.logs.viewExceptionsLog');
    }

    /**
     * @description returns the exception log records
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function getExceptionsLog() {
        try {
            $allLogs = DB::table('exception_log')
                ->join('users', 'users.id' , '=' , 'exception_log.user_id')
                ->orderBy('exception_log.time', 'asc')
                ->get(array('exception_log.*', 'users.*'));

            return response($allLogs);
        } catch(\Exception $exception){
            $exceptionData['user_id'] = Auth::user()->id;
            $exceptionData['exception'] = $exception->getMessage();
            $exceptionData['time'] = Carbon::now()->toDateTimeString();

            ExceptionsLog::create($exceptionData);

            return response()->json([
                'responseText' => 'A database error occurred when creating the user.'
            ], 500);
        }
    }
}
