<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Ixudra\Curl\Facades\Curl;
use Log;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use App\User;
use Illuminate\Support\Facades\Session;
use App\Project;
use Carbon\Carbon;
use App\ExceptionsLog;

class BitbucketContoller extends Controller
{
    public function show() {
        return view('projects.bitbucket.show');
    }

    public function getCommitData(Request $request) {
        try {
            $id = Session::get('project_id');
            $project = Project::where('id', $id)
                ->first();

            $repoUrl = "https://api.bitbucket.org/2.0/repositories/".$project->repo_owner."/".$project->repo_slug."/commits";

            $response = Curl::to($repoUrl)->get();
            $repoData = json_decode($response);
            $repoData = $repoData->values;
            $consumeRepoData = array();

            foreach($repoData as $commit) {
                $date = new \DateTime($commit->date);
                $temp = array(
                    "author" => $commit->author->user->display_name,
                    "date" => $date->format('Y-m-d'),
                    "message" => $commit->message
                );
                array_push($consumeRepoData, $temp);
            }

            return response()->json([
                'data' => $consumeRepoData
            ], 200);

        } catch(\Exception $exception){
            $exceptionData['user_id'] = Auth::user()->id;
            $exceptionData['exception'] = $exception->getMessage();
            $exceptionData['time'] = Carbon::now()->toDateTimeString();

            ExceptionsLog::create($exceptionData);

            return response()->json([
                'responseText' => 'An error occured while fetching the data. Please check the \'Repo Slug\' and \'Repo Owner\' field again.'
            ], 422);
        }
    }

    public function getUserRepos() {
        try {
            $id = Auth::user()->id;
            $user = User::where('id', $id)->first();

            $repoUrl = "https://api.bitbucket.org/2.0/repositories/". Auth::user()->bitbucket_username;
            error_log($repoUrl);
            $response = Curl::to($repoUrl)->get();
            $repoData = json_decode($response);
            $repoData = $repoData->values;
            $consumeRepoData = array();

            foreach($repoData as $projects) {
                $date = new \DateTime($projects->created_on);
                $formattedSlug = strtolower($projects->name);
                $formattedSlug = str_replace(" ", "-", $formattedSlug);
                $temp = array(
                    "createdOn" => $date->format('Y-m-d'),
                    "name" => $projects->name,
                    "language" => $projects->language,
                    "repoSlug" => $formattedSlug,
                    "repoOwner" => $projects->owner->username
                );
                array_push($consumeRepoData, $temp);
            }

            return response()->json([
                'data' => $consumeRepoData
            ], 200);

        } catch(\Exception $exception){
            $exceptionData['user_id'] = Auth::user()->id;
            $exceptionData['exception'] = $exception->getMessage();
            $exceptionData['time'] = Carbon::now()->toDateTimeString();

            ExceptionsLog::create($exceptionData);

            return response()->json([
                'responseText' => 'An error occured while fetching the data. Please check the \'Bitbucket Username\' field again.'
            ], 422);
        }
    }

    public function registerView(Request $request) {
        return view('admin.registerBitbucket');
    }

    public function register(Request $request) {
        $data = $request->json()->all();
        //error_log("accessToken: " . $data['accessToken']);

        $user = User::find(Auth::user()->id);
        $user->bitbucket_token = $data['accessToken'];

        if($user->save()) {
            return response()->json([
                'message' => 'The user account was successfully linked. Your will be redirected in 5 seconds.',
                'redirectUri' => 'http://localhost:8888/admin/user-management/edit/' . Auth::user()->id
            ], 200);
        }
        else {
            return response()->json([
                'responseText' => 'A database error occurred when updating the record.'
            ], 500);
        }
    }
}
