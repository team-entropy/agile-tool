<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function() {
    return redirect('/auth/login');
});


/**
 * Use the space below to test controller methods
 * using postman bypassing the middleware via Routes
*/

/**
 * This route grouping is used if the route has to support sessions ONLY.
 * Most routes either require no sessions or need sessions along with additional functionality.
 * This grouping should therefore not exist in production
 */

Route::group(['middleware' => 'web'], function() {
    // Authentication routes...
    Route::get('auth/login', 'CustomAuthenticationController@show');
    Route::post('auth/login', 'CustomAuthenticationController@loginUser');
    Route::get('auth/logout', 'CustomAuthenticationController@logoutUser');

    //Password reset routes
    Route::get('/auth/reset-password', 'CustomAuthenticationController@resetPasswordView');
    Route::post('/auth/reset-password', 'CustomAuthenticationController@passwordReset');

    // Registration routes...
    Route::get('auth/register', 'Auth\AuthController@getRegister');
    Route::post('auth/register', 'Auth\AuthController@postRegister');

    //Bitbucket authentication
    Route::get('bitbucket-auth/register', 'BitbucketContoller@registerView');
    Route::post('bitbucket-auth/register', 'BitbucketContoller@register');
});

/*
|--------------------------------------------------------------------------
| General Routes
|--------------------------------------------------------------------------
|
| This grouping of routes are used when the user must be logged into their account
| Most pages will require the user to be logged in, except for a few such as the login page
| This route grouping provides the required middleware for user authorisation via the
| 'RedirectIfUserIsNotLoggedIn' middleware
|
*/
Route::group(['middleware' => ['web', 'requireAuth']], function() {
    Route::get('/notifications','NotificationController@show');
    route::get('/notification-history', 'ProfileController@showNotifications');
    route::get('/notification-history-log', 'ProfileController@getNotifications');
    Route::patch('/notifications','NotificationController@update');
});
/*
|--------------------------------------------------------------------------
| Admin Routes
|--------------------------------------------------------------------------
|
| This grouping of routes are used when the user that is logged in is of an 'admin' type.
| Relevant middleware can be found in 'RedirectIfUserIsNotAdmin'
|
*/
Route::group(['middleware' => ['web', 'requireAuthAdmin']], function() {
    Route::get('/admin', 'AdminController@index');

    //routes for user management module
    Route::get('/admin/user-management/create', 'AdminController@showCreateUser');
    Route::get('/admin/user-management/view', 'AdminController@showViewUsers');
    Route::get('/admin/get-all-users', 'AdminController@getAllUsers');
    Route::post('/admin/user-management/create', 'AdminController@createUser');
    Route::get('/admin/user-management/edit/{id}', 'AdminController@showEdit');
    Route::post('/admin/user-management/edit', 'AdminController@updateUser');
    Route::post('/admin/user-management/password-reset', 'AdminController@updatePassword');
    Route::post('/admin/user-management/delete', 'AdminController@deleteUser');

    //routes for logs module
    Route::get('/admin/logs/login', 'AdminController@viewLoginLog');
    Route::get('/admin/get-all-login', 'AdminController@getLoginLog');
    Route::get('/admin/get-all-exceptions', 'AdminController@getExceptionsLog');
    Route::get('/admin/logs/exceptions', 'AdminController@viewExceptionsLog');
});

/*
|--------------------------------------------------------------------------
| Client Routes
|--------------------------------------------------------------------------
|
| This grouping of routes are used when the user that is logged in is of an 'client' type.
| Relevant middleware can be found in 'RedirectIfUserIsNotClient'
|
*/
Route::group(['middleware' => ['web', 'requireAuthClient']], function() {
    Route::get('/client','ClientSessionController@getClientProjects');
    Route::get('/client/project/{id}','ClientSessionController@setSession');
    Route::get('/client/dashboard','ClientProjectController@getDashboard');
    Route::get('client/product-backlog','ClientProjectController@getProductBacklog');
    Route::get('/client/project-analytics','ClientProjectController@getProjectAnalytics');
    Route::get('/client/product-backlog-all','ClientProjectController@getAllUserStories');
});

/*
|--------------------------------------------------------------------------
| Developer Routes
|--------------------------------------------------------------------------
|
| This grouping of routes are used when the user that is logged in is of an 'dev' type.
| Relevant middleware can be found in 'RedirectIfUserIsNotDev'
|
*/
Route::group(['middleware' => ['web', 'requireAuthDev']], function() {
    //developer backlog controller
    Route::get('/dev/product-backlog', 'DeveloperBacklogController@index');
    Route::get('/dev/product-backlog/{userStoryId}', 'DeveloperBacklogController@show');
    Route::put('/dev/product-backlog/{userStoryId}/edit', 'DeveloperBacklogController@store');
    Route::put('/dev/product-backlog/{userStoryId}/delete', 'DeveloperBacklogController@storeDel');

    //developer request controller
    Route::get('/dev/requests', 'DeveloperRequestController@index');
    Route::delete('/dev/requests', 'DeveloperRequestController@destroy');

    //developer project controller
    Route::get('/dev/project', 'DeveloperProjectController@index');
    Route::get('/dev/project/{projectid}', 'DeveloperProjectController@show');
    Route::post('/dev/project/{projectid}', 'DeveloperProjectController@store');

    //developer create user story
    Route::get('/dev/user-story/create', 'UserStoryController@create');
    Route::post('/dev/user-story/create', 'UserStoryController@store');

    //developer my profile
    Route::get('/dev/my-profile/{id}', 'ProfileController@index')
        ->where(['id' => '[0-9]+']);
    Route::get('/dev/my-profile/{id}/edit', 'ProfileController@edit')
        ->where(['id' => '[0-9]+']);
    Route::put('/dev/my-profile/{id}/edit', 'ProfileController@update')
        ->where(['id' => '[0-9]+']);
    Route::put('/dev/my-profile/{id}/edit/password', 'ProfileController@updatePass')
        ->where(['id' => '[0-9]+']);

    //developer dashboard
    Route::get('/dev/dashboard', 'DeveloperDashboardController@index')
        ->where(['id' => '[0-9]+']);

    //developer Sprint Backlog
    Route::get('/dev/sprint-backlog-all/{id}','DeveloperSprintController@getUserStories');
    Route::get('/dev/sprint-backlog','DeveloperSprintController@index');


});

/*
|--------------------------------------------------------------------------
| Project Manager Routes
|--------------------------------------------------------------------------
|
| This grouping of routes are used when the user that is logged in is of a 'pm' type.
| Relevant middleware can be found in 'RedirectIfUserIsNotPM'
|
*/
Route::group(['middleware' => ['web', 'requireAuthPM']], function() {
    //Routes for projects
    Route::get('/pm', 'ProjectsController@index');
    Route::get('/pm/projects/create', 'ProjectsController@create');

    Route::get('/pm/projects/{id}', 'ProjectsController@show')
        ->where(['id' => '[0-9]+']);
    Route::get('/pm/projects/{id}/edit', 'ProjectsController@edit')
        ->where(['id' => '[0-9]+']);
    Route::post('/pm', 'ProjectsController@store');
    Route::patch('/pm/projects/{update}', 'ProjectsController@update')
        ->where(['name' => '[a-z]+']);
    Route::delete('/pm/projects/{delete}','ProjectsController@delete')
        ->where(['name' => '[a-z]+']);

    // Routes for project requests
    Route::get('/pm/projects/requests/delete/', 'ProjectsController@showDeleteRequests');
    Route::get('/pm/projects/requests/edit/', 'ProjectsController@showEditRequests');
    Route::patch('/pm/projects/requests/update-request', 'ProjectsController@updateDeleteRequest');
    Route::patch('/pm/projects/requests/update-project', 'ProjectsController@updateProjectRequest');
    Route::patch('/pm/projects/requests/update-edit', 'ProjectsController@updateEditRequest');
    Route::delete('/pm/projects/requests','ProjectsController@deleteRequest');

    //Routes for team management
    Route::get('/pm/projects/add-team', 'ProjectsController@showAvailableMembers');
    Route::get('/pm/projects/delete-team', 'ProjectsController@showTeamMembers');
    Route::patch('/pm/projects/team/update-team', 'ProjectsController@updateTeamDetails');

    //create user stories
    Route::get('/pm/user-story/create', 'UserStoryController@create');
    Route::post('/pm/user-story/create', 'UserStoryController@store');


    //product backlog
    Route::get('/pm/product-backlog', 'ProductBacklogController@show');
    Route::get('/pm/product-backlog/edit/{id}', 'ProductBacklogController@edit');
    Route::post('/pm/product-backlog/update', 'ProductBacklogController@update');
    Route::get('/pm/product-backlog-all', 'ProductBacklogController@all');
    Route::post('/pm/product-backlog/delete', 'ProductBacklogController@remove');

    /*Routes for sprint backlog for PM*/
    Route::resource('pm/sprint-backlog','SprintController');
    Route::put('/pm/user-story-progress/edit','UserStoryCompletionController@update');
    Route::get('pm/sprint-backlog/sprint/{id}','SprintController@getSprintUserStories');
    Route::put('pm/sprint-backlog/desc/add','SprintController@addDescriptionToUserStories');
    Route::put('pm/sprint-backlog/desc/edit','SprintController@addDescriptionToUserStories');
    Route::get('pm/sprint-backlog/sprints/available','SprintController@getAvailableSprints');
    Route::put('/pm/sprint-backlog/sprints/update','SprintController@updateUserStorySprint');
    Route::get('/pm/sprint-backlog-all/{id}','SprintController@getUserStories');

    Route::get('/pm/sprints-timeline','SprintController@allSprints');
    Route::get('/pm/sprints-timeline-data','SprintController@getSprintsTimeline');
    Route::get('/pm/project-analytics','SprintController@getBurnDown');
    Route::get('/pm/project-burndown','SprintController@renderBurnDown');
    /* End of defining routes for Sprint backlog PM */

    Route::post('tasks/store','TaskController@store');
    Route::put('tasks/{task}','TaskController@update');
    Route::put('tasks/status/{task}','TaskController@updateTaskStatus');
    Route::get('/tasks/progress/{id}','TaskController@calculateUserStoryProgress');
    Route::delete('tasks/{delete}','TaskController@delete');


    //pm my profile
    Route::get('/pm/my-profile/{id}', 'ProfileController@index')
        ->where(['id' => '[0-9]+']);
    Route::get('/pm/my-profile/{id}/edit', 'ProfileController@edit')
        ->where(['id' => '[0-9]+']);
    Route::put('/pm/my-profile/{id}/edit', 'ProfileController@update')
        ->where(['id' => '[0-9]+']);
    Route::put('/pm/my-profile/{id}/edit/password', 'ProfileController@updatePass')
        ->where(['id' => '[0-9]+']);

    //Bitbucket routes for PM
    Route::get('pm/bitbucket/','BitbucketContoller@show');
    Route::get('pm/bitbucket/commit-data','BitbucketContoller@getCommitData');
    Route::get('pm/bitbucket/repo-data','BitbucketContoller@getUserRepos');
});



