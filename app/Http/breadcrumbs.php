<?php
/*
 * Library: http://laravel-breadcrumbs.davejamesmiller.com
 * Documentation: http://laravel-breadcrumbs.davejamesmiller.com/en/latest/start.html
 * File Author: Deleepa
 * Description: This file contains all the code required for breadcrumb navigation
 */

/*
 * Admin Section
 * -------------
 * The following breadcrumb navigations will only apply to the admins page
 * */
// Admin Dashboard
Breadcrumbs::register('admin-home', function($breadcrumbs)
{
    $breadcrumbs->push('Admin Dashboard', '/admin');
});

// Admin Dashboard > Create Users
Breadcrumbs::register('create-users', function($breadcrumbs)
{
    $breadcrumbs->parent('admin-home');
    $breadcrumbs->push('Create Users', '/admin/user-management/create');
});

// Admin Dashboard > View Users
Breadcrumbs::register('view-users', function($breadcrumbs)
{
    $breadcrumbs->parent('admin-home');
    $breadcrumbs->push('View Users', '/admin/user-management/view');
});

// Admin Dashboard > View Users > Edit Users
Breadcrumbs::register('edit-users', function($breadcrumbs)
{
    $breadcrumbs->parent('view-users');
    $breadcrumbs->push('Edit Users', '/admin/user-management/edit/1');
});

// Admin Dashboard > Login Log
Breadcrumbs::register('login-log', function($breadcrumbs)
{
    $breadcrumbs->parent('admin-home');
    $breadcrumbs->push('Login Log', '/admin/logs/login');
});

// Admin Dashboard > Exception Log
Breadcrumbs::register('exception-log', function($breadcrumbs)
{
    $breadcrumbs->parent('admin-home');
    $breadcrumbs->push('Exception Log', '/admin/logs/exceptions');
});


// Dev-Dashboard >
Breadcrumbs::register('dev-dashboard', function($breadcrumbs)
{
    $breadcrumbs->push('Dashboard', action('DeveloperProjectController@index'));
    //$breadcrumbs->push('Dashboard', '/dev/project');
});


// Project >
Breadcrumbs::register('dev-project', function($breadcrumbs)
{
    //$breadcrumbs->push('Project', action('DeveloperProjectController@index'));
    $breadcrumbs->push('Project', '/dev/project');
});

// Project > View Project
Breadcrumbs::register('dev-project-view', function($breadcrumbs)
{
    $breadcrumbs->parent('dev-project');
    $breadcrumbs->push('View Project', '/dev/project/{$id}');
});

// Create User Story
Breadcrumbs::register('create-user-story', function($breadcrumbs, $type)
{
    $breadcrumbs->push('Create User Story', '/'.$type.'/project/{$id}');
});

// Product Backlog >
Breadcrumbs::register('dev-product-backlog', function($breadcrumbs)
{
    $breadcrumbs->push('Product Backlog', '/dev/product-backlog');
});

// Product Backlog > User Story
Breadcrumbs::register('dev-product-backlog-user-story', function($breadcrumbs)
{
    $breadcrumbs->parent('dev-product-backlog');
    $breadcrumbs->push('User Story', '/dev/product-backlog/{userStoryId}');
});

// Requests
Breadcrumbs::register('dev-requests', function($breadcrumbs)
{
    $breadcrumbs->push('Requests', '/dev/requests');
});

// My Profile
Breadcrumbs::register('my-profile', function($breadcrumbs, $id, $type)
{
    $breadcrumbs->push('My Profile', '/'.$type.'/my-profile/'.$id);
});

// My Profile > Edit Profile
Breadcrumbs::register('my-profile-edit', function($breadcrumbs, $id, $type)
{
    $breadcrumbs->parent('my-profile' , $id, $type);
    $breadcrumbs->push('Edit Profile', '/'.$type.'/my-profile/{$id}/edit');
});



//PM-Dashboard
Breadcrumbs::register('pm-dashboard', function($breadcrumbs)
{
    $breadcrumbs->push('Dashboard', '/pm');
});

// PM-Dashboard > Sprint-Backlog
Breadcrumbs::register('sprint-backlog', function($breadcrumbs) {
    $breadcrumbs->parent('pm-dashboard');
    $breadcrumbs->push('Sprint Backlog', action('SprintController@index'));
});

//PM-Dashboard > Projects
Breadcrumbs::register('pm-projects', function($breadcrumbs, $id)
{
    $breadcrumbs->parent('pm-dashboard');
    $breadcrumbs->push('Project Details', '/pm/projects/'.$id);
});

//PM-Dashboard > Create
Breadcrumbs::register('pm-projects-create', function($breadcrumbs)
{
    $breadcrumbs->parent('pm-dashboard');
    $breadcrumbs->push('Create Project', '/pm/projects/create/');
});

//PM-Dashboard > Edit
Breadcrumbs::register('pm-projects-edit', function($breadcrumbs , $id)
{
    $breadcrumbs->parent('pm-projects', $id);
    $breadcrumbs->push('Edit Project', '/pm/projects/{id}/edit');
});

//PM-Dashboard > Edit Requests
Breadcrumbs::register('pm-projects-edit-requests', function($breadcrumbs)
{
    $breadcrumbs->parent('pm-dashboard');
    $breadcrumbs->push('Edit Requests', '/pm/projects/requests/edit/');
});

//PM-Dashboard > Delete Requests
Breadcrumbs::register('pm-projects-delete-requests', function($breadcrumbs)
{
    $breadcrumbs->parent('pm-dashboard');
    $breadcrumbs->push('Delete Requests', '/pm/projects/requests/delete/');
});

//PM-Dashboard > Team Members
Breadcrumbs::register('pm-projects-team-members', function($breadcrumbs)
{
    $breadcrumbs->parent('pm-dashboard');
    $breadcrumbs->push('Delete Requests', '/pm/projects/delete-team');
});

//PM-Dashboard > Add Developers
Breadcrumbs::register('pm-projects-add-developers', function($breadcrumbs)
{
    $breadcrumbs->parent('pm-dashboard');
    $breadcrumbs->push('Add Developers', '/pm/projects/add-team');
});

//PM-Dashboard > Delete Requests
Breadcrumbs::register('pm-projects-delete-developers', function($breadcrumbs)
{
    $breadcrumbs->parent('pm-dashboard');
    $breadcrumbs->push('Project Team', '/pm/projects/delete-team');
});

//Notifications
Breadcrumbs::register('notifications', function($breadcrumbs)
{
    $breadcrumbs->parent('pm-dashboard');
    $breadcrumbs->push('Notifications', '/notifications');
});

//projects
Breadcrumbs::register('client-projects', function($breadcrumbs)
{
    $breadcrumbs->push('Projects', '/client');
});

//Projects > Product-Backlog
Breadcrumbs::register('client-product-backlog', function($breadcrumbs)
{
    $breadcrumbs->parent('client-projects');
    $breadcrumbs->push('Product Backlog', '/client/product-backlog');
});

//Projects > Dashboard
Breadcrumbs::register('client-dashboard', function($breadcrumbs)
{
    $breadcrumbs->parent('client-projects');
    $breadcrumbs->push('Dashboard', '/client/dashboard');
});

//Dev-Sprint Backlog
Breadcrumbs::register('dev-sprintBacklog', function($breadcrumbs)
{
    $breadcrumbs->push('Dev-Sprint Backlog', '/dev/sprint-backlog');
});

//Dashboard Sprint Duration
Breadcrumbs::register('pm-sprintDuration', function($breadcrumbs)
{
    $breadcrumbs->parent('pm-dashboard');
    $breadcrumbs->push('Sprint Durations', '/pm/sprints-timeline');
});



