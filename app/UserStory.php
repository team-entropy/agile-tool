<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserStory extends Model
{
    //Specify which table should be used
    protected $table = 'user_story';
    protected $primaryKey = 'id';

    public $timestamps = false;

    protected $fillable = [
        'title',
        'description',
        'effort',
        'priority',
        'progress'
    ];

    public function scopeProject($query,$value){
        $query->where('project_id','=',$value);
    }

}
