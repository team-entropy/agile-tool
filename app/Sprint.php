<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sprint extends Model
{
    protected $table = 'sprint';
    protected $primaryKey = 'sprint_id';
    public  $timestamps = false;

    /**
     * @param $query
     * @param $value
     */
    public function scopeProject($query,$value){
        $query->where('project_id','=',$value);
    }



}
