<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/*
 * This factory defines how the User table should be filled out.
 * The email of every user is composed from their name so those are processed outside the return array
 * */
$factory->define(App\User::class, function (Faker\Generator $faker) {

    $name = $faker->name;
    $email = str_replace(' ', '', $name);
    $email = strtolower($email) . "@gmail.com";

    return [
        'name' => $name,
        'email' => $email,
        'password' => bcrypt('password'),
        'technology_tags' => implode('', $faker->randomElements(array('#java', '#php', '#c++', '#ruby', '#c', '#go'), 3)),
        'project_id' => $faker->optional()->randomElement(array(1, 2, 3, 4, 5)),
        'type' => $faker->randomElement(array('dev', 'client', 'pm'))
    ];
});

/*
 * Factory defines how the Project table will be filled
 * */
$factory->define(App\Project::class, function(Faker\Generator $faker) {
    return [
        'project_owner' => 1,
        'title' => $faker->sentence(5, true),
        'description' => $faker->paragraph(2, true),
        'technology' => $faker->randomElement(array('#java', '#php', '#c++', '#ruby', '#c', '#go')),
        'created_on' => $faker->dateTimeBetween('-2 months', 'now')
    ];
});

/*
 * Factory defines how the UserStory table will be filled
 * */
$factory->define(App\UserStory::class, function(Faker\Generator $faker) {
    return [
        'project_id' => $faker->randomElement(array(1, 2, 3, 4, 5)),
        'title' => $faker->sentence(5, true),
        'description' => $faker->paragraph(3, true),
        'effort' => $faker->numberBetween(1, 12),
        'progress' => $faker->randomElement(array('Completed', 'In Progress', 'Testing')),
        'priority' => $faker->numberBetween(1, 5),
        'user_id' => $faker->numberBetween(3, 9)
    ];
});
