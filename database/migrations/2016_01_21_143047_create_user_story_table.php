<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserStoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_story', function (Blueprint $table) {
            $table->increments('id');
            $table->string('project_id');
            $table->string('sprint_id')->nullable();
            $table->string('title');
            $table->string('description');
            $table->integer('effort');
            $table->integer('priority')->nullable();
            $table->string('progress');
            $table->string('filename')->nullable();
            $table->integer('user_id')->unsigned();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('user_story');
    }
}
