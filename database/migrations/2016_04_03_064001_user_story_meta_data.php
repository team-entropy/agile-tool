<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UserStoryMetaData extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('user_story_meta_data', function (Blueprint $table) {
            $table->increments('id');
            $table->string('status');
            $table->timestamp('time_changed');
            $table->integer('project_id');
            $table->integer('user_story_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('user_story_meta_data');
    }
}
