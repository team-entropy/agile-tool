<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDeleteUsRequestTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('delete_us_request', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('us_id');
            $table->integer('user_id');
            $table->integer('pm_id');
            $table->integer('project_id');
            $table->string('delete_reason');
            $table->string('type');
            $table->date('requested_on');
            $table->string('reason')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('delete_us_request');
    }
}
