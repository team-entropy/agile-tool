<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEditUsRequestTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('edit_us_request', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('us_id');
            $table->integer('user_id');
            $table->integer('pm_id');
            $table->integer('project_id');
            $table->string('field');
            $table->string('value');
            $table->string('edited_value');
            $table->string('type');
            $table->date('requested_on');
            $table->string('reason')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('edit_us_request');
    }
}
