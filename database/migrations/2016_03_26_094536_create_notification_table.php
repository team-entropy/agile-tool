<?php

use Carbon\Carbon;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNotificationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notification', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('project_id');
            $table->integer('us_id');
            $table->integer('triggered_by');
            $table->integer('affected_user');
            $table->boolean('read');
            $table->string('type');
            $table->timestamp('time');
        });

        DB::table('notification')->insert(
            array(
                'project_id' => 1,
                'triggered_by' => 1,
                'affected_user' => 3,
                'read' => 0,
                'type' => 'add-developer',
                'time' =>  Carbon::now()->toDateTimeString()
            )
        );

        DB::table('notification')->insert(
            array(
                'project_id' => 1,
                'triggered_by' => 1,
                'affected_user' => 3,
                'read' => 0,
                'type' => 'remove-developer',
                'time' =>  Carbon::now()->toDateTimeString()
            )
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('notification');
    }
}
