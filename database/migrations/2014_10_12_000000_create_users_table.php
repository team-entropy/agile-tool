<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->string('password', 60);
            $table->string('avatar')->nullable();
            $table->string('technology_tags')->nullable();
            $table->string('project_id')->nullable();
            $table->string('bitbucket_username')->nullable();
            $table->string('type'); //type is used to resolve Project Manager (pm), Developer (dev) or Client (client)
            $table->rememberToken();
            $table->timestamps();
        });

        /**
         * Insert some dummy user data into the database - Database Seeding
        */
        DB::table('users')->insert(
            array(
                'name' => 'John Wick',
                'email' => 'johnwick@gmail.com',
                'password' => bcrypt('password'),
                'type' => 'pm'
            )
        );

        DB::table('users')->insert(
            array(
                'name' => 'Bruce Wayne',
                'email' => 'brucewayne@gmail.com',
                'password' => bcrypt('password'),
                'type' => 'client'
            )
        );

        DB::table('users')->insert(
            array(
                'name' => 'Austin Powers',
                'email' => 'austinpowers@gmail.com',
                'password' => bcrypt('password'),
                'type' => 'dev'
            )
        );

        DB::table('users')->insert(
            array(
                'name' => 'Charlie Day',
                'email' => 'charlieday@gmail.com',
                'password' => bcrypt('password'),
                'type' => 'dev'
            )
        );

        DB::table('users')->insert(
            array(
                'name' => 'Dennis Reynolds',
                'email' => 'dennisreynolds@gmail.com',
                'password' => bcrypt('password'),
                'type' => 'dev'
            )
        );

        DB::table('users')->insert(
            array(
                'name' => 'Ben Kingsley',
                'email' => 'benkingsley@gmail.com',
                'password' => bcrypt('password'),
                'type' => 'dev'
            )
        );

        DB::table('users')->insert(
            array(
                'name' => 'Veronica Mars',
                'email' => 'veronicamars@gmail.com',
                'password' => bcrypt('password'),
                'type' => 'dev'
            )
        );

        DB::table('users')->insert(
            array(
                'name' => 'Emma Watson',
                'email' => 'emmawatson@gmail.com',
                'password' => bcrypt('password'),
                'type' => 'dev'
            )
        );

        DB::table('users')->insert(
            array(
                'name' => 'India Summers',
                'email' => 'indiasummers@gmail.com',
                'password' => bcrypt('password'),
                'type' => 'dev'
            )
        );

        DB::table('users')->insert(
            array(
                'name' => 'Mary Watson',
                'email' => 'mary@gmail.com',
                'password' => bcrypt('password'),
                'type' => 'admin'
            )
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
